<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['page'] = 'Auth';
        $this->load->view('login', $data);
    }


    public function log(){

        if($_POST){
            $query = $this->login_model->validate_user();

            if($query){
                $data = array();
                foreach($query as $row){
                    $data = array(
                        'id' => $row->id,
                        'name' => $row->first_name.' '.$row->last_name,
                        'email' =>$row->email,
                        'branch' =>$row->branch,
                        'role' =>$row->role,
                        'psn' =>'SELAMAT BEKERJA',
                        'psn_tmb' => base_url('assets/images/gallery/prank3.png'),
                        'psn_mid' => base_url('assets/images/gallery/prank3.png'),
                        'mid' =>base_url($row->img_mid),
                        'tmb' =>base_url($row->img_tmb),
                        'is_login' => TRUE
                    );
                    $this->session->set_userdata($data);
                    $url = base_url('dashboard');
                }
                echo json_encode(array('st'=>1,'url'=> $url)); //--success
            }else{
                echo json_encode(array('st'=>0)); //-- error
            }

        }else{
            $this->load->view('auth',$data);
        }
    }



    function logout(){
        $this->session->sess_destroy();
        $data = array();
        $data['page'] = 'logout';
        $this->load->view('login', $data);
    }

}
