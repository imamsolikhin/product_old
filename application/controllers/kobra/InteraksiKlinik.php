<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class InteraksiKlinik extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('report_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'interaksi_klinik';
        $data['services'] = 'kobra/InteraksiKlinik/';
        $data['page_title'] = 'Interaksi Klinik';
        $data['main_content'] = $this->load->view('kobra/interaksi-klinik/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    public function closing(){
        $data = array();
        $data['table'] = 'interaksi_klinik';
        $data['services'] = 'kobra/InteraksiKlinik/';
        $data['page_title'] = 'Request Closing';
        $data['main_content'] = $this->load->view('kobra/interaksi-klinik/closing', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {
            $data = array(
                'klinik' => $_POST['klinik'],
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'phone_wa' => $_POST['phone_wa'],
                'age' => $_POST['age'],
                'advertise' => $_POST['advertise'],
                'interaksi_kategory' => $_POST['interaksi_kategory'],
                'id_iklan' => $_POST['id_iklan'],
                'gender' => $_POST['gender'],
                'address' => $_POST['address'],
                'city' => $_POST['city'],
                'complain' => $_POST['complain'],
                'ship_kerja' => $_POST['ship_kerja'],
                'pasien_status' => $_POST['pasien_status'],
                'emosional' => $_POST['emosional'],
                'confirm_status' => $_POST['confirm_status'],
                'confirm_date' => $_POST['confirm_date'],
                'status' => TRUE,
                'created_by' => $this->session->userdata('name'),
                'created_at' => current_datetime()
            );

            // echo $_POST['is_stay'];die;
            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $InteraksiKlinik = $this->common_model->get_opt('name',$_POST['name'],'interaksi_klinik');

            if (empty($InteraksiKlinik)) {
                $InteraksiKlinik = $this->common_model->insert($data, 'interaksi_klinik');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                if($_POST['is_stay']!="on"){
                    redirect(base_url('kobra/InteraksiKlinik'));
                }
            } else {
                $this->session->set_flashdata('msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/InteraksiKlinik/';
        $data['page_title'] = 'Interaksi Klinik';
        $data['list_klinik'] = $this->common_model->select_active('klinik');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['list_iklan'] = $this->common_model->select_active('iklan');
        $data['list_interaksi_kategory'] = $this->common_model->select_active('interaksi_kategory');
        $data['list_gender'] = $this->common_model->select_active('gender');
        $data['list_emosional'] = $this->common_model->select_active('emosional');
        $data['list_pasien_status'] = $this->common_model->select_active('pasien_status');
        $data['list_confirm_status'] = $this->common_model->select_active('confirm_status');
        $data['main_content'] = $this->load->view('kobra/interaksi-klinik/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            
            $data = array(
                'klinik' => $_POST['klinik'],
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'phone_wa' => $_POST['phone_wa'],
                'age' => $_POST['age'],
                'advertise' => $_POST['advertise'],
                'interaksi_kategory' => $_POST['interaksi_kategory'],
                'id_iklan' => $_POST['id_iklan'],
                'gender' => $_POST['gender'],
                'address' => $_POST['address'],
                'city' => $_POST['city'],
                'complain' => $_POST['complain'],
                'ship_kerja' => $_POST['ship_kerja'],
                'pasien_status' => $_POST['pasien_status'],
                'emosional' => $_POST['emosional'],
                'confirm_status' => $_POST['confirm_status'],
                'confirm_date' => $_POST['confirm_date']
            );
            
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'interaksi_klinik');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/InteraksiKlinik'));
        }
        $data['services'] = 'kobra/InteraksiKlinik/';
        $data['page_title'] = 'Interaksi Klinik';
        $data['list_klinik'] = $this->common_model->select_active('klinik');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['list_iklan'] = $this->common_model->select_active('iklan');
        $data['list_interaksi_kategory'] = $this->common_model->select_active('interaksi_kategory');
        $data['list_gender'] = $this->common_model->select_active('gender');
        $data['list_emosional'] = $this->common_model->select_active('emosional');
        $data['list_pasien_status'] = $this->common_model->select_active('pasien_status');
        $data['list_confirm_status'] = $this->common_model->select_active('confirm_status');
        $data['data'] = $this->common_model->get_id($id,'interaksi_klinik');
        $data['main_content'] = $this->load->view('kobra/interaksi-klinik/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function reqsclosing($id){
        if ($_POST) {
            
            $data = array(
                'remark' => $_POST['remark']
            );
            
            $result = $this->common_model->upload_image(2000,$_POST['name'],"photo");
            if($result){
                $data['img_mid'] .= $result['images'];
                $data['img_tmb'] .= $result['thumb'];
            }
            $data = $this->security->xss_clean($data);
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            $this->common_model->edit_option($data, $id, 'interaksi_klinik');
            redirect(base_url('kobra/InteraksiKlinik/closing'));
        }
        $data['services'] = 'kobra/InteraksiKlinik/';
        $data['page_title'] = 'Closing Pasien';
        $data['data'] = $this->common_model->get_id($id,'interaksi_klinik');
        $data['main_content'] = $this->load->view('kobra/interaksi-klinik/edit_closing', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'interaksi_klinik');
        $this->session->set_flashdata('msg', 'Interaksi Klinik active Successfully');
        redirect(base_url('kobra/InteraksiKlinik'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'interaksi_klinik');
        $this->session->set_flashdata('msg', 'Interaksi Klinik deactive Successfully');
        redirect(base_url('kobra/InteraksiKlinik'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'interaksi_klinik'); 
        $this->session->set_flashdata('msg', 'Interaksi Klinik deleted Successfully');
        redirect(base_url('kobra/InteraksiKlinik'));
    }


}