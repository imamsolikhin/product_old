<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TypeTransaction extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'type_transaction';
        $data['services'] = 'kobra/TypeTransaction/';
        $data['page_title'] = 'Type Transaction';
        $data['main_content'] = $this->load->view('kobra/type-transaction/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $TypeTransaction = $this->common_model->get_opt('name',$_POST['name'],'type_transaction');

            if (empty($TypeTransaction)) {
                $TypeTransaction = $this->common_model->insert($data, 'type_transaction');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/TypeTransaction'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/TypeTransaction/';
        $data['page_title'] = 'Type Transaction';
        $data['main_content'] = $this->load->view('kobra/type-transaction/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'type_transaction');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/TypeTransaction'));
        }
        $data['services'] = 'kobra/TypeTransaction/';
        $data['page_title'] = 'TypeTransaction';
        $data['data'] = $this->common_model->get_id($id,'type_transaction');
        $data['main_content'] = $this->load->view('kobra/type-transaction/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'type_transaction');
        $this->session->set_flashdata('msg', 'Type Transaction active Successfully');
        redirect(base_url('kobra/TypeTransaction'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'type_transaction');
        $this->session->set_flashdata('msg', 'Type Transaction deactive Successfully');
        redirect(base_url('kobra/TypeTransaction'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'type_transaction'); 
        $this->session->set_flashdata('msg', 'Type Transaction deleted Successfully');
        redirect(base_url('kobra/TypeTransaction'));
    }


}