<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'produk';
        $data['services'] = 'kobra/Produk/';
        $data['page_title'] = 'Produk';
        $data['main_content'] = $this->load->view('kobra/produk/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {
            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Produk = $this->common_model->get_opt('name',$_POST['name'],'Produk');

            if (empty($Produk)) {
                $Produk = $this->common_model->insert($data, 'Produk');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Produk'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/Produk/';
        $data['page_title'] = 'Produk';
        $data['main_content'] = $this->load->view('kobra/produk/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'Produk');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/Produk'));
        }
        $data['services'] = 'kobra/Produk/';
        $data['page_title'] = 'Produk';
        $data['data'] = $this->common_model->get_id($id,'Produk');
        $data['main_content'] = $this->load->view('kobra/produk/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'Produk');
        $this->session->set_flashdata('msg', 'Produk active Successfully');
        redirect(base_url('kobra/Produk'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'Produk');
        $this->session->set_flashdata('msg', 'Produk deactive Successfully');
        redirect(base_url('kobra/Produk'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'Produk'); 
        $this->session->set_flashdata('msg', 'Produk deleted Successfully');
        redirect(base_url('kobra/Produk'));
    }


}