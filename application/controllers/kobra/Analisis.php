<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analisis extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function cs_klinik(){
        $data = array();
        $data['page_title'] = 'Analisis Marketing Klinik';
        $data['main_content'] = $this->load->view('analisis/cs-klinik', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function calendar_klinik(){
        $data = array();
        $data['page_title'] = 'Analisis Kalendar Klinik';
        $data['list_confirm_status'] = $this->common_model->select_other('confirm_status as name', '1=1 AND confirm_status != "" AND confirm_status != "0" ','confirm_status' , '', 'interaksi_klinik');
        $data['main_content'] = $this->load->view('analisis/calendar-klinik', $data, TRUE);
        $this->load->view('index', $data);
    }
}