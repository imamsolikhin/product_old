<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DeliveryStatus extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'delivery_status';
        $data['services'] = 'kobra/DeliveryStatus/';
        $data['page_title'] = 'Delivery Status';
        $data['main_content'] = $this->load->view('kobra/delivery-status/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $DeliveryStatus = $this->common_model->get_opt('name',$_POST['name'],'delivery_status');

            if (empty($DeliveryStatus)) {
                $DeliveryStatus = $this->common_model->insert($data, 'delivery_status');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/DeliveryStatus'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/DeliveryStatus/';
        $data['page_title'] = 'Delivery Status';
        $data['main_content'] = $this->load->view('kobra/delivery-status/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'delivery_status');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/DeliveryStatus'));
        }
        $data['services'] = 'kobra/DeliveryStatus/';
        $data['page_title'] = 'Delivery Status';
        $data['data'] = $this->common_model->get_id($id,'delivery_status');
        $data['main_content'] = $this->load->view('kobra/delivery-status/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'delivery_status');
        $this->session->set_flashdata('msg', 'DeliveryStatus active Successfully');
        redirect(base_url('kobra/DeliveryStatus'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'delivery_status');
        $this->session->set_flashdata('msg', 'DeliveryStatus deactive Successfully');
        redirect(base_url('kobra/DeliveryStatus'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'delivery_status'); 
        $this->session->set_flashdata('msg', 'DeliveryStatus deleted Successfully');
        redirect(base_url('kobra/DeliveryStatus'));
    }


}