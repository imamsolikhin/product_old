<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdvertiseProduk extends CI_Controller {

    public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'advertise_produk';
        $data['services'] = 'kobra/AdvertiseProduk/';
        $data['page_title'] = 'AdvertiseProduk';
        $data['main_content'] = $this->load->view('kobra/advertise-produk/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $AdvertiseProduk = $this->common_model->get_opt('name',$_POST['name'],'advertise_produk');

            if (empty($AdvertiseProduk)) {
                $AdvertiseProduk = $this->common_model->insert($data, 'advertise_produk');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/AdvertiseProduk'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/AdvertiseProduk/';
        $data['page_title'] = 'AdvertiseProduk';
        $data['main_content'] = $this->load->view('kobra/advertise-produk/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'advertise_produk');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/AdvertiseProduk'));
        }
        $data['services'] = 'kobra/AdvertiseProduk/';
        $data['page_title'] = 'AdvertiseProduk';
        $data['data'] = $this->common_model->get_id($id,'advertise_produk');
        $data['main_content'] = $this->load->view('kobra/advertise-produk/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'advertise_produk');
        $this->session->set_flashdata('msg', 'Advertise Produk active Successfully');
        redirect(base_url('kobra/AdvertiseProduk'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'advertise_produk');
        $this->session->set_flashdata('msg', 'Advertise Produk deactive Successfully');
        redirect(base_url('kobra/AdvertiseProduk'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'advertise_produk'); 
        $this->session->set_flashdata('msg', 'Advertise Produk deleted Successfully');
        redirect(base_url('kobra/AdvertiseProduk'));
    }


}