<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'pesan';
        $data['services'] = 'kobra/Pesan/';
        $data['page_title'] = 'Pesan Sistem';
        $data['main_content'] = $this->load->view('kobra/pesan/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'branch'  => $_POST['branch'],
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Pesan = $this->common_model->get_opt('name',$_POST['name'],'pesan');

            if (empty($Pesan)) {
                    
                $data = array(
                    'name' => $_POST['name']
                );
                
                $result = $this->common_model->upload_image(2000,date("d-m-Y",strtotime(current_datetime())).'PSN'.$_POST['id'],'photo');
                if($result){
                    $data['img_mid'] .= $result['images'];
                    $data['img_tmb'] .= $result['thumb'];
                }
                $Pesan = $this->common_model->insert($data, 'pesan');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Pesan'));
            }
            $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
        }

        $data['services'] = 'kobra/Pesan/';
        $data['page_title'] = 'Pesan Sistem';
        $data['list_branch'] = $this->common_model->select_active('branch');
        $data['main_content'] = $this->load->view('kobra/pesan/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'branch'  => $_POST['branch'],
                'name' => $_POST['name']
            );
            $result = $this->common_model->upload_image(2000,date("d-m-Y",strtotime(current_datetime())).'PSN'.$_POST['id'],'photo');
            if($result){
                $data['img_mid'] .= $result['images'];
                $data['img_tmb'] .= $result['thumb'];
            }
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'pesan');
            $this->session->set_flashdata('msg', $_POST['name'].' Updated Successfully');
            redirect(base_url('kobra/Pesan'));
        }
        $data['services'] = 'kobra/Pesan/';
        $data['page_title'] = 'Pesan Sistem';
        $data['list_branch'] = $this->common_model->select_active('branch');
        $data['data'] = $this->common_model->get_id($id,'pesan');
        $data['main_content'] = $this->load->view('kobra/pesan/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'pesan');
        $this->session->set_flashdata('msg', 'Pesan active Successfully');
        redirect(base_url('kobra/Pesan'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'pesan');
        $this->session->set_flashdata('msg', 'Pesan deactive Successfully');
        redirect(base_url('kobra/Pesan'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'pesan'); 
        $this->session->set_flashdata('msg', 'Pesan deleted Successfully');
        redirect(base_url('kobra/Pesan'));
    }


}