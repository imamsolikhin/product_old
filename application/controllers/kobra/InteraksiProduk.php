<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class InteraksiProduk extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('report_model');  
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'interaksi_produk';
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Interaksi Produk';
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    public function closing(){
        $data = array();
        $data['table'] = 'interaksi_produk';
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Request Closing';
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/closing', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function approval(){
        $data = array();
        $data['table'] = 'interaksi_produk';
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Request Approval';
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/approval', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function gudang(){
        $data = array();
        $data['table'] = 'interaksi_produk';
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'GUDANG CONFIRMATION';
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/gudang', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function invoice_customer($id){
        $data['services'] = 'kobra/InteraksiProduk/closing';
        $data['page_title'] = 'Invoice Customer';
        $data['data'] = $this->common_model->get_id($id,'interaksi_produk');
        $data['main_content'] = $this->load->view('kobra/report/invoice_customer', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {
            $data = array(
                  'branch'  => $this->session->userdata('branch'),
                  'produk'  => $_POST['produk'],
                  'ship_kerja'  => $_POST['ship_kerja'],
                  'advertise' => $_POST['advertise'],
                  'id_iklan'  => $_POST['id_iklan'],
                  'interaksi_kategory'  => $_POST['interaksi_kategory'],
                  'nama_lengkap'  => $_POST['nama_lengkap'],
                  'keluhan' => $_POST['keluhan'],
                  'confirm_status' => $_POST['confirm_status'],
                  'jenis_kelamin' => $_POST['jenis_kelamin'],
                  'umur'  => $_POST['umur'],
                  'tlp' => $_POST['tlp'],
                  'wa'  => $_POST['wa'],
                  'alamat'  => $_POST['alamat'],
                  'alamat_lengkap'  => $_POST['alamat_lengkap'],
                  'no_rumah'  => $_POST['no_rumah'],
                  'rt'  => $_POST['rt'],
                  'rw'  => $_POST['rw'],
                  'desa'  => $_POST['desa'],
                  'kelurahan' => $_POST['kelurahan'],
                  'patokan' => $_POST['patokan'],
                  'kecamatan' => $_POST['kecamatan'],
                  'kota'  => $_POST['kota'],
                  'provinsi'  => $_POST['provinsi'],
                  'kode_pos'  => $_POST['kode_pos'],
                  'emosional' => $_POST['emosional'],
                  'jml_pesanan' => $_POST['jml_pesanan'],
                  'harga' => $_POST['harga'],
                  'biaya_pengiriman'  => $_POST['biaya_pengiriman'],
                  'asuransi'  => $_POST['asuransi'],
                  'total_transaksi' => $_POST['total_transaksi'],
                  'total_transaksi_id'  => $_POST['transaksi'],
                  'kurir' => $_POST['kurir'],
                  'type_transaction' => $_POST['type_transaction'],
                  'akun_market' => $_POST['akun_market'],
                  'bank'  => $_POST['bank'],
                  'reqs_status' => $_POST['reqs_status'],
                  'customer_status'  => $_POST['customer_status'],
                  'tanggal_order' => $_POST['tanggal_order'],
                  'estimasi_sampai' => $_POST['estimasi_sampai'],
                  'created_by' => $this->session->userdata('name'),
                  'created_at' => current_datetime()

                // 'produk' => $_POST['produk'],
                // 'name' => $_POST['name'],
                // 'email' => $_POST['email'],
                // 'phone' => $_POST['phone'],
                // 'phone_wa' => $_POST['phone_wa'],
                // 'age' => $_POST['age'],
                // 'advertise' => $_POST['advertise_produk'],
                // 'interaksi_kategory' => $_POST['interaksi_kategory'],
                // 'id_iklan' => $_POST['id_iklan'],
                // 'gender' => $_POST['gender'],
                // 'address' => $_POST['address'],
                // 'city' => $_POST['city'],
                // 'complain' => $_POST['complain'],
                // 'ship_kerja' => $_POST['ship_kerja'],
                // 'customer_status' => $_POST['customer_status'],
                // 'emosional' => $_POST['emosional'],
                // 'confirm_status' => $_POST['confirm_status'],
                // 'confirm_date' => $_POST['confirm_date'],
                // 'status' => TRUE,
                // 'created_by' => $this->session->userdata('name'),
                // 'created_at' => current_datetime()
            );

            // echo $_POST['is_stay'];die;
            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            // $InteraksiProduk = $this->common_model->get_opt('nama_lengkap',$_POST['nama_lengkap'],'interaksi_produk');

            // if (empty($InteraksiProduk)) {
                $InteraksiProduk = $this->common_model->insert($data, 'interaksi_produk');
                $this->session->set_flashdata('msg', $_POST['nama_lengkap'].' added Successfully');
                if($_POST['is_stay']!="on"){
                    redirect(base_url('kobra/InteraksiProduk'));
                }
            // } else {
                // $this->session->set_flashdata('msg', $_POST['nama_lengkap'].' already exist, try another name');
            // }
        }

        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Interaksi Produk';
        $data['list_produk'] = $this->common_model->select_active('produk');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['list_advertise_produk'] = $this->common_model->select_active('advertise_produk');
        $data['list_interaksi_kategory'] = $this->common_model->select_active('interaksi_kategory');
        $data['list_gender'] = $this->common_model->select_active('gender');
        $data['list_emosional'] = $this->common_model->select_active('emosional');
        $data['list_kurir'] = $this->common_model->select_active('kurir');
        $data['list_type_transaction'] = $this->common_model->select_active('type_transaction');
        $data['list_bank'] = $this->common_model->select_active('bank');
        $data['list_akun_market'] = $this->common_model->select_active('akun_market');
        $data['list_customer_status'] = $this->common_model->select_active('customer_status');
        $data['list_confirm_status'] = $this->common_model->select_active('confirm_status');
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            
            $data = array(
                  'branch'  => $this->session->userdata('branch'),
                  'produk'  => $_POST['produk'],
                  'ship_kerja'  => $_POST['ship_kerja'],
                  'advertise' => $_POST['advertise'],
                  'id_iklan'  => $_POST['id_iklan'],
                  'interaksi_kategory'  => $_POST['interaksi_kategory'],
                  'nama_lengkap'  => $_POST['nama_lengkap'],
                  'keluhan' => $_POST['keluhan'],
                  'confirm_status' => $_POST['confirm_status'],
                  'jenis_kelamin' => $_POST['jenis_kelamin'],
                  'umur'  => $_POST['umur'],
                  'tlp' => $_POST['tlp'],
                  'wa'  => $_POST['wa'],
                  'alamat'  => $_POST['alamat'],
                  'alamat_lengkap'  => $_POST['alamat_lengkap'],
                  'no_rumah'  => $_POST['no_rumah'],
                  'rt'  => $_POST['rt'],
                  'rw'  => $_POST['rw'],
                  'desa'  => $_POST['desa'],
                  'kelurahan' => $_POST['kelurahan'],
                  'patokan' => $_POST['patokan'],
                  'kecamatan' => $_POST['kecamatan'],
                  'kota'  => $_POST['kota'],
                  'provinsi'  => $_POST['provinsi'],
                  'kode_pos'  => $_POST['kode_pos'],
                  'emosional' => $_POST['emosional'],
                  'jml_pesanan' => $_POST['jml_pesanan'],
                  'harga' => $_POST['harga'],
                  'biaya_pengiriman'  => $_POST['biaya_pengiriman'],
                  'asuransi'  => $_POST['asuransi'],
                  'total_transaksi' => $_POST['total_transaksi'],
                  'total_transaksi_id'  => $_POST['transaksi'],
                  'kurir' => $_POST['kurir'],
                  'type_transaction' => $_POST['type_transaction'],
                  'akun_market' => $_POST['akun_market'],
                  'bank'  => $_POST['bank'],
                  'reqs_status' => $_POST['reqs_status'],
                  'customer_status'  => $_POST['customer_status'],
                  'tanggal_order' => $_POST['tanggal_order'],
                  'estimasi_sampai' => $_POST['estimasi_sampai']
            );
            
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'interaksi_produk');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/InteraksiProduk'));
        }
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Interaksi Produk';
        $data['list_produk'] = $this->common_model->select_active('produk');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['list_advertise_produk'] = $this->common_model->select_active('advertise_produk');
        $data['list_interaksi_kategory'] = $this->common_model->select_active('interaksi_kategory');
        $data['list_gender'] = $this->common_model->select_active('gender');
        $data['list_emosional'] = $this->common_model->select_active('emosional');
        $data['list_kurir'] = $this->common_model->select_active('kurir');
        $data['list_type_transaction'] = $this->common_model->select_active('type_transaction');
        $data['list_bank'] = $this->common_model->select_active('bank');
        $data['list_akun_market'] = $this->common_model->select_active('akun_market');
        $data['list_customer_status'] = $this->common_model->select_active('customer_status');
        $data['list_confirm_status'] = $this->common_model->select_active('confirm_status');
        $data['data'] = $this->common_model->get_id($id,'interaksi_produk');
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function reqsclosing($id){
        if ($_POST) {
            
            $data = array(
                'delivery_resi' => $_POST['delivery_resi'],
                'delivery_ref' => $_POST['delivery_ref'],
                'closing_remark' => $_POST['closing_remark']
            );
            
            $result = $this->common_model->upload_image(2000,$this->session->userdata('name').'-'.$id,'delivery_photo');
            if($result){
                $data['delivery_img_mid'] .= $result['images'];
                $data['delivery_img_tmb'] .= $result['thumb'];
            }
            $data = $this->security->xss_clean($data);
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            $this->common_model->edit_option($data, $id, 'interaksi_produk');
            redirect(base_url('kobra/InteraksiProduk/closing'));
        }
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Closing Pasien';
        $data['data'] = $this->common_model->get_id($id,'interaksi_produk');
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/edit_closing', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    public function check_generade_no($no=1){
        // $branch= $this->common_model->get_name($this->session->userdata('branch'),'branch');
        // $where_range = " DATE('".date("Y-m", strtotime("-1 month"))."-".$branch->tgl."') AND DATE('".date("Y-m",strtotime(current_datetime()))."-".$branch->tgl."') ";
        // $where = "1=1 AND DATE(tanggal_order) BETWEEN ".$where_range." AND branch = '".$this->session->userdata('branch')."' AND delivery_order!=''";
        // $no_order = $this->common_model->get_generade_code($where,'interaksi_produk');
        $order = "HPI".date("my",strtotime(current_datetime()))."-".sprintf("%05d", rand(5,100000));
        print_r($this->db->last_query());
        echo $order;die;
    }

    //-- update users info
    public function reqsapproval($id){
        if ($_POST) {
            $data = array(
                'closing_approval' => $_POST['closing_approval'],
                'delivery_order' => $_POST['delivery_order'],
                'delivery_status' => $_POST['delivery_status'],
                'admin_confirm_by' => $this->session->userdata('name'),
                'closing_approval_date' => current_datetime()
            );

            if($_POST['delivery_order'] == "AUTO"){
            //   $branch= $this->common_model->get_name($this->session->userdata('branch'),'branch');
            //   $where_range = " DATE('".date("Y-m", strtotime("-1 month"))."-".$branch->tgl."') AND DATE('".date("Y-m",strtotime(current_datetime()))."-".$branch->tgl."') ";
            //   $where = "1=1 AND DATE(tanggal_order) BETWEEN ".$where_range." AND branch = '".$this->session->userdata('branch')."' AND delivery_order!=''";
            //   $no_order = $this->common_model->get_generade_code($where,'interaksi_produk');
              $order = "HPI".date("my",strtotime(current_datetime()))."-".sprintf("%05d", rand(5,100000));
              $data['delivery_order'] = $order;
            }
            
            $result = $this->common_model->upload_image(2000,$this->session->userdata('name').'-'.$id,'delivery_photo');
            if($result){
                $data['delivery_img_mid'] .= $result['images'];
                $data['delivery_img_tmb'] .= $result['thumb'];
            }
            
            $data = $this->security->xss_clean($data);
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            $this->common_model->edit_option($data, $id, 'interaksi_produk');
            redirect(base_url('kobra/InteraksiProduk/approval'));
        }
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'Approval Sales';
        $data['list_delivery_status'] = $this->common_model->select_active('delivery_status');
        $data['data'] = $this->common_model->get_id($id,'interaksi_produk');
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/edit_approval', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function reqsgudang($id){
        if ($_POST) {
            $data = array(
                'packing_by' => $_POST['packing_by'],
                'delivery_status' => $_POST['delivery_status'],
                'gudang_confirm_by' => $this->session->userdata('name'),
                'delivery_date' => current_datetime()
            );
            
            $result = $this->common_model->upload_image(2000,$this->session->userdata('name').'-'.$id,'delivery_photo');
            if($result){
                $data['delivery_img_mid'] .= $result['images'];
                $data['delivery_img_tmb'] .= $result['thumb'];
            }
            $data = $this->security->xss_clean($data);
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            $this->common_model->edit_option($data, $id, 'interaksi_produk');
            redirect(base_url('kobra/InteraksiProduk/gudang'));
        }
        $data['services'] = 'kobra/InteraksiProduk/';
        $data['page_title'] = 'GUDANG CONFIRMATION';
        $data['list_delivery_status'] = $this->common_model->select_active('delivery_status');
        $data['data'] = $this->common_model->get_id($id,'interaksi_produk');
        $data['main_content'] = $this->load->view('kobra/interaksi-produk/edit_gudang', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'interaksi_produk');
        $this->session->set_flashdata('msg', 'Interaksi Produk active Successfully');
        redirect(base_url('kobra/InteraksiProduk'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'interaksi_produk');
        $this->session->set_flashdata('msg', 'Interaksi Produk deactive Successfully');
        redirect(base_url('kobra/InteraksiProduk'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'interaksi_produk'); 
        $this->session->set_flashdata('msg', 'Interaksi Produk deleted Successfully');
        redirect(base_url('kobra/InteraksiProduk'));
    }


}