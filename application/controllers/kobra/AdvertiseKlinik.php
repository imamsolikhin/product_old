<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdvertiseKlinik extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'advertise_klinik';
        $data['services'] = 'kobra/AdvertiseKlinik/';
        $data['page_title'] = 'AdvertiseKlinik';
        $data['main_content'] = $this->load->view('kobra/advertise-klinik/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $AdvertiseKlinik = $this->common_model->get_opt('name',$_POST['name'],'advertise_klinik');

            if (empty($AdvertiseKlinik)) {
                $AdvertiseKlinik = $this->common_model->insert($data, 'advertise_klinik');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/AdvertiseKlinik'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/AdvertiseKlinik/';
        $data['page_title'] = 'AdvertiseKlinik';
        $data['main_content'] = $this->load->view('kobra/advertise-klinik/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'advertise_klinik');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/AdvertiseKlinik'));
        }
        $data['services'] = 'kobra/AdvertiseKlinik/';
        $data['page_title'] = 'AdvertiseKlinik';
        $data['data'] = $this->common_model->get_id($id,'advertise_klinik');
        $data['main_content'] = $this->load->view('kobra/advertise-klinik/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'advertise_klinik');
        $this->session->set_flashdata('msg', 'Advertise Klinik active Successfully');
        redirect(base_url('kobra/AdvertiseKlinik'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'advertise_klinik');
        $this->session->set_flashdata('msg', 'Advertise Klinik deactive Successfully');
        redirect(base_url('kobra/AdvertiseKlinik'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'advertise_klinik'); 
        $this->session->set_flashdata('msg', 'Advertise Klinik deleted Successfully');
        redirect(base_url('kobra/AdvertiseKlinik'));
    }


}