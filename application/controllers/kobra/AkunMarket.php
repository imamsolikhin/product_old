<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AkunMarket extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'akun_market';
        $data['services'] = 'kobra/AkunMarket/';
        $data['page_title'] = 'Akun Market';
        $data['main_content'] = $this->load->view('kobra/akun-market/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $AkunMarket = $this->common_model->get_opt('name',$_POST['name'],'akun_market');

            if (empty($AkunMarket)) {
                $AkunMarket = $this->common_model->insert($data, 'akun_market');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/AkunMarket'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/AkunMarket/';
        $data['page_title'] = 'Akun Market';
        $data['main_content'] = $this->load->view('kobra/akun-market/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'akun_market');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/AkunMarket'));
        }
        $data['services'] = 'kobra/AkunMarket/';
        $data['page_title'] = 'AkunMarket';
        $data['data'] = $this->common_model->get_id($id,'akun_market');
        $data['main_content'] = $this->load->view('kobra/akun-market/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'akun_market');
        $this->session->set_flashdata('msg', 'Akun Market active Successfully');
        redirect(base_url('kobra/AkunMarket'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'akun_market');
        $this->session->set_flashdata('msg', 'Akun Market deactive Successfully');
        redirect(base_url('kobra/AkunMarket'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'akun_market'); 
        $this->session->set_flashdata('msg', 'Akun Market deleted Successfully');
        redirect(base_url('kobra/AkunMarket'));
    }


}