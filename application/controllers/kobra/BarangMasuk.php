<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BarangMasuk extends CI_Controller {

    public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'barang_masuk';
        $data['services'] = 'kobra/BarangMasuk/';
        $data['page_title'] = 'Barang Masuk';
        $data['main_content'] = $this->load->view('kobra/barang-masuk/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'branch'  => $this->session->userdata('branch'),
                'produk' => $_POST['produk'],
                'tanggal' => $_POST['tanggal'],
                'total' => $_POST['total'],
                'remark' => $_POST['remark'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            // $BarangMasuk = $this->common_model->get_opt('CONCATE(tanggal,produkA)',$_POST['tanggal'].$_POST['produk'],'barang_masuk');

            // if (empty($BarangMasuk)) {
                $BarangMasuk = $this->common_model->insert($data, 'barang_masuk');
                $this->session->set_flashdata('msg', $_POST['tanggal'].' added Successfully');
                redirect(base_url('kobra/BarangMasuk'));
            // } else {
                // $this->session->set_flashdata('error_msg', $_POST['tanggal'].' already exist, try another date');
            // }
        }

        $data['services'] = 'kobra/BarangMasuk/';
        $data['page_title'] = 'Barang Masuk';
        $data['list_produk'] = $this->common_model->select_active('produk');
        $data['main_content'] = $this->load->view('kobra/barang-masuk/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'branch'  => $this->session->userdata('branch'),
                'produk' => $_POST['produk'],
                'tanggal' => $_POST['tanggal'],
                'total' => $_POST['total'],
                'remark' => $_POST['remark']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'barang_masuk');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/BarangMasuk'));
        }
        $data['services'] = 'kobra/BarangMasuk/';
        $data['page_title'] = 'Barang Masuk';
        $data['list_produk'] = $this->common_model->select_active('produk');
        $data['data'] = $this->common_model->get_id($id,'barang_masuk');
        $data['main_content'] = $this->load->view('kobra/barang-masuk/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'barang_masuk');
        $this->session->set_flashdata('msg', 'BarangMasuk active Successfully');
        redirect(base_url('kobra/BarangMasuk'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'barang_masuk');
        $this->session->set_flashdata('msg', 'BarangMasuk deactive Successfully');
        redirect(base_url('kobra/BarangMasuk'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'barang_masuk'); 
        $this->session->set_flashdata('msg', 'BarangMasuk deleted Successfully');
        redirect(base_url('kobra/BarangMasuk'));
    }


}