<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PasienKlinik extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('report_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'pasien_klinik';
        $data['services'] = 'kobra/PasienKlinik/';
        $data['page_title'] = 'Pasien Klinik';
        $data['main_content'] = $this->load->view('kobra/pasien-klinik/list', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function approval(){
        $data = array();
        $data['table'] = 'pasien_klinik';
        $data['services'] = 'kobra/PasienKlinik/';
        $data['page_title'] = 'Approval Pasien';
        $data['main_content'] = $this->load->view('kobra/pasien-klinik/approval', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function report()
    {
        $data["data"] ="";
        if ($_POST) {
            if(!empty($_POST['start_date']) && !empty($_POST['end_date'])){
                $where = "* FROM pasien_klinik WHERE 1=1 AND DATE(pasien_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
                $result_berkunjung = $this->report_model->select($where,"pasien_klinik.advertise ASC");

                $where = "* FROM pasien_klinik WHERE 1=1 AND (pasien_klinik.phone != '' OR pasien_klinik.phone_wa != '') AND DATE(pasien_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
                $result_WA = $this->report_model->select($where,"pasien_klinik.advertise");

                $where = "* FROM pasien_klinik WHERE 1=1 AND (pasien_klinik.phone != '' OR pasien_klinik.phone_wa != '') AND confirm_status = 'Di Jawab' AND DATE(pasien_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
                $result_tlp = $this->report_model->select($where,"pasien_klinik.advertise");

                $where = "* FROM pasien_klinik WHERE 1=1 AND (pasien_klinik.phone != '' OR pasien_klinik.phone_wa != '') AND confirm_status != 'Di Jawab' AND DATE(pasien_klinik.confirm_date) <= DATE(pasien_klinik.created_at) AND DATE(pasien_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
                $result_belum_resp = $this->report_model->select($where,"pasien_klinik.advertise");

                $where = "* FROM pasien_klinik WHERE 1=1 AND (pasien_klinik.phone != '' OR pasien_klinik.phone_wa != '') AND confirm_status = 'Di Jawab' AND DATE(pasien_klinik.confirm_date) >= DATE(pasien_klinik.created_at) AND DATE(pasien_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
                $result_janji = $this->report_model->select($where,"pasien_klinik.advertise");

                $where = "count(pasien_klinik.id) AS jumlah, pasien_klinik.* FROM pasien_klinik WHERE 1=1 AND DATE(pasien_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') GROUP BY pasien_klinik.advertise";
                $result_resume_iklan = $this->report_model->select($where);

                $data["data"] .= '
                <label style="font-weight:900;">
                             *LAPORAN HASIL IKLAN '.strtoupper($this->input->post('klinik')).'*
                        <br> *Pesan Masuk Messengger : '.$nilai = (!$result_berkunjung) ? 0:count($result_berkunjung).'*		
                        <br> *WA : '.$nilai = (!$result_WA) ? 0:count($result_WA).' Tlp : '.$nilai = (!$result_tlp) ? 0:count($result_tlp).' Janji : '.$nilai = (!$result_janji) ? 0:count($result_janji).'* 
                        <br> *Tanggal Mulai: '.$this->input->post('start_date').'*	
                        <br> *Tanggal Akhir: '.$this->input->post('end_date').'*		
                        <br> *JENIS IKLAN : Carosel, Lead Form, Video Ads dan Video Skip Ads*
                </label>
                ';
                // echo $data["data"];
                $title = "";
                $descrption = "";
                if($result_resume_iklan){
                    $no=1;
                    foreach ($result_resume_iklan as $pasien_list_group) {
                        // echo var_dump($pasien_list_group);
                        $data["data"] .= '<br><br><label style="font-weight:900;">*IKLAN: '.$pasien_list_group->advertise.'*</label>';
                        $jml=0;
                        if($result_berkunjung){
                            $i=1;
                            foreach ($result_berkunjung as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' '.$pasien_list->gender.' '.$pasien_list->name.' '.date("h:i", strtotime($pasien_list->created_at));
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><label style="font-weight:900;">*1. Jumlah yg berkunjung : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_WA){
                            $i=1;
                            foreach ($result_WA as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Iklan: '.$pasien_list->id_iklan;
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><br><label style="font-weight:900;">*2. Jumlah Nomor Yang Didapat : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_tlp){
                            $i=1;
                            foreach ($result_tlp as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Iklan: '.$pasien_list->id_iklan;
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><br><label style="font-weight:900;">*3.  jumlah yg sudah merespon : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_belum_resp){
                            $i=1;
                            foreach ($result_belum_resp as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Ket: '.$pasien_list->remark;
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><br><label style="font-weight:900;">*4.  Jumlah yg belum merespon : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_janji){
                            $i=1;
                            foreach ($result_janji as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Jadwal: '.date("d-m-Y H:i", strtotime($pasien_list->confirm_date));
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Iklan: '.$pasien_list->id_iklan;
                                    $i++;
                                }
                            }
                        }

                        $title .= '<br><br><label style="font-weight:900;">*5.  Jumlah yg janji pasien : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        $no++;
                    }
                }

                $data["data"] .= '<br><br><label style="font-weight:900;">*RESUME SEMUA IKLAN*</label>';
                $i=1;
                if($result_resume_iklan){
                    foreach ($result_resume_iklan as $pasien_list) {
                        $data["data"] .= '<br>&nbsp;'.($i).'.) '.$pasien_list->advertise.': '.$pasien_list->jumlah;
                        $i++;
                    }
                }

                $data["data"] .= '<br><br>--------------------------------------';
                $data["data"] .= '<br><label style="font-weight:900;">*Admin: '.$this->session->userdata('name').'*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label style="font-weight:900;">*Terimakasih*</label>';
            }
        }

        $data['services'] = 'kobra/PasienKlinik/';
        $data['page_title'] = 'Laporan Pasien Klinik';
        $data['list_klinik'] = $this->common_model->select_active('klinik');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['main_content'] = $this->load->view('kobra/pasien-klinik/report', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {
            $data = array(
                'klinik' => $_POST['klinik'],
                'advertise' => $_POST['advertise'],
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'phone_wa' => $_POST['phone_wa'],
                'age' => $_POST['age'],
                'gender' => $_POST['gender'],
                'address' => $_POST['address'],
                'city' => $_POST['city'],
                'complain' => $_POST['complain'],
                'resep' => $_POST['resep'],
                'pembelian' => $_POST['pembelian'],
                'pasien_status_klinik' => $_POST['pasien_status_klinik'],
                'emosional' => $_POST['emosional'],
                'confirm_date' => $_POST['confirm_date'],
                'status' => TRUE,
                'created_by' => $this->session->userdata('name'),
                'created_at' => current_datetime()
            );

            // echo $_POST['is_stay'];die;
            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $PasienKlinik = $this->common_model->get_opt('name',$_POST['name'],'pasien_klinik');

            if (empty($PasienKlinik)) {
                $result = $this->common_model->upload_image(2000,date("d-m-Y",strtotime(current_datetime())).$_POST['name'],'photo');
                if($result){
                    $data['img_mid'] .= $result['images'];
                    $data['img_tmb'] .= $result['thumb'];
                }
                $PasienKlinik = $this->common_model->insert($data, 'pasien_klinik');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                if($_POST['is_stay']!="on"){
                    redirect(base_url('kobra/PasienKlinik'));
                }
            } else {
                $this->session->set_flashdata('msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/PasienKlinik/';
        $data['page_title'] = 'Pasien Klinik';
        $data['list_klinik'] = $this->common_model->select_active('klinik');
        $data['list_advertise'] = $this->common_model->select_active('advertise');
        $data['list_gender'] = $this->common_model->select_active('gender');
        $data['list_emosional'] = $this->common_model->select_active('emosional');
        $data['list_pasien_status_klinik'] = $this->common_model->select_active('pasien_status_klinik');
        $data['main_content'] = $this->load->view('kobra/pasien-klinik/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            
            $data = array(
                'klinik' => $_POST['klinik'],
                'advertise' => $_POST['advertise'],
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'phone_wa' => $_POST['phone_wa'],
                'age' => $_POST['age'],
                'gender' => $_POST['gender'],
                'address' => $_POST['address'],
                'city' => $_POST['city'],
                'complain' => $_POST['complain'],
                'resep' => $_POST['resep'],
                'pembelian' => $_POST['pembelian'],
                'pasien_status_klinik' => $_POST['pasien_status_klinik'],
                'emosional' => $_POST['emosional'],
                'confirm_date' => $_POST['confirm_date']
            );
            
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'pasien_klinik');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/PasienKlinik'));
        }
        $data['services'] = 'kobra/PasienKlinik/';
        $data['page_title'] = 'Pasien Klinik';
        $data['list_klinik'] = $this->common_model->select_active('klinik');
        $data['list_advertise'] = $this->common_model->select_active('advertise');
        $data['list_gender'] = $this->common_model->select_active('gender');
        $data['list_emosional'] = $this->common_model->select_active('emosional');
        $data['list_pasien_status_klinik'] = $this->common_model->select_active('pasien_status_klinik');
        $data['data'] = $this->common_model->get_id($id,'pasien_klinik');
        $data['main_content'] = $this->load->view('kobra/pasien-klinik/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function reqsapproval($id){
        if ($_POST) {
            
            $result_pasien = $this->common_model->get_opt("md5(id)", $_POST['approval_code'], 'interaksi_klinik');
            
            $data = array(
                'approval_code' => $_POST['approval_code'],
                'approval_status' => $_POST['approval_status']
            );

            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'pasien_klinik');
            
            $data = array(
                'closing_by' => $this->session->userdata('name'),
                'closing_status' => true,
                'closing_date' => current_datetime()
            );
            $this->common_model->edit_option($data, $result_pasien->id, 'interaksi_klinik');


            $this->session->set_flashdata('msg', 'Information Approval Successfully');
            redirect(base_url('kobra/PasienKlinik/approval'));
        }
        
        $pasien = $this->common_model->get_opt('id',$id,'pasien_klinik');
        
		$select = 	"*";
		$table	= 'interaksi_klinik';
		$where	= "1=1 AND interaksi_klinik.confirm_status = 'Berkunjung' AND (interaksi_klinik.name like '%".$pasien->name."%')";
		$group	= "";
		$order	= "interaksi_klinik.confirm_date ASC";

        $data['services'] = 'kobra/PasienKlinik/';
        $data['page_title'] = 'Approval Pasien';
		$data['list_pasien_closing'] = $this->common_model->select_other($select, $where, $group, $order, $table);
        $data['list_approval_status'] = $this->common_model->select_active('approval_status');
        $data['data'] = $this->common_model->get_id($id,'pasien_klinik');
        $data['main_content'] = $this->load->view('kobra/pasien-klinik/edit_approval', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'pasien_klinik');
        $this->session->set_flashdata('msg', 'Pasien Klinik active Successfully');
        redirect(base_url('kobra/PasienKlinik'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'pasien_klinik');
        $this->session->set_flashdata('msg', 'Pasien Klinik deactive Successfully');
        redirect(base_url('kobra/PasienKlinik'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'pasien_klinik'); 
        $this->session->set_flashdata('msg', 'Pasien Klinik deleted Successfully');
        redirect(base_url('kobra/PasienKlinik'));
    }


}