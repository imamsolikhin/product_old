<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PasienStatusKlinik extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'pasien_status_klinik';
        $data['services'] = 'kobra/PasienStatusKlinik/';
        $data['page_title'] = 'Pasien Status Klinik';
        $data['main_content'] = $this->load->view('kobra/pasien-status-klinik/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $PasienStatusKlinik = $this->common_model->get_opt('name',$_POST['name'],'pasien_status_klinik');

            if (empty($PasienStatusKlinik)) {
                $PasienStatusKlinik = $this->common_model->insert($data, 'pasien_status_klinik');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/PasienStatusKlinik'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/PasienStatusKlinik/';
        $data['page_title'] = 'Pasien Status Klinik';
        $data['main_content'] = $this->load->view('kobra/pasien-status-klinik/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'pasien_status_klinik');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/PasienStatusKlinik'));
        }
        $data['services'] = 'kobra/PasienStatusKlinik/';
        $data['page_title'] = 'Pasien Status Klinik';
        $data['data'] = $this->common_model->get_id($id,'pasien_status_klinik');
        $data['main_content'] = $this->load->view('kobra/pasien-status-klinik/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'pasien_status_klinik');
        $this->session->set_flashdata('msg', 'PasienStatusKlinik active Successfully');
        redirect(base_url('kobra/PasienStatusKlinik'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'pasien_status_klinik');
        $this->session->set_flashdata('msg', 'PasienStatusKlinik deactive Successfully');
        redirect(base_url('kobra/PasienStatusKlinik'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'pasien_status_klinik'); 
        $this->session->set_flashdata('msg', 'PasienStatusKlinik deleted Successfully');
        redirect(base_url('kobra/PasienStatusKlinik'));
    }


}