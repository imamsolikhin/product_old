<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->library('TCPDF');
        $this->load->model('common_model');
        $this->load->model('report_model');
        $this->load->model('login_model');
    }
    
    public function klinik(){
        $data["data"] ="";
        if ($_POST) {
            if(!empty($_POST['start_date']) && !empty($_POST['end_date'])){
                $where = "* FROM interaksi_klinik WHERE 1=1 AND DATE(interaksi_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
                $result_berkunjung = $this->report_model->select($where,"interaksi_klinik.advertise ASC");

                $where = "* FROM interaksi_klinik WHERE 1=1 AND (interaksi_klinik.phone != '' OR interaksi_klinik.phone_wa != '') AND DATE(interaksi_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."')  ";
                $result_WA = $this->report_model->select($where,"interaksi_klinik.advertise");

                $where = "* FROM interaksi_klinik WHERE 1=1 AND (interaksi_klinik.phone != '' OR interaksi_klinik.phone_wa != '') AND confirm_status = 'Di Jawab' AND DATE(interaksi_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."')  ";
                $result_tlp = $this->report_model->select($where,"interaksi_klinik.advertise");

                $where = "* FROM interaksi_klinik WHERE 1=1 AND (interaksi_klinik.phone != '' OR interaksi_klinik.phone_wa != '') AND confirm_status != 'Di Jawab' AND DATE(interaksi_klinik.confirm_date) <= DATE(interaksi_klinik.created_at) AND DATE(interaksi_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."')  ";
                $result_belum_resp = $this->report_model->select($where,"interaksi_klinik.advertise");

                $where = "* FROM interaksi_klinik WHERE 1=1 AND (interaksi_klinik.phone != '' OR interaksi_klinik.phone_wa != '') AND confirm_status = 'Di Jawab' AND DATE(interaksi_klinik.confirm_date) >= DATE(interaksi_klinik.created_at) AND DATE(interaksi_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."')  ";
                $result_janji = $this->report_model->select($where,"interaksi_klinik.advertise");

                $where = "count(interaksi_klinik.id) AS jumlah, interaksi_klinik.* FROM interaksi_klinik WHERE 1=1 AND DATE(interaksi_klinik.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."')   GROUP BY interaksi_klinik.advertise";
                $result_resume_iklan = $this->report_model->select($where);

                $data["data"] .= '
                <label style="font-weight:900;">
                             *LAPORAN HASIL IKLAN '.strtoupper($this->input->post('klinik')).'*
                        <br> *Pesan Masuk Messengger : '.$nilai = (!$result_berkunjung) ? 0:count($result_berkunjung).'*		
                        <br> *WA : '.$nilai = (!$result_WA) ? 0:count($result_WA).' Tlp : '.$nilai = (!$result_tlp) ? 0:count($result_tlp).' Janji : '.$nilai = (!$result_janji) ? 0:count($result_janji).'* 
                        <br> *Tanggal Mulai: '.date("d-m-Y",strtotime($this->input->post('start_date'))).'*  
                        <br> *Tanggal Akhir: '.date("d-m-Y",strtotime($this->input->post('end_date'))).'*   	
                        <br> *JENIS IKLAN : Carosel, Lead Form, Video Ads dan Video Skip Ads*
                </label>
                ';
                // echo $data["data"];
                $title = "";
                $descrption = "";
                if($result_resume_iklan){
                    $no=1;
                    foreach ($result_resume_iklan as $pasien_list_group) {
                        // echo var_dump($pasien_list_group);
                        $data["data"] .= '<br><br><label style="font-weight:900;">*IKLAN: '.$pasien_list_group->advertise.'*</label>';
                        $jml=0;
                        if($result_berkunjung){
                            $i=1;
                            foreach ($result_berkunjung as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' '.$pasien_list->gender.' '.$pasien_list->name.' '.date("h:i", strtotime($pasien_list->created_at));
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><label style="font-weight:900;">*1. Jumlah yg berkunjung : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_WA){
                            $i=1;
                            foreach ($result_WA as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Iklan: '.$pasien_list->id_iklan;
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><br><label style="font-weight:900;">*2. Jumlah Nomor Yang Didapat : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_tlp){
                            $i=1;
                            foreach ($result_tlp as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Iklan: '.$pasien_list->id_iklan;
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><br><label style="font-weight:900;">*3.  jumlah yg sudah merespon : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_belum_resp){
                            $i=1;
                            foreach ($result_belum_resp as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Ket: '.$pasien_list->remark;
                                    $i++;
                                }
                            }
                        }
                        $title .= '<br><br><label style="font-weight:900;">*4.  Jumlah yg belum merespon : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        if($result_janji){
                            $i=1;
                            foreach ($result_janji as $pasien_list) {
                                if ($pasien_list->advertise == $pasien_list_group->advertise) {
                                    $jml++;
                                    $descrption .= '<br>&nbsp;'.$i.' Nama: '.$pasien_list->gender.' '.$pasien_list->name;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Keluhan: '.$pasien_list->complain;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Alamat: '.$pasien_list->address;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$pasien_list->phone;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   No.WA: '.$pasien_list->phone_wa;
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Jadwal: '.date("d-m-Y H:i", strtotime($pasien_list->confirm_date));
                                    $descrption .= '<br>&nbsp;&nbsp;&nbsp;   Iklan: '.$pasien_list->id_iklan;
                                    $i++;
                                }
                            }
                        }

                        $title .= '<br><br><label style="font-weight:900;">*5.  Jumlah yg janji pasien : '.$jml.'*</label>';
                         $data["data"] .= $title;
                         $data["data"] .= $descrption;

                        $title ="";
                        $descrption ="";

                        $i=0;
                        $jml=0;

                        $no++;
                    }
                }

                $data["data"] .= '<br><br><label style="font-weight:900;">*RESUME SEMUA IKLAN*</label>';
                $i=1;
                if($result_resume_iklan){
                    foreach ($result_resume_iklan as $pasien_list) {
                        $data["data"] .= '<br>&nbsp;'.($i).'.) '.$pasien_list->advertise.': '.$pasien_list->jumlah;
                        $i++;
                    }
                }

                $data["data"] .= '<br><br>--------------------------------------';
                $data["data"] .= '<br><label style="font-weight:900;">*Admin: '.$this->session->userdata('name').'*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label style="font-weight:900;">*Terimakasih*</label>';
            }
        }

        $data['services'] = 'kobra/Report/';
        $data['page_title'] = 'Laporan Interaksi Klinik';
        $data['list_klinik'] = $this->common_model->select_active('klinik');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['main_content'] = $this->load->view('kobra/report/klinik', $data, TRUE);
        $this->load->view('index', $data);
    }

	public function report_klinik(){
        if(!$_GET){
            $data['services'] = 'kobra/Report/';
            $data['page_title'] = 'LAPORAN MARKETING KLINIK';
            $data['list_klinik'] = $this->common_model->select_active('klinik');
            $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
            $data['main_content'] = $this->load->view('kobra/report/report_klinik', $data, TRUE);
            $this->load->view('index', $data);
        }else{   
            $data['data_company'] = $this->common_model->get_opt('code','KDM','setup');;
            $data['page_title'] = 'Laporan Marketing Klinik';
            $this->load->view('kobra/report/report_klinik_cetak',$data,false);
        }
    }
    
    public function produk(){
        $data["data"] ="";
        $data["data_header"] ="";
        $data["data_detail"] ="";
        if ($_POST) {
            if(!empty($_POST['start_date']) && !empty($_POST['end_date'])){
                $data["data"] .= '
                <label style="font-weight:900;">
                             *LAPORAN HASIL '.strtoupper($this->input->post('advertise_produk')).' '.strtoupper($this->input->post('produk')).'*
                        <br> *Nama CS : '.$this->session->userdata('name').'*		
                        <br> *Tanggal Mulai: '.date("d-m-Y",strtotime($this->input->post('start_date'))).'*  
                        <br> *Tanggal Akhir: '.date("d-m-Y",strtotime($this->input->post('end_date'))).'*         
                        <br> *'.$this->input->post('shipkerja').'*		
                </label>
                ';
                
                $title = "";
                $descrption = "";
                $data["data_header"] .= '<br><br><label style="font-weight:900;">*RESUME HASIL '.strtoupper($this->input->post('advertise_produk')).'*</label>';
                
                $where = "interaksi_produk.* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND advertise = '".$this->input->post('advertise_produk')."'";
                $result_list_data = $this->report_model->select($where);
                $jml=0;
                if($result_list_data){
                    $i=0;
                    foreach ($result_list_data as $result_data) {
                        $i++;
                        $descrption .= '<br>&nbsp;'.$i.' '.strtolower($result_data->jenis_kelamin.' '.$result_data->nama_lengkap);
                        $descrption .= '<br>&nbsp;&nbsp;&nbsp; '.strtolower($result_data->tlp);
                    }
                    $jml=$i;
                }
                
                $data["data_detail"] .= '<br>';
                $title = '<br><label style="font-weight:900;">*Data '.strtolower($this->input->post('advertise_produk')).' yang masuk.*</label>';
                $data["data_header"] .= '<br><label style="font-weight:900;">*1.) '.strtolower($this->input->post('advertise_produk')).' yang masuk : '.$jml.'*</label>';
                $data["data_detail"] .= $title;
                $data["data_detail"] .= $descrption;
                $title = "";
                $descrption = "";
                
                $where = "interaksi_produk.* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND advertise = '".$this->input->post('advertise_produk')."' ";
                $result_list_data = $this->report_model->select($where);
                $jml=0;
                if($result_list_data){
                    $i=0;
                    foreach ($result_list_data as $result_data) {
                        $i++;
                        $descrption .= '<br>&nbsp;'.$i.' '.strtolower($result_data->jenis_kelamin.' '.$result_data->nama_lengkap);
                        $descrption .= '<br>&nbsp;&nbsp;&nbsp; '.strtolower($result_data->tlp);
                    }
                    $jml=$i;
                }
                
                $data["data_detail"] .= '<br>';
                $title .= '<br><label style="font-weight:900;">*Data '.strtolower($this->input->post('advertise_produk')).' yang dibalas.*</label>';
                $data["data_header"] .= '<br><label style="font-weight:900;">*2.) '.strtolower($this->input->post('advertise_produk')).' yang dibalas : '.$jml.'*</label>';
                $data["data_detail"] .= $title;
                $data["data_detail"] .= $descrption;
                $title = "";
                $descrption = "";
                
                $where = "interaksi_produk.* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND advertise = '".$this->input->post('advertise_produk')."' AND interaksi_produk.confirm_status in('Merespon','Closing','Tidak Closing')";
                $result_list_data = $this->report_model->select($where);
                $jml=0;
                if($result_list_data){
                    $i=0;
                    foreach ($result_list_data as $result_data) {
                        $i++;
                        $descrption .= '<br>&nbsp;'.$i.' '.strtolower($result_data->jenis_kelamin.' '.$result_data->nama_lengkap);
                        $descrption .= '<br>&nbsp;&nbsp;&nbsp; '.strtolower($result_data->tlp);
                    }
                    $jml=$i;
                }
                
                $data["data_detail"] .= '<br>';
                $title = '<br><label style="font-weight:900;">*Data '.strtolower($this->input->post('advertise_produk')).' yang merespon.*</label>';
                $data["data_header"] .= '<br><label style="font-weight:900;">*3.) '.strtolower($this->input->post('advertise_produk')).' yang merespon : '.$jml.'*</label>';
                $data["data_detail"] .= $title;
                $data["data_detail"] .= $descrption;
                $title = "";
                $descrption = "";
                
                
                $where = "interaksi_produk.* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND advertise = '".$this->input->post('advertise_produk')."' AND interaksi_produk.confirm_status in('Tidak Merespon','Tidak Closing')";
                $result_list_data = $this->report_model->select($where);
                $jml=0;
                if($result_list_data){
                    $i=0;
                    foreach ($result_list_data as $result_data) {
                        $i++;
                        $descrption .= '<br>&nbsp;'.$i.' '.strtolower($result_data->jenis_kelamin.' '.$result_data->nama_lengkap);
                        $descrption .= '<br>&nbsp;&nbsp;&nbsp; '.strtolower($result_data->tlp);
                    }
                    $jml=$i;
                }
                
                $data["data_detail"] .= '<br>';
                $title = '<br><label style="font-weight:900;">*Data '.strtolower($this->input->post('advertise_produk')).' yang tidak merespon.*</label>';
                $data["data_header"] .= '<br><label style="font-weight:900;">*4.) '.strtolower($this->input->post('advertise_produk')).' yang tidak merespon : '.$jml.'*</label>';
                $data["data_detail"] .= $title;
                $data["data_detail"] .= $descrption;
                $title = "";
                $descrption = "";
                
                $where = "interaksi_produk.* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND advertise = '".$this->input->post('advertise_produk')."' AND interaksi_produk.confirm_status in('Tidak Merespon','Tidak Closing')";
                $result_list_data = $this->report_model->select($where);
                $jml=0;
                if($result_list_data){
                    $i=0;
                    foreach ($result_list_data as $result_data) {
                        $i++;
                        $descrption .= '<br>&nbsp;'.$i.' '.strtolower($result_data->jenis_kelamin.' '.$result_data->nama_lengkap);
                        $descrption .= '<br>&nbsp;&nbsp;&nbsp; '.strtolower($result_data->tlp);
                    }
                    $jml=$i;
                }
                
                $data["data_detail"] .= '<br>';
                $title = '<br><label style="font-weight:900;">*Data '.strtolower($this->input->post('advertise_produk')).' yang tidak Closing.*</label>';
                $data["data_header"] .= '<br><label style="font-weight:900;">*5.) '.strtolower($this->input->post('advertise_produk')).' yang tidak Closing : '.$jml.'*</label>';
                $data["data_detail"] .= $title;
                $data["data_detail"] .= $descrption;
                $title = "";
                $descrption = "";
                
                
                $where = "interaksi_produk.* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND advertise = '".$this->input->post('advertise_produk')."' AND interaksi_produk.confirm_status in('Closing')";
                $result_list_data = $this->report_model->select($where);
                $jml=0;
                $jml_btl=0;
                if($result_list_data){
                    $i=0;
                    foreach ($result_list_data as $customer_list) {
                        $i++;
                        $jml_btl = $jml_btl+(int)$customer_list->jml_pesanan;
                        $descrption .= '<br>&nbsp;*'.$i.' Nama Customer:* '.$customer_list->jenis_kelamin.' '.$customer_list->nama_lengkap;
                        $descrption .= '<br>&nbsp;&nbsp;&nbsp;   *No.Tlp:* '.$customer_list->tlp;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Alamat:* '.$customer_list->alamat;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Rt/Rw:* '.$customer_list->rt.'/'.$customer_list->rw;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *No Rumah/Kantor:* '.$customer_list->no_rumah;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Desa:* '.$customer_list->desa;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Patokan:* '.$customer_list->patokan;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Kelurahan:* '.$customer_list->kelurahan;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Kecamatan:* '.$customer_list->kecamatan;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Kota:* '.$customer_list->kota;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Provinsi:* '.$customer_list->provinsi;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Kode Pos:* '.$customer_list->kode_pos;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Berikut data Pesanan:* ';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *No. Pesanan :* '.$customer_list->delivery_order;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Jumlah Pemesanan :* '.(int)$customer_list->jml_pesanan.' ['.$customer_list->customer_status.']';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Biaya Pesanan :*  Rp ' . number_format(($customer_list->jml_pesanan*$customer_list->harga),2,',','.');
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Biaya Pengiriman :*  Rp ' . number_format($customer_list->biaya_pengiriman,2,',','.');
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Biaya Asuransi :*  Rp ' . number_format($customer_list->asuransi,2,',','.');
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Total Pembayaran :*  Rp ' . number_format($customer_list->total_transaksi,2,',','.');
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Jenis transaksi :* '.$customer_list->type_transaction.'['.$customer_list->bank.']';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Kurir Pengiriman :* '.$customer_list->kurir;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Tanggal Order :* '.date("d-m-Y",strtotime($customer_list->tanggal_order));
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Estimasi Sampai :* '.date("d-m-Y",strtotime($customer_list->estimasi_sampai));
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *No. Resi:* '.$customer_list->delivery_resi;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *No. Ref:* '.$customer_list->delivery_ref;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Status pemesanan:* '.$customer_list->delivery_status;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Noted:* '.$customer_list->closing_remark;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   ';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *ACC*';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *CS Marketing'.$customer_list->produk.' ✔*';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Nama :* '.$customer_list->created_by;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   ';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *ACC*';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Admin Marketing'.$customer_list->produk.' ✔*';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Nama :* '.$customer_list->admin_confirm_by;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *ACC*';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Admin Gudang '.$customer_list->produk.' ✔*';
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   *Nama :* '.$customer_list->gudang_confirm_by;
						$descrption .= '<br>&nbsp;&nbsp;&nbsp;   ';
                    }
                    $jml=$i;
                }
                
                $data["data_detail"] .= '<br>';
                $title = '<br><label style="font-weight:900;">*Data '.strtolower($this->input->post('advertise_produk')).' yang closing.*</label>';
                $data["data_header"] .= '<br><label style="font-weight:900;">*6.) '.strtolower($this->input->post('advertise_produk')).' yang Closing : '.$jml.'*</label>';
                $data["data_detail"] .= $title;
                $data["data_detail"] .= $descrption;
                $title = "";
                $descrption = "";

                // $data["data"] .= '<br><br><label style="font-weight:900;">*RESUME SEMUA IKLAN*</label>';
                // $i=1;
                // if($result_resume_iklan){
                //     foreach ($result_resume_iklan as $customer_list) {
                //         $data["data"] .= '<br>&nbsp;'.($i).'.) '.$customer_list->advertise.': '.$customer_list->jumlah;
                //         $i++;
                //     }
                // }
                
                

                // $data["data"] .= '<br><br>---------------------------------------';
                // $data["data"] .= '<br><br><label style="font-weight:900;">*RESUME BONUS '.strtoupper($this->input->post('advertise_produk')).'*</label>';
                // $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;*-<label style="font-weight:600;">Estimasi Closing '.strtolower($this->input->post('advertise_produk')).' : '.$jml_interaksi.' Customer*</label>';
                // $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;*-<label style="font-weight:600;">Tidak Closing '.strtolower($this->input->post('advertise_produk')).' : '.($jml_tlp-$jml_cust_closing).' Customer*</label>';
                // $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;*-<label style="font-weight:600;">Sukses Closing '.strtolower($this->input->post('advertise_produk')).' : '.$jml_cust_closing.' Customer*</label>';
                // $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;*-<label style="font-weight:600;">Bonus Closing '.strtolower($this->input->post('advertise_produk')).' : '.$jml_sale.' Btl (Rp ' . number_format(($jml_sale*10000),2,',','.').')*</label>';
            
                $data["data"] .= $data["data_header"];
                
                $data["data"] .= '<br><br><label style="font-weight:900;">*RESUME BONUS '.strtoupper($this->input->post('advertise_produk')).'*</label>';
                $data["data"] .= '<br><label style="font-weight:800;">*Jumlah Bonus Closing*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;<label style="font-weight:600;">*Hari ini : Rp '.number_format(($jml_btl*10000),2,',','.').'*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;<label style="font-weight:600;">*Dari : '.$jml_btl.' X Rp '.number_format(10000,2,',','.').'*</label>';
                $data["data"] .= '<br><label style="font-weight:800;">*Jumlah Bonus Iklan*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;<label style="font-weight:600;">*Kemarin : Rp (?)*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;<label style="font-weight:600;">*Dari : (?) X Rp '.number_format(10000,2,',','.').'*</label>';
                $data["data"] .= '<br><label style="font-weight:800;">*Jumlah Bonus Hari Ini dan Kemarin*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;<label style="font-weight:600;">*Total : Rp (?)*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;<label style="font-weight:600;">*Dari : (?) X Rp '.number_format(10000,2,',','.').'*</label>';
                
                $data["data"] .= $data["data_detail"];
                
                $data["data"] .= '<br><br>---------------------------------------';
                $data["data"] .= '<br><label style="font-weight:900;">*Demikian laporan hasil '.strtolower($this->input->post('advertise_produk')).' ini saya buat dengan sejujur nya sebagai pertanggung jawaban kerja saya dan di pastikan laporan tidak ada yang keliru dan salah .*</label>';
                $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label style="font-weight:900;">*Terimakasih*</label>';
                $data["data"] .= '<br><br><label style="font-weight:900;">*Ttd*</label>';
                $data["data"] .= '<br><label style="font-weight:900;">*'.$this->session->userdata('name').'*</label>';
                $data["data"] .= '<br><br><label style="font-weight:900;">*Telah di cek oleh*</label>';
                $data["data"] .= '<br><label style="font-weight:900;">*Bu Ira:*</label>';
                $data["data"] .= '<br><label style="font-weight:900;">*Ummi:*</label>';
            }
        }

        $data['services'] = 'kobra/Report/';
        $data['page_title'] = 'Laporan Interaksi Produk';
        $data['list_produk'] = $this->common_model->select_active('produk');
        $data['list_advertise_produk'] = $this->common_model->select_active('advertise_produk');
        $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
        $data['main_content'] = $this->load->view('kobra/report/produk', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function report_produk(){
        if(!$_GET){
            $data['services'] = 'kobra/Report/';
            $data['page_title'] = 'LAPORAN MARKETING PRODUK';
            $data['list_produk'] = $this->common_model->select_active('produk');
            $data['list_advertise_produk'] = $this->common_model->select_active('advertise_produk');
            $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
            $data['main_content'] = $this->load->view('kobra/report/report_produk', $data, TRUE);
            $this->load->view('index', $data);
        }else{   
            $data['data_company'] = $this->common_model->get_opt('code','KDM','setup');;
            $data['page_title'] = 'Laporan Marketing Klinik';
            $this->load->view('kobra/report/report_produk_cetak',$data,false);
        }
    }

    public function report_cs_produk(){
        if($_POST){
                $where = "COUNT(interaksi_produk.id) AS jml_customer,SUM(interaksi_produk.jml_pesanan) AS jml_produk,SUM(interaksi_produk.total_transaksi) AS grand_total,interaksi_produk.akun_market,interaksi_produk.kurir FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."'  AND type_transaction='".$this->input->post('payment_method')."'  GROUP BY interaksi_produk.akun_market,interaksi_produk.kurir";
                $result_closing = $this->report_model->select($where,"interaksi_produk.akun_market ASC, interaksi_produk.kurir ASC");

                // KALKULASI
                $where = "SUM(interaksi_produk.jml_pesanan) AS jumlah FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND reqs_status='YES' AND type_transaction='".$this->input->post('payment_method')."'";
                $result_resume_product = $this->report_model->get_select($where);
                
                $where = "SUM(interaksi_produk.total_transaksi) AS jumlah, SUM((interaksi_produk.biaya_pengiriman+interaksi_produk.asuransi)) AS jumlah_ongkir FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND reqs_status='YES' AND reqs_status='YES' AND type_transaction='".$this->input->post('payment_method')."'";
                $result_resume_grand_total = $this->report_model->get_select($where);
                
                $data["data"]  = '';
                $data["data"] .= '
                <label style="font-weight:900;">
                             *LAPORAN CUSTOMER '.strtoupper($this->input->post('produk')).'*
                        <br> *Metode Pembayaran :'.strtoupper($this->input->post('payment_method')).'* 
                        <br> *Jml Botol : '.$result_resume_product = (!$result_resume_product) ? 0:(int)$result_resume_product->jumlah.' Botol* 
                        <br> *Transaksi : '.$result_resume_grand_total = (!$result_resume_grand_total) ? 0:'Rp. '.number_format(($result_resume_grand_total->jumlah-$result_resume_grand_total->jumlah_ongkir),0,',','.').'*  
                        <br> *Ongkir : '.$result_resume_grand_total = (!$result_resume_grand_total) ? 0:'Rp. '.number_format($result_resume_grand_total->jumlah_ongkir,0,',','.').'*  
                        <br> *Total Transaksi : '.$result_resume_grand_total = (!$result_resume_grand_total) ? 0:'Rp. '.number_format($result_resume_grand_total->jumlah,0,',','.').'*  
                        <br> *Tanggal Mulai: '.date("d-m-Y",strtotime($this->input->post('start_date'))).'*  
                        <br> *Tanggal Akhir: '.date("d-m-Y",strtotime($this->input->post('end_date'))).'*        
                        <br> *'.$this->input->post('shipkerja').'*      
                </label>
                ';

                $list_market = $this->common_model->select_active('akun_market');
                $list_kurir = $this->common_model->select_active('kurir');
                if($result_closing){
                    $no=1;
                    foreach ($list_market as $market) {
                        // echo var_dump($customer_list_group);
                        $data["data"] .= '<br><br><label style="font-weight:900;">*AKUN MARKET: '.$market['name'].'*</label>';
                        $jml=0;
                        if($result_closing){
                            $i=1;
                            foreach ($list_kurir as $kurir) {
                                $data["data"] .= '<br><label style="font-weight:600;">&nbsp;*'.$i.'. Kurir: '.$kurir['name'].'*</label>';
                                foreach ($result_closing as $customer_list) {
                                        // echo var_dump($result_closing);die;
                                    // if ($customer_list->akun_market == $market['name']) {
                                        // foreach ($result_closing as $customer_list) {
                                            if ($customer_list->akun_market == $market['name'] && $customer_list->kurir == $kurir['name']) {
                                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Penjualan '.(int)$customer_list->jml_produk.' ['.(int)$customer_list->jml_customer.' Order] = '.'Rp. '.number_format($customer_list->grand_total,0,',','.');
                                            }
                                        // }
                                    // }
                                }
                            $i++;
                            }
                        }
                    }
                }


        $where = "barang_masuk.* FROM barang_masuk WHERE 1=1 AND DATE(barang_masuk.tanggal) BETWEEN DATE('".$this->input->post('start_date_barang')."') AND DATE('".$this->input->post('end_date_barang')."') AND branch = '".$this->session->userdata('branch')."' AND produk = '".$this->input->post('produk')."' ORDER BY barang_masuk.tanggal DESC";
        $result_barang_masuk = $this->report_model->select($where);


        $where = "barang_keluar.* FROM barang_keluar WHERE 1=1 AND DATE(barang_keluar.tanggal) BETWEEN DATE('".$this->input->post('start_date_barang')."') AND DATE('".$this->input->post('end_date_barang')."') AND branch = '".$this->session->userdata('branch')."' AND produk = '".$this->input->post('produk')."'  ORDER BY barang_keluar.tanggal DESC";
        $result_barang_keluar = $this->report_model->select($where);


            $data["data"] .= '<br><br>';
            $data["data"] .= '<br><label style="font-weight:900;">*LAPORAN BARANG MASUK '.strtoupper($this->input->post('produk')).'*</label>';
            if($result_barang_masuk){
                $no=1;
                foreach ($result_barang_masuk as $barang_masuk) {                        
                    $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;- '.date("d-m-Y",strtotime($barang_masuk->tanggal)).' = '.(int)$barang_masuk->total.' '.$barang_masuk->remark;
                    $data["data"] .= '<br>';
                }
            }else{
                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;- tidak ada barang masuk';
            }

            $data["data"] .= '<br><label style="font-weight:900;">*LAPORAN BARANG KELUAR '.strtoupper($this->input->post('produk')).'*</label>';
            if($result_barang_keluar){
                $no=1;
                foreach ($result_barang_keluar as $barang_keluar) {
                    $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;- '.date("d-m-Y",strtotime($barang_keluar->tanggal)).' = '.(int)$barang_keluar->total.' '.$barang_keluar->remark;
                    $data["data"] .= '<br>';
                }
            }else{
                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;- tidak ada barang masuk';
            }


            $data["data"] .= '<br><br>--------------------------------------';
            $data["data"] .= '<br><label style="font-weight:900;">*Admin: '.$this->session->userdata('name').'*</label>';
            $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label style="font-weight:900;">*Terimakasih*</label>';


            $data['services'] = 'kobra/Report/';
            $data['page_title'] = 'LAPORAN CS PRODUK';
            $data['list_produk'] = $this->common_model->select_active('produk');
            $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
            $data['main_content'] = $this->load->view('kobra/report/report_cs_produk', $data, TRUE);
            $this->load->view('index', $data);
        }else{   
            $data['services'] = 'kobra/Report/';
            $data['page_title'] = 'LAPORAN CS PRODUK';
            $data['list_produk'] = $this->common_model->select_active('produk');
            $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
            $data['main_content'] = $this->load->view('kobra/report/report_cs_produk', $data, TRUE);
            $this->load->view('index', $data);
        }
    }

    public function report_cs_customer(){
        if($_POST){
                $where = "* FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND reqs_status='YES' AND reqs_status='YES' AND type_transaction='".$this->input->post('payment_method')."'";
                $result_closing = $this->report_model->select($where,"interaksi_produk.nama_lengkap ASC, interaksi_produk.tanggal_order ASC");

                // KALKULASI
                $where = "SUM(interaksi_produk.jml_pesanan) AS jumlah FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND reqs_status='YES' AND type_transaction='".$this->input->post('payment_method')."'";
                $result_resume_product = $this->report_model->get_select($where);
                
                $where = "SUM(interaksi_produk.total_transaksi) AS jumlah, SUM((interaksi_produk.biaya_pengiriman+interaksi_produk.asuransi)) AS jumlah_ongkir FROM interaksi_produk WHERE 1=1 AND DATE(interaksi_produk.created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') AND branch = '".$this->session->userdata('branch')."' AND ship_kerja = '".$this->input->post('shipkerja')."' AND produk = '".$this->input->post('produk')."' AND reqs_status='YES' AND reqs_status='YES' AND type_transaction='".$this->input->post('payment_method')."'";
                $result_resume_grand_total = $this->report_model->get_select($where);

                $data["data"]  = '';
                $data["data"] .= '
                <label style="font-weight:900;">
                             *LAPORAN PENJUALAN '.strtoupper($this->input->post('produk')).'*
                        <br> *Metode Pembayaran :'.strtoupper($this->input->post('payment_method')).'* 
                        <br> *Jml Botol : '.$result_resume_product = (!$result_resume_product) ? 0:(int)$result_resume_product->jumlah.' Botol* 
                        <br> *Transaksi : '.$result_resume_grand_total = (!$result_resume_grand_total) ? 0:'Rp. '.number_format(($result_resume_grand_total->jumlah-$result_resume_grand_total->jumlah_ongkir),0,',','.').'*  
                        <br> *Ongkir : '.$result_resume_grand_total = (!$result_resume_grand_total) ? 0:'Rp. '.number_format($result_resume_grand_total->jumlah_ongkir,0,',','.').'*  
                        <br> *Total Transaksi : '.$result_resume_grand_total = (!$result_resume_grand_total) ? 0:'Rp. '.number_format($result_resume_grand_total->jumlah,0,',','.').'*  
                        <br> *Tanggal Mulai: '.date("d-m-Y",strtotime($this->input->post('start_date'))).'*  
                        <br> *Tanggal Akhir: '.date("d-m-Y",strtotime($this->input->post('end_date'))).'*        
                        <br> *'.$this->input->post('shipkerja').'*      
                </label>
                ';


                $list_user = $this->report_model->select("* FROM user WHERE 1=1 AND (role='gudang-produk' OR role='cs-produk' OR role='admin-produk') AND branch='".$this->session->userdata('branch')."'");
                $no=1;
                foreach ($list_user as $user) {
                    if($result_closing){
                        $i=1;
                        $data["data"] .= '<br><br>';
                        $data["data"] .= '<br><label style="font-weight:900;">*'.$no.'.) CS '.$user->first_name.' '.$user->last_name.'*</label>';
                        $botol = 0;
                        $ongkir = 0;
                        $total = 0;
                        foreach ($result_closing as $customer_list) {
                            if ($customer_list->created_by == ($user->first_name.' '.$user->last_name)) {
                                 $data["data"] .= '<br>&nbsp;'.$i.' Nama: '.$customer_list->jenis_kelamin.' '.$customer_list->nama_lengkap;
                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;   No.Tlp: '.$customer_list->tlp.'/'.$customer_list->wa;
                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;   Jumlah: '.(int)$customer_list->jml_pesanan." Botol [".$customer_list->kurir."]";
                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;   Ongkir: Rp. '.number_format($customer_list->biaya_pengiriman,0,',','.');
                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;   Asuransi: Rp. '.number_format($customer_list->asuransi,0,',','.');
                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;   Total: '.$customer_list->total_transaksi_id;
                                 $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;   Bank: '.$customer_list->bank;
                                 $data["data"] .= '<br>';
                                $i++;
                                $botol = $botol+$customer_list->jml_pesanan;
                                $ongkir = $ongkir+($customer_list->biaya_pengiriman+$customer_list->asuransi);
                                $total = $total+$customer_list->total_transaksi;
                            }
                        }
                        if((int)$botol!=0){
                            $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;<label style="font-weight:600;">*Total Botol: Rp. '.number_format($botol,0,',','.')." Botol*</label>";
                            $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;<label style="font-weight:600;">*Total Ongkir: Rp. '.number_format($ongkir,0,',','.')."*</label>";
                            $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;<label style="font-weight:600;">*Total Transaksi: Rp. '.number_format($total,0,',','.')."*</label>";
                        }
                    }
                $no++;
                }


            $data["data"] .= '<br><br>--------------------------------------';
            $data["data"] .= '<br><label style="font-weight:900;">*Admin: '.$this->session->userdata('name').'*</label>';
            $data["data"] .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label style="font-weight:900;">*Terimakasih*</label>';


            $data['services'] = 'kobra/Report/';
            $data['page_title'] = 'LAPORAN CS CUSTOMER';
            $data['list_produk'] = $this->common_model->select_active('produk');
            $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
            $data['main_content'] = $this->load->view('kobra/report/report_cs_customer', $data, TRUE);
            $this->load->view('index', $data);
        }else{   
            $data['services'] = 'kobra/Report/';
            $data['page_title'] = 'LAPORAN CS CUSTOMER';
            $data['list_produk'] = $this->common_model->select_active('produk');
            $data['list_ship_kerja'] = $this->common_model->select_active('ship_kerja');
            $data['main_content'] = $this->load->view('kobra/report/report_cs_customer', $data, TRUE);
            $this->load->view('index', $data);
        }
    }

}