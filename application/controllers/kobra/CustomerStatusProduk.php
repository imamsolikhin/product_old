<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CustomerStatusProduk extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'customer_status_produk';
        $data['services'] = 'kobra/CustomerStatusProduk/';
        $data['page_title'] = 'Customer Status Produk';
        $data['main_content'] = $this->load->view('kobra/customer-status-produk/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $CustomerStatusProduk = $this->common_model->get_opt('name',$_POST['name'],'customer_status_produk');

            if (empty($CustomerStatusProduk)) {
                $CustomerStatusProduk = $this->common_model->insert($data, 'customer_status_produk');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/CustomerStatusProduk'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/CustomerStatusProduk/';
        $data['page_title'] = 'Customer Status Produk';
        $data['main_content'] = $this->load->view('kobra/customer-status-produk/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'customer_status_produk');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/CustomerStatusProduk'));
        }
        $data['services'] = 'kobra/CustomerStatusProduk/';
        $data['page_title'] = 'Customer Status Produk';
        $data['data'] = $this->common_model->get_id($id,'customer_status_produk');
        $data['main_content'] = $this->load->view('kobra/customer-status-produk/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'customer_status_produk');
        $this->session->set_flashdata('msg', 'CustomerStatusProduk active Successfully');
        redirect(base_url('kobra/CustomerStatusProduk'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'customer_status_produk');
        $this->session->set_flashdata('msg', 'CustomerStatusProduk deactive Successfully');
        redirect(base_url('kobra/CustomerStatusProduk'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'customer_status_produk'); 
        $this->session->set_flashdata('msg', 'CustomerStatusProduk deleted Successfully');
        redirect(base_url('kobra/CustomerStatusProduk'));
    }


}