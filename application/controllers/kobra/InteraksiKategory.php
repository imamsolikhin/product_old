<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class InteraksiKategory extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'interaksi_kategory';
        $data['services'] = 'kobra/InteraksiKategory/';
        $data['page_title'] = 'Interaksi Kategory';
        $data['main_content'] = $this->load->view('kobra/interaksi-kategory/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $InteraksiKategory = $this->common_model->get_opt('name',$_POST['name'],'interaksi_kategory');

            if (empty($InteraksiKategory)) {
                $InteraksiKategory = $this->common_model->insert($data, 'interaksi_kategory');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/InteraksiKategory'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/InteraksiKategory/';
        $data['page_title'] = 'Interaksi Kategory';
        $data['main_content'] = $this->load->view('kobra/interaksi-kategory/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'interaksi_kategory');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/InteraksiKategory'));
        }
        $data['services'] = 'kobra/InteraksiKategory/';
        $data['page_title'] = 'InteraksiKategory';
        $data['data'] = $this->common_model->get_id($id,'interaksi_kategory');
        $data['main_content'] = $this->load->view('kobra/interaksi-kategory/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'interaksi_kategory');
        $this->session->set_flashdata('msg', 'Interaksi Kategory active Successfully');
        redirect(base_url('kobra/InteraksiKategory'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'interaksi_kategory');
        $this->session->set_flashdata('msg', 'Interaksi Kategory deactive Successfully');
        redirect(base_url('kobra/InteraksiKategory'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'interaksi_kategory'); 
        $this->session->set_flashdata('msg', 'Interaksi Kategory deleted Successfully');
        redirect(base_url('kobra/InteraksiKategory'));
    }


}