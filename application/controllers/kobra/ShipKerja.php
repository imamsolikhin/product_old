<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ShipKerja extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'ship_kerja';
        $data['services'] = 'kobra/ShipKerja/';
        $data['page_title'] = 'Ship Kerja';
        $data['main_content'] = $this->load->view('kobra/ship-kerja/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $ShipKerja = $this->common_model->get_opt('name',$_POST['name'],'ship_kerja');

            if (empty($ShipKerja)) {
                $ShipKerja = $this->common_model->insert($data, 'ship_kerja');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/ShipKerja'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/ShipKerja/';
        $data['page_title'] = 'Ship Kerja';
        $data['main_content'] = $this->load->view('kobra/ship-kerja/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'ship_kerja');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/ShipKerja'));
        }
        $data['services'] = 'kobra/ShipKerja/';
        $data['page_title'] = 'ShipKerja';
        $data['data'] = $this->common_model->get_id($id,'ship_kerja');
        $data['main_content'] = $this->load->view('kobra/ship-kerja/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'ship_kerja');
        $this->session->set_flashdata('msg', 'Ship Kerja active Successfully');
        redirect(base_url('kobra/ShipKerja'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'ship_kerja');
        $this->session->set_flashdata('msg', 'Ship Kerja deactive Successfully');
        redirect(base_url('kobra/ShipKerja'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'ship_kerja'); 
        $this->session->set_flashdata('msg', 'Ship Kerja deleted Successfully');
        redirect(base_url('kobra/ShipKerja'));
    }


}