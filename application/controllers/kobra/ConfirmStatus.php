<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ConfirmStatus extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'confirm_status';
        $data['services'] = 'kobra/ConfirmStatus/';
        $data['page_title'] = 'Confirm Status';
        $data['main_content'] = $this->load->view('kobra/confirm-status/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $ConfirmStatus = $this->common_model->get_opt('name',$_POST['name'],'confirm_status');

            if (empty($ConfirmStatus)) {
                $ConfirmStatus = $this->common_model->insert($data, 'confirm_status');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/ConfirmStatus'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/ConfirmStatus/';
        $data['page_title'] = 'Confirm Status';
        $data['main_content'] = $this->load->view('kobra/confirm-status/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'confirm_status');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/ConfirmStatus'));
        }
        $data['services'] = 'kobra/ConfirmStatus/';
        $data['page_title'] = 'ConfirmStatus';
        $data['data'] = $this->common_model->get_id($id,'confirm_status');
        $data['main_content'] = $this->load->view('kobra/confirm-status/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'confirm_status');
        $this->session->set_flashdata('msg', 'Confirm Status active Successfully');
        redirect(base_url('kobra/ConfirmStatus'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'confirm_status');
        $this->session->set_flashdata('msg', 'Confirm Status deactive Successfully');
        redirect(base_url('kobra/ConfirmStatus'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'confirm_status'); 
        $this->session->set_flashdata('msg', 'Confirm Status deleted Successfully');
        redirect(base_url('kobra/ConfirmStatus'));
    }


}