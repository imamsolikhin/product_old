<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'branch';
        $data['services'] = 'kobra/Branch/';
        $data['page_title'] = 'Branch';
        $data['main_content'] = $this->load->view('kobra/branch/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Branch = $this->common_model->get_opt('name',$_POST['name'],'branch');

            if (empty($Branch)) {
                $Branch = $this->common_model->insert($data, 'branch');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Branch'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/Branch/';
        $data['page_title'] = 'Branch';
        $data['main_content'] = $this->load->view('kobra/branch/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'branch');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/Branch'));
        }
        $data['services'] = 'kobra/Branch/';
        $data['page_title'] = 'Branch';
        $data['data'] = $this->common_model->get_id($id,'branch');
        $data['main_content'] = $this->load->view('kobra/branch/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'branch');
        $this->session->set_flashdata('msg', 'Branch active Successfully');
        redirect(base_url('kobra/Branch'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'branch');
        $this->session->set_flashdata('msg', 'Branch deactive Successfully');
        redirect(base_url('kobra/Branch'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'branch'); 
        $this->session->set_flashdata('msg', 'Branch deleted Successfully');
        redirect(base_url('kobra/Branch'));
    }


}