<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phone extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'phone';
        $data['services'] = 'kobra/Phone/';
        $data['page_title'] = 'Phone';
        $data['main_content'] = $this->load->view('kobra/phone/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Phone = $this->common_model->get_opt('name',$_POST['name'],'phone');

            if (empty($Phone)) {
                $Phone = $this->common_model->insert($data, 'phone');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Phone'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/Phone/';
        $data['page_title'] = 'Phone';
        $data['main_content'] = $this->load->view('kobra/phone/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'phone');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/Phone'));
        }
        $data['services'] = 'kobra/Phone/';
        $data['page_title'] = 'Phone';
        $data['data'] = $this->common_model->get_id($id,'phone');
        $data['main_content'] = $this->load->view('kobra/phone/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'phone');
        $this->session->set_flashdata('msg', 'Phone active Successfully');
        redirect(base_url('kobra/Phone'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'phone');
        $this->session->set_flashdata('msg', 'Phone deactive Successfully');
        redirect(base_url('kobra/Phone'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'phone'); 
        $this->session->set_flashdata('msg', 'Phone deleted Successfully');
        redirect(base_url('kobra/Phone'));
    }


}