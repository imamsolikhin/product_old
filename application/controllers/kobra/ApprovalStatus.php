<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ApprovalStatus extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'approval_status';
        $data['services'] = 'kobra/ApprovalStatus/';
        $data['page_title'] = 'Approval Status';
        $data['main_content'] = $this->load->view('kobra/approval-status/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $ApprovalStatus = $this->common_model->get_opt('name',$_POST['name'],'approval_status');

            if (empty($ApprovalStatus)) {
                $ApprovalStatus = $this->common_model->insert($data, 'approval_status');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/ApprovalStatus'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/ApprovalStatus/';
        $data['page_title'] = 'Approval Status';
        $data['main_content'] = $this->load->view('kobra/approval-status/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'approval_status');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/ApprovalStatus'));
        }
        $data['services'] = 'kobra/ApprovalStatus/';
        $data['page_title'] = 'Approval Status';
        $data['data'] = $this->common_model->get_id($id,'approval_status');
        $data['main_content'] = $this->load->view('kobra/approval-status/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'approval_status');
        $this->session->set_flashdata('msg', 'ApprovalStatus active Successfully');
        redirect(base_url('kobra/ApprovalStatus'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'approval_status');
        $this->session->set_flashdata('msg', 'ApprovalStatus deactive Successfully');
        redirect(base_url('kobra/ApprovalStatus'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'approval_status'); 
        $this->session->set_flashdata('msg', 'ApprovalStatus deleted Successfully');
        redirect(base_url('kobra/ApprovalStatus'));
    }


}