<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emosional extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'emosional';
        $data['services'] = 'kobra/Emosional/';
        $data['page_title'] = 'Emosional';
        $data['main_content'] = $this->load->view('kobra/emosional/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Emosional = $this->common_model->get_opt('name',$_POST['name'],'emosional');

            if (empty($Emosional)) {
                $Emosional = $this->common_model->insert($data, 'emosional');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Emosional'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/Emosional/';
        $data['page_title'] = 'Emosional';
        $data['main_content'] = $this->load->view('kobra/emosional/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'emosional');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/Emosional'));
        }
        $data['services'] = 'kobra/Emosional/';
        $data['page_title'] = 'Emosional';
        $data['data'] = $this->common_model->get_id($id,'emosional');
        $data['main_content'] = $this->load->view('kobra/emosional/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'emosional');
        $this->session->set_flashdata('msg', 'Emosional active Successfully');
        redirect(base_url('kobra/Emosional'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'emosional');
        $this->session->set_flashdata('msg', 'Emosional deactive Successfully');
        redirect(base_url('kobra/Emosional'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'emosional'); 
        $this->session->set_flashdata('msg', 'Emosional deleted Successfully');
        redirect(base_url('kobra/Emosional'));
    }


}