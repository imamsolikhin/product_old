<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iklan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'iklan';
        $data['services'] = 'kobra/Iklan/';
        $data['page_title'] = 'Iklan';
        $data['main_content'] = $this->load->view('kobra/iklan/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Iklan = $this->common_model->get_opt('name',$_POST['name'],'iklan');

            if (empty($Iklan)) {
                $Iklan = $this->common_model->insert($data, 'iklan');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Iklan'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/Iklan/';
        $data['page_title'] = 'Iklan';
        $data['main_content'] = $this->load->view('kobra/iklan/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'iklan');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/Iklan'));
        }
        $data['services'] = 'kobra/Iklan/';
        $data['page_title'] = 'Iklan';
        $data['data'] = $this->common_model->get_id($id,'iklan');
        $data['main_content'] = $this->load->view('kobra/iklan/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'iklan');
        $this->session->set_flashdata('msg', 'Iklan active Successfully');
        redirect(base_url('kobra/Iklan'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'iklan');
        $this->session->set_flashdata('msg', 'Iklan deactive Successfully');
        redirect(base_url('kobra/Iklan'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'iklan'); 
        $this->session->set_flashdata('msg', 'Iklan deleted Successfully');
        redirect(base_url('kobra/Iklan'));
    }


}