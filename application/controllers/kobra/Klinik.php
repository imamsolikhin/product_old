<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Klinik extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
       $this->load->model('login_model');
    }

    public function index(){
        $data = array();
        $data['table'] = 'klinik';
        $data['services'] = 'kobra/klinik/';
        $data['page_title'] = 'klinik';
        $data['main_content'] = $this->load->view('kobra/klinik/list', $data, TRUE);
        $this->load->view('index', $data);
    }
    
    //-- add new user by admin 
    public function add(){   
        if ($_POST) {

            $data = array(
                'name' => $_POST['name'],
                'status' => TRUE,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            
            //-- check duplicate email
            $Klinik = $this->common_model->get_opt('name',$_POST['name'],'Klinik');

            if (empty($Klinik)) {
                $Klinik = $this->common_model->insert($data, 'Klinik');
                $this->session->set_flashdata('msg', $_POST['name'].' added Successfully');
                redirect(base_url('kobra/Klinik'));
            } else {
                $this->session->set_flashdata('error_msg', $_POST['name'].' already exist, try another name');
            }
        }

        $data['services'] = 'kobra/klinik/';
        $data['page_title'] = 'klinik';
        $data['main_content'] = $this->load->view('kobra/klinik/add', $data, TRUE);
        $this->load->view('index', $data);
    }

    //-- update users info
    public function update($id){
        if ($_POST) {
            $data = array(
                'name' => $_POST['name']
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'Klinik');
            $this->session->set_flashdata('msg', 'Information Updated Successfully');
            redirect(base_url('kobra/Klinik'));
        }
        $data['services'] = 'kobra/klinik/';
        $data['page_title'] = 'klinik';
        $data['data'] = $this->common_model->get_id($id,'Klinik');
        $data['main_content'] = $this->load->view('kobra/klinik/edit', $data, TRUE);
        $this->load->view('index', $data);
    }

    
    //-- active user
    public function active($id){
        $data = array(
            'status' => 1
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'Klinik');
        $this->session->set_flashdata('msg', 'Klinik active Successfully');
        redirect(base_url('kobra/Klinik'));
    }

    //-- deactive user
    public function deactive($id){
        $data = array(
            'status' => 0
        );
        $data = $this->security->xss_clean($data);
        $this->common_model->update($data, $id,'Klinik');
        $this->session->set_flashdata('msg', 'Klinik deactive Successfully');
        redirect(base_url('kobra/Klinik'));
    }

    //-- delete user
    public function delete($id){
        $this->common_model->delete($id,'Klinik'); 
        $this->session->set_flashdata('msg', 'Klinik deleted Successfully');
        redirect(base_url('kobra/Klinik'));
    }


}