<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Interaksi extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
       $this->load->model('common_model');
    }

    public function interaksi_produk(){
        $data = array();
        $data['token'] = $this->security->get_csrf_hash();
        $data['table'] = 'interaksi_produk';
        $data['services'] = 'management/Interaksi/interaksi_produk';
        $data['page_title'] = 'Management Interaksi Produk';
        $data['main_content'] = $this->load->view('management/list_interaksi_produk', $data, TRUE);
        $this->load->view('index', $data);
    }

    public function marketing_data(){
        $data = array();
        $data['token'] = $this->security->get_csrf_hash();
        $data['table'] = 'interaksi_produk';
        $data['services'] = 'management/Interaksi/marketing_data';
        $data['page_title'] = 'Management Marketing Produk';
        // $data['list_admin_status'] = $this->common_model->select_active('branch');
        $data['list_gudang_status'] = $this->common_model->select_active('delivery_status');
        $data['main_content'] = $this->load->view('management/list_marketing_produk', $data, TRUE);
        $this->load->view('index', $data);
    }


}