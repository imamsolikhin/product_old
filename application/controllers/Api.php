<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Api extends REST_Controller {

	public function __construct(){
        parent::__construct();
       $this->load->model('common_model');
       $this->load->model('interaksi_model');
       $this->load->model('login_model');
    }

    //-- show list datatable
	public function ajax_data_GET(){
		$table = $this->input->get('table');
		$services = $this->input->get('services');
        $result = $this->common_model->get_id($this->input->get('id'),$table);
        $status_admin = ($result->admin_confirm_by != '')? ' ✔':'';
        $status_gudang = ($result->gudang_confirm_by != '')? ' ✔':'';

        $print_data = " <br>*LAPORAN PEMESANAN CS ".$result->produk."*
						<br>*Tanggal : ".date("d-m-Y",strtotime(current_datetime()))."*
						<br>
						<br>*Berikut data pengiriman pemesanan customer:*
						<br>*Nama Pembeli:* ".$result->nama_lengkap."
						<br>*No. Telp:* ".$result->tlp."/".$result->wa."
						<br>*Alamat:* ".$result->alamat."
						<br>*Rt/Rw:* ".$result->rt."/".$result->rw."
						<br>*No Rumah/Kantor:* ".$result->no_rumah."
						<br>*Desa:* ".$result->desa."
						<br>*Patokan:* ".$result->patokan."
						<br>*Kelurahan:* ".$result->kelurahan."
						<br>*Kecamatan:* ".$result->kecamatan."
						<br>*Kota:* ".$result->kota."
						<br>*Provinsi:* ".$result->provinsi."
						<br>*Kode Pos:* ".$result->kode_pos."
						<br>
						<br>*Berikut data Pesanan:* 
						<br>*No. Pesanan :* ".$result->delivery_order."
						<br>*Jumlah Pemesanan :* ".(int)$result->jml_pesanan." [".$result->customer_status."]
						<br>*Biaya Pesanan :*  Rp " . number_format(($result->jml_pesanan*$result->harga),2,',','.')."
						<br>*Biaya Pengiriman :*  Rp " . number_format($result->biaya_pengiriman,2,',','.')."
						<br>*Biaya Asuransi :*  Rp " . number_format($result->asuransi,2,',','.')."
						<br>*Total Pembayaran :*  Rp " . number_format($result->total_transaksi,2,',','.')."
						<br>*Jenis transaksi :* ".$result->type_transaction."[".$result->bank."]
						<br>*Kurir Pengiriman :* ".$result->kurir."
						<br>*Tanggal Order :* ".date("d-m-Y",strtotime($result->tanggal_order))."
						<br>*Estimasi Sampai :* ".date("d-m-Y",strtotime($result->estimasi_sampai))."
						<br>*No. Resi:* ".$result->delivery_resi."
						<br>*No. Ref:* ".$result->delivery_ref."
						<br>*Status pemesanan:* ".$result->delivery_status."
						<br>*Noted:* ".$result->closing_remark."
						<br>
						<br>ACC
						<br>*CS ".$result->produk." ✔*
						<br>*Nama :* ".$result->created_by."
						<br>
						<br>*ADMIN ".$result->produk.$status_admin."*
						<br>*Nama :* ".$result->admin_confirm_by."
						<br>
						<br>*GUDANG ".$result->produk.$status_gudang."*
						<br>*Nama :* ".$result->gudang_confirm_by;
		
		$data = array("data" => $result, "data_show" => $print_data);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable
	public function ajax_list_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->common_model->get_datatables("1=1",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".$result_list->name."'";
			$is_data = ($result_list->status != 0)? '<a href="'.base_url($services."deactive/".$result_list->id).'" data-toggle="tooltip" data-original-title="Deactive"> <i class="fa fa-close text-danger m-r-10"></i> </a>':'<a href="'.base_url($services."active/".$result_list->id).'" data-toggle="tooltip" data-original-title="Active"> <i class="fa fa-check text-info m-r-10"></i> </a>';
			
			$row = array();
			// $row[] = $no;
			$row[] = $result_list->name;
			$row[] = ($result_list->status == 0)? '<div class="label label-table label-danger">Inactive</div>':'<div class="label label-table label-success">Active</div>';
			$row[] = '
					<a href="'.base_url($services."update/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a>
					<a href="#delete'.$result_list->id.'" onclick="delete_data('.$result_list->id.$name_data.')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>
				'.$is_data;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->common_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->common_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable
	public function ajax_list_barang_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->common_model->get_datatables("1=1 AND branch = '".$this->session->userdata('branch')."'",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".date("d-m-Y",strtotime($result_list->tanggal))."'";
			$is_data = ($result_list->status != 0)? '<a href="'.base_url($services."deactive/".$result_list->id).'" data-toggle="tooltip" data-original-title="Deactive"> <i class="fa fa-close text-danger m-r-10"></i> </a>':'<a href="'.base_url($services."active/".$result_list->id).'" data-toggle="tooltip" data-original-title="Active"> <i class="fa fa-check text-info m-r-10"></i> </a>';
			
			$row = array();
			// $row[] = $no;
			$row[] = $result_list->produk;
			$row[] = date("d-m-Y",strtotime($result_list->tanggal));
			$row[] = number_format(($result_list->total),0,',','.');
			$row[] = $result_list->remark;
			$row[] = ($result_list->status == 0)? '<div class="label label-table label-danger">Inactive</div>':'<div class="label label-table label-success">Active</div>';
			$row[] = '
					<a href="'.base_url($services."update/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a>
					<a href="#delete'.$result_list->id.'" onclick="delete_data('.$result_list->id.$name_data.')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>
				'.$is_data;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->common_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->common_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}
	
    //-- show list datatable
	public function ajax_list_setup_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->common_model->get_datatables("1=1",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".character_limiter($result_list->name, 1)."..'";
			$is_data = ($result_list->status != 0)? '<a href="'.base_url($services."deactive/".$result_list->id).'" data-toggle="tooltip" data-original-title="Deactive"> <i class="fa fa-close text-danger m-r-10"></i> </a>':'<a href="'.base_url($services."active/".$result_list->id).'" data-toggle="tooltip" data-original-title="Active"> <i class="fa fa-check text-info m-r-10"></i> </a>';
			
			$row = array();
			// $row[] = $no;
			$row[] = '<img src="'. base_url($result_list->img_tmb).'" class="img-thumbnail" width="180" height="200"/>';
			$row[] = $result_list->name;
			$row[] = ($result_list->status == 0)? '<div class="label label-table label-danger">Inactive</div>':'<div class="label label-table label-success">Active</div>';
			$row[] = '
					<a href="'.base_url($services."update/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a>
					<a href="#delete'.$result_list->id.'" onclick="delete_data('.$result_list->id.$name_data.')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>
				'.$is_data;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->common_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->common_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
    }

    //-- show list datatable Interaksi Klinik
	public function ajax_list_management_interaksi_produk_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
		$range_date = " AND DATE(created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
		$reqs_status = " AND reqs_status = '".$this->input->post('reqs_status')."'";
		$branch = (strtolower($this->session->userdata('role')) == 'cs-produk') ? " AND branch = '".$this->session->userdata('branch')."' AND created_by = '".$this->session->userdata('name')."' ": (strtolower($this->session->userdata('role')) == 'admin-produk') ? " AND branch = '".$this->session->userdata('branch')."' ":"";
		$where = "1=1 ".$branch.$range_date.$reqs_status;
        $result = $this->interaksi_model->get_datatables($where,$table,""); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $result_list->branch;
			$row[] = $result_list->produk;
			$row[] = $result_list->ship_kerja;
			$row[] = $result_list->advertise;
			$row[] = $result_list->interaksi_kategory;
			$row[] = $result_list->id_iklan;
			$row[] = $result_list->nama_lengkap;
			$row[] = $result_list->confirm_status;
			$row[] = $result_list->keluhan;
			$row[] = $result_list->jenis_kelamin;
			$row[] = $result_list->umur;
			$row[] = $result_list->tlp;
			$row[] = $result_list->wa;
			$row[] = $result_list->alamat;
			$row[] = $result_list->alamat_lengkap;
			$row[] = $result_list->no_rumah;
			$row[] = $result_list->rt;
			$row[] = $result_list->rw;
			$row[] = $result_list->desa;
			$row[] = $result_list->kelurahan;
			$row[] = $result_list->patokan;
			$row[] = $result_list->kecamatan;
			$row[] = $result_list->kota;
			$row[] = $result_list->provinsi;
			$row[] = $result_list->kode_pos;
			$row[] = $result_list->emosional;
			$row[] = $result_list->jml_pesanan;
			$row[] = $result_list->harga;
			$row[] = $result_list->biaya_pengiriman;
			$row[] = $result_list->asuransi;
			$row[] = $result_list->total_transaksi;
			$row[] = $result_list->total_transaksi_id;
			$row[] = $result_list->kurir;
			$row[] = $result_list->type_transaction;
			$row[] = $result_list->akun_market;
			$row[] = $result_list->bank;
			$row[] = $result_list->reqs_status;
			$row[] = $result_list->customer_status;
			$row[] = $result_list->tanggal_order;
			$row[] = $result_list->estimasi_sampai;
			$row[] = $result_list->status;
			$row[] = $result_list->closing_status;
			$row[] = $result_list->closing_approval;
			$row[] = $result_list->closing_remark;
			$row[] = $result_list->created_by;
			$row[] = $result_list->created_at;
			$row[] = $result_list->delivery_img_tmb;
			$row[] = $result_list->delivery_img_mid;
			$row[] = $result_list->delivery_status;
			$row[] = $result_list->delivery_resi;
			$row[] = $result_list->delivery_ref;

			$data[] = $row;
		}
		$data = array(
			 	  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all($where,$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered($where,$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable Interaksi Klinik
	public function ajax_list_management_marketing_produk_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
		$range_date = (!$this->input->post('create_or_order_status'))? " AND DATE(created_at) BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ":" AND tanggal_order BETWEEN DATE('".$this->input->post('start_date')."') AND DATE('".$this->input->post('end_date')."') ";
		$admin_status = ($this->input->post('admin_status') == "" )? "":" AND closing_approval = '".$this->input->post('admin_status')."'";
		$gudang_status = ($this->input->post('gudang_status') == "" )? "":" AND delivery_status = '".$this->input->post('gudang_status')."'";
		$interaksi_status = ($this->input->post('interaksi_status') == "" )? "":" AND reqs_status = '".$this->input->post('interaksi_status')."'";
		$branch = (strtolower($this->session->userdata('role')) == 'cs-produk') ? " AND branch = '".$this->session->userdata('branch')."' AND created_by = '".$this->session->userdata('name')."' ":" AND branch = '".$this->session->userdata('branch')."' ";
		$where = "1=1 ".$branch.$range_date.$admin_status.$gudang_status.$interaksi_status;
        $result = $this->interaksi_model->get_datatables($where,$table,""); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $result_list->delivery_ref;
			$row[] = "'".$result_list->delivery_resi;
			$row[] = $result_list->delivery_status;
			$row[] = $result_list->branch;
			$row[] = $result_list->produk;
			$row[] = $result_list->ship_kerja;
			$row[] = $result_list->advertise;
			$row[] = $result_list->interaksi_kategory;
			$row[] = $result_list->id_iklan;
			$row[] = $result_list->created_by;
			$row[] = $result_list->created_at;
			$row[] = $result_list->jenis_kelamin.' '.$result_list->nama_lengkap;
			$row[] = $result_list->confirm_status;
			$row[] = $result_list->keluhan;
			$row[] = $result_list->umur;
			$row[] = $result_list->emosional;
			$row[] = $result_list->tlp;
			$row[] = $result_list->wa;
			$row[] = $result_list->alamat;
			$row[] = $result_list->alamat_lengkap;
			$row[] = $result_list->no_rumah;
			$row[] = $result_list->rt;
			$row[] = $result_list->rw;
			$row[] = $result_list->desa;
			$row[] = $result_list->kelurahan;
			$row[] = $result_list->patokan;
			$row[] = $result_list->kecamatan;
			$row[] = $result_list->kota;
			$row[] = $result_list->provinsi;
			$row[] = $result_list->kode_pos;
			$row[] = $result_list->jml_pesanan;
			$row[] = $result_list->harga;
			$row[] = $result_list->biaya_pengiriman;
			$row[] = $result_list->asuransi;
			$row[] = $result_list->total_transaksi;
			$row[] = $result_list->total_transaksi_id;
			$row[] = $result_list->kurir;
			$row[] = $result_list->type_transaction;
			$row[] = $result_list->akun_market;
			$row[] = $result_list->bank;
			$row[] = $result_list->reqs_status;
			$row[] = $result_list->customer_status;
			$row[] = date("d-m-Y",strtotime($result_list->tanggal_order));
			$row[] = $result_list->estimasi_sampai;
			$row[] = $result_list->closing_status;
			$row[] = $result_list->closing_approval;
			$row[] = $result_list->closing_remark;
			$row[] = $result_list->packing_by;

			$data[] = $row;
		}
		$data = array(
			 	  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all($where,$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered($where,$table)
				, "data" => $data
				, "message"	=> "ok".$where
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}


    //-- show list datatable Interaksi Klinik
	public function ajax_list_interaksi_klinik_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->interaksi_model->get_datatables("1=1",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".$result_list->name."'";
			$is_data = ($result_list->status != 0)? '<a href="'.base_url($services."deactive/".$result_list->id).'" data-toggle="tooltip" data-original-title="Deactive"> <i class="fa fa-close text-danger m-r-10"></i> </a>':'<a href="'.base_url($services."active/".$result_list->id).'" data-toggle="tooltip" data-original-title="Active"> <i class="fa fa-check text-info m-r-10"></i> </a>';
			$btn = ($result_list->closing_status==true)? "":'<a href="'.base_url($services."update/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a><a href="#delete'.$result_list->id.'" onclick="delete_data('.$result_list->id.$name_data.')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>';
			$row = array();
			$row[] = $no;
			$row[] = '<b style="font-weight:600;">'.$result_list->gender.' '.$result_list->name.'</b> <br> '.$result_list->klinik.'<br><b>Karakter : </b>'.$result_list->emosional.' <br><b>ACTION : </b>'.$btn;
			$row[] = '<b>No Telp: '.$result_list->phone.'</b><br><b>No WA: </b>'.$result_list->phone_wa.' <br><b>Kedatangan:  </b>'.date("d-m-Y",strtotime($result_list->confirm_date));
			$row[] = '<b>Kota : '.$result_list->city.'</b> <br><b>Alamat : </b>'.$result_list->address.' <br><b>Keluhan : </b>'.$result_list->complain;
			$row[] = '<b>Iklan : '.$result_list->advertise.' '.$result_list->interaksi_kategory.' </b> <br><b>ID Iklan : </b>'.$result_list->id_iklan.' <br><b>Ship Kerja : </b>'.$result_list->ship_kerja;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable Interaksi Klinik
	public function ajax_list_interaksi_produk_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->interaksi_model->get_datatables("1=1 AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND created_by = '".$this->session->userdata('name')."'",$table,"closing_approval ASC");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".$result_list->nama_lengkap."'";
			$is_data = ($result_list->status != 0)? '<a href="'.base_url($services."deactive/".$result_list->id).'" data-toggle="tooltip" data-original-title="Deactive"> <i class="fa fa-close text-danger m-r-10"></i> </a>':'<a href="'.base_url($services."active/".$result_list->id).'" data-toggle="tooltip" data-original-title="Active"> <i class="fa fa-check text-info m-r-10"></i> </a>';
			$btn = ($result_list->closing_status==true)? "":'<a href="'.base_url($services."update/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a><a href="#delete'.$result_list->id.'" onclick="delete_data('.$result_list->id.$name_data.')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>';
			$show = ((int)$result_list->closing_approval==1)? "Lock Data":$btn;
			$row = array();
			$row[] = $no;
			$row[] = '<b style="font-weight:600;">'.$result_list->jenis_kelamin.' '.$result_list->nama_lengkap.'</b> <br> '.$result_list->produk.'<br><b>ACTION : </b>'.$show;
			$row[] = '<b>No Telp: '.$result_list->tlp.'</b><br><b>No WA: </b>'.$result_list->wa.' <br><b>Tanggal Order:  </b>'.date("d-m-Y",strtotime($result_list->tanggal_order));
			$row[] = $result_list->alamat_lengkap;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all("1=1 AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND created_by = '".$this->session->userdata('name')."'",$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered("1=1 AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."' AND created_by = '".$this->session->userdata('name')."'",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable closing Klinik
	public function ajax_list_closing_klinik_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->common_model->get_datatables("1=1 AND confirm_status = 'Berkunjung'",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".$result_list->name."'";
			$btn = ($result_list->closing_status==true)? '<i class="fa fa-check text-danger m-r-10"></i>':'<a href="'.base_url($services."reqsclosing/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-success m-r-10"></i></a>';
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'. base_url($result_list->img_tmb).'" class="img-thumbnail" width="180" height="200"/><br>Nama Pasien [&nbsp;&nbsp;'.$btn.']<br>'.$result_list->gender.' '.$result_list->name;
			$row[] = '<b>No Telp: '.$result_list->phone.'</b><br><b>No WA: </b>'.$result_list->phone_wa.' <br><b>Kedatangan:  </b>'.date("d-m-Y",strtotime($result_list->confirm_date));
			$row[] = '<b>Kota : '.$result_list->city.'</b> <br><b>Alamat : </b>'.$result_list->address.' <br><b>Keluhan : </b>'.$result_list->complain;
			$row[] = '<b>'.$result_list->klinik.'</b><br>'.$result_list->remark;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->common_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->common_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}


    //-- show list datatable closing Klinik
	public function ajax_list_closing_produk_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->interaksi_model->get_datatables("1=1 AND reqs_status='YES' AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."'",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$btn_show = "show_data_product('".$result_list->id."','".$table."'".",'".$services."')";
			$btn_show_format = '<a href="#show_'.$result_list->nama_lengkap.'" onclick="'.$btn_show.'" data-toggle="tooltip" data-original-title="Show Data"> <i class="fa fa-file-text text-success m-r-10"></i> </a>';
			// $btn_inv = "show_data_invoice('".$result_list->id."','".$table."'".",'".$services."')";
			$btn_show_invoice = '<a href="'.base_url($services).'/invoice_customer/'.$result_list->id.'" data-toggle="tooltip" data-original-title="Show Data"> <i class="fa fa-arrow-circle-down text-success m-r-10"></i> </a>';
			$name_data = ",'".$result_list->nama_lengkap."'";
			$btn = ($result_list->closing_status)? '<i class="fa fa-check text-danger m-r-10"></i>':'<a href="'.base_url($services."reqsclosing/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-success m-r-10"></i></a>';
			$show = (!$result_list->closing_approval)? '<b style="font-weight:600;">Pendding</b>': $btn_show_format.$btn_show_invoice;
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'. base_url($result_list->delivery_img_mid).'" class="img-thumbnail" width="180" height="200"/><br>
					'.'&nbsp;[&nbsp;&nbsp;'.$btn.$show.']<br>'.$result_list->jenis_kelamin.' '.$result_list->nama_lengkap.'<br/> <a href="https://wa.me/+62'.substr(trim($result_list->tlp), 1).'" class="btn btn-success" target="_blank"><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i> Chat WA</a> <a href="https://cekresi.com/?noresi='.trim($result_list->delivery_resi).'" class="btn btn-success" target="_blank"><i class="fa fa-truck fa-lg" aria-hidden="true"></i> Check</a>';
			$row[] = '<br><b>Jml: '.(int)$result_list->jml_pesanan.'</b><br><b>Total: </b>'.$result_list->total_transaksi_id.' <br><b>Transaksi:  </b>'.date("d-m-Y",strtotime($result_list->tanggal_order));
			$row[] = '<b>No. Order:</b>'.$result_list->delivery_order.'<br/><b>No. Resi:</b> '.$result_list->delivery_resi.'<br/><b>No. Ref:</b> '.$result_list->delivery_ref;
			$row[] = $result_list->alamat_lengkap;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all("1=1 AND reqs_status='YES' AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."'",$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered("1=1 AND reqs_status='YES' AND created_by = '".$this->session->userdata('name')."' AND branch = '".$this->session->userdata('branch')."'",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}


    //-- show list datatable closing Klinik
	public function ajax_list_approval_produk_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->interaksi_model->get_datatables("1=1 AND reqs_status='YES' AND branch = '".$this->session->userdata('branch')."'",$table,"closing_approval ASC,closing_approval_date DESC");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$btn_show = "show_data_product('".$result_list->id."','".$table."'".",'".$services."')";
			$btn_show_format = '<a href="#show_'.$result_list->nama_lengkap.'" onclick="'.$btn_show.'" data-toggle="tooltip" data-original-title="Show Data"> <i class="fa fa-file-text text-success m-r-10"></i> </a>';
			$btn_show_invoice = '<a href="'.base_url($services).'/invoice_customer/'.$result_list->id.'" data-toggle="tooltip" data-original-title="Show Data"> <i class="fa fa-arrow-circle-down text-success m-r-10"></i> </a>';
			$name_data = ",'".$result_list->nama_lengkap."'";
			$btn = ($result_list->closing_status)? '<i class="fa fa-check text-danger m-r-10"></i>':'<a href="'.base_url($services."reqsapproval/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-success m-r-10"></i></a>';
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'. base_url($result_list->delivery_img_tmb).'" class="img-thumbnail" width="180" height="200"/><br>&nbsp;[&nbsp;&nbsp;'.$btn.$btn_show_format.$btn_show_invoice.']<br>'.$result_list->jenis_kelamin.' '.$result_list->nama_lengkap;
			$row[] = '<b>Jml: '.(int)$result_list->jml_pesanan.'</b><br><b>Total: </b>'.$result_list->total_transaksi_id.' <br><b>Transaksi:  </b>'.date("d-m-Y",strtotime($result_list->tanggal_order));
			$row[] = '<b>No. Order :</b>'.$result_list->delivery_order.'
					<br/><b>No. Resi:</b> '.$result_list->delivery_resi.'
					<br/><b>No. Ref:</b> '.$result_list->delivery_ref;
			$row[] = '
			        <b>Nama CS : </b> '.$result_list->created_by.'
			        <br/><b>Status Order : </b> '.$result_list->delivery_status.'
					<br/><b>Approval Status :</b> '.$status = ($result_list->closing_approval)?"Approved":"Pending".'
					<br/><b>Approval Date :</b> '.date("d-m-Y",strtotime($result_list->closing_approval_date));

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all("1=1 AND reqs_status='YES' AND branch = '".$this->session->userdata('branch')."'",$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered("1=1 AND reqs_status='YES' AND branch = '".$this->session->userdata('branch')."'",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable closing Klinik
	public function ajax_list_gudang_produk_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->interaksi_model->get_datatables("1=1 AND reqs_status='YES' AND branch = '".$this->session->userdata('branch')."' AND closing_approval = true",$table,"closing_approval ASC,closing_approval_date DESC");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$btn_show = "show_data_product('".$result_list->id."','".$table."'".",'".$services."')";
			$btn_show_format = '<a href="#show_'.$result_list->nama_lengkap.'" onclick="'.$btn_show.'" data-toggle="tooltip" data-original-title="Show Data"> <i class="fa fa-file-text text-success m-r-10"></i> </a>';
			$btn_show_invoice = '<a href="'.base_url($services).'/invoice_customer/'.$result_list->id.'" data-toggle="tooltip" data-original-title="Show Data"> <i class="fa fa-arrow-circle-down text-success m-r-10"></i> </a>';
			$name_data = ",'".$result_list->nama_lengkap."'";
			$btn = ($result_list->closing_status)? '<i class="fa fa-check text-danger m-r-10"></i>':'<a href="'.base_url($services."reqsgudang/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-success m-r-10"></i></a>';
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'. base_url($result_list->delivery_img_tmb).'" class="img-thumbnail" width="180" height="200"/><br>&nbsp;[&nbsp;&nbsp;'.$btn.$btn_show_format.$btn_show_invoice.']<br>'.$result_list->jenis_kelamin.' '.$result_list->nama_lengkap;
			$row[] = '<b>Jml: '.(int)$result_list->jml_pesanan.'</b><br><b>Total: </b>'.$result_list->total_transaksi_id.' <br><b>Transaksi:  </b>'.date("d-m-Y",strtotime($result_list->tanggal_order));
			$row[] = '<b>No. Order :</b>'.$result_list->delivery_order.'
					<br/><b>No. Resi:</b> '.$result_list->delivery_resi.'
					<br/><b>No. Ref:</b> '.$result_list->delivery_ref;
			$row[] = '
			        <b>Nama CS : </b> '.$result_list->created_by.'
			        <br/><b>Status Order : </b> '.$result_list->delivery_status.'
					<br/><b>Approval Status :</b> '.$status = ($result_list->closing_approval)?"Approved":"Pending".'
					<br/><b>Approval Date :</b> '.date("d-m-Y",strtotime($result_list->closing_approval_date));

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->interaksi_model->count_all("1=1 AND reqs_status='YES' AND branch = '".$this->session->userdata('branch')."'",$table)
				, "recordsFiltered" => $this->interaksi_model->count_filtered("1=1 AND reqs_status='YES' AND branch = '".$this->session->userdata('branch')."'",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}

    //-- show list datatable closing Klinik
	public function ajax_list_klinik_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->common_model->get_datatables("1=1",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".$result_list->name."'";
			$btn = ($result_list->closing_status==true)? "":'<a href="'.base_url($services."update/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-success m-r-10"></i></a>';
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'. base_url($result_list->img_tmb).'" class="img-thumbnail" width="180" height="200"/><br>Nama Pasien [&nbsp;&nbsp;'.$btn.']<br>'.$result_list->gender.' '.$result_list->name;
			$row[] = '<b>No Telp: '.$result_list->phone.'</b><br><b>No WA: </b>'.$result_list->phone_wa.' <br><b>Kedatangan:  </b>'.date("d-m-Y",strtotime($result_list->confirm_date));
			$row[] = '<b>Kota : '.$result_list->city.'</b> <br><b>Alamat : </b>'.$result_list->address.' <br><b>Keluhan : </b>'.$result_list->complain;
			// $row[] = '<b>'.$result_list->klinik.'</b><br>'.$result_list->remark;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->common_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->common_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}
	
    //-- show list datatable closing Klinik
	public function ajax_list_appproval_POST(){
		$table = $this->input->post('table');
		$services = $this->input->post('services');
        $result = $this->common_model->get_datatables("1=1",$table,"");
		$data = array();
		$no = $this->input->post('start');
		foreach ($result as $result_list) {
			$no++;
			$name_data = ",'".$result_list->name."'";
			$btn = ($result_list->appproval_code != "")? '<i class="fa fa-check text-danger m-r-10"></i>':'<a href="'.base_url($services."reqsappproval/".$result_list->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-success m-r-10"></i></a>';
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'. base_url($result_list->img_tmb).'" class="img-thumbnail" width="180" height="200"/><br>Nama Pasien [&nbsp;&nbsp;'.$btn.']<br>'.$result_list->gender.' '.$result_list->name;
			$row[] = '<b>No Telp: '.$result_list->phone.'</b><br><b>No WA: </b>'.$result_list->phone_wa.' <br><b>Kedatangan:  </b>'.date("d-m-Y",strtotime($result_list->confirm_date));
			$row[] = '<b>Kota : '.$result_list->city.'</b> <br><b>Alamat : </b>'.$result_list->address.' <br><b>Keluhan : </b>'.$result_list->complain;
			$row[] = '<b>KLINIK : '.$result_list->klinik.'</b><br> Appproval : '.$result_list->appproval_status.'</b><br> KODE : '.$result_list->appproval_code;

			$data[] = $row;
		}
		$data = array(
				  "draw" => $this->input->post('draw')
				, "recordsTotal" => $this->common_model->count_all("1=1",$table)
				, "recordsFiltered" => $this->common_model->count_filtered("1=1",$table)
				, "data" => $data
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}
	
    //-- show list datatable Interaksi Klinik
	public function ajax_interaksi_calendar_GET(){
		$select = 	"COUNT(*) AS jml,
						interaksi_klinik.confirm_status,
						DATE(interaksi_klinik.created_at) AS created_at
					";
		$table	= "interaksi_klinik";
		$where	= "1=1 AND interaksi_klinik.created_at BETWEEN DATE('".$this->input->get('start')."') AND DATE('".$this->input->get('end')."')";
		$group	= "DATE(interaksi_klinik.created_at),interaksi_klinik.confirm_status";
		$order	= "DATE(interaksi_klinik.created_at) DESC";

		$result = $this->common_model->select_other($select, $where, $group, $order, $table);
		foreach ($result as $result_list) {
			$color;
			if($result_list['confirm_status'] == "Berkunjung"){
				$color = "Green";
			}elseif($result_list['confirm_status'] == "Belum Konfirmasi"){
				$color = "Black";
			}elseif($result_list['confirm_status'] == "Di Jawab"){
				$color = "Blue";
			}else{
				$color = "Red";
			}
			$data_list[] = array(
				'id'   	=> $result_list['jml'],
				'title' => '['.$result_list['jml'].'] '.$result_list['confirm_status'],
				'start' => $result_list['created_at'],
				'end'   => $result_list['created_at'],
				'url'   => "#".$result_list['confirm_status'],
				'color'   => $color
			);
		}
		//FORMAT RETURN JSON
		echo json_encode($data_list);
        return;
    }
	
    //-- show list datatable Interaksi Klinik
	public function ajax_analisis_calendar_GET(){
		$select = 	"COUNT(*) AS jml,
						IFNULL(interaksi_klinik.created_by,'Unknow') as created_by,
						DATE(interaksi_klinik.".$this->input->get('show').") AS created_at,
						IFNULL(user.img_mid,'assets/images/users/1.jpg') as img_mid,
						IFNULL(user.img_tmb,'assets/images/users/1.jpg') as img_tmb
					";
		$table	= "interaksi_klinik LEFT JOIN user on CONCAT(user.first_name,' ',user.last_name) = interaksi_klinik.created_by";
		$where	= "1=1 AND interaksi_klinik.confirm_status = '".$this->input->get('status')."'
					AND interaksi_klinik.".$this->input->get('data')." BETWEEN DATE('".date("Y-m-d",(int)$this->input->get('start'))."') AND DATE('".date("Y-m-d",(int)$this->input->get('end'))."')";
		$group	= "DATE(interaksi_klinik.".$this->input->get('show')."),interaksi_klinik.created_by";
		$order	= "DATE(interaksi_klinik.".$this->input->get('show').") DESC";

		$result = $this->common_model->select_other($select, $where, $group, $order, $table);
		$data_result = array();
		foreach ($result as $result_list) {$row = array();
			$row[] = array();
			$row['id'] = $result_list['jml'];
			$row['title'] = '['.$result_list['jml'].'] '.$result_list['created_by'];
			$row['start'] = $result_list['created_at'];
			$row['end'] = $result_list['created_at'];
			$row['url'] = "#".$result_list['created_by'];
			$row['by'] = $result_list['created_by'];
			$row['mid'] = $result_list['img_mid'];
			$row['tmb'] = $result_list['img_tmb'];
			
			$data_result[] = $row;
		}
		
		$data = array(
				  "data" => $data_result
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
		$this->response($data, REST_Controller::HTTP_OK);
		return;
    }

    //-- show list datatable Interaksi Klinik
	public function ajax_interaksi_chart_GET(){
		$select =  "COUNT(*) AS jml,
					DATE_FORMAT(interaksi_klinik.confirm_date,'%b-%y') AS name,
					interaksi_klinik.klinik AS klinik
					";
		$table	= "interaksi_klinik";
		$where	= "1=1 AND interaksi_klinik.confirm_status = 'Berkunjung' AND DATE(interaksi_klinik.confirm_date) >= DATE(DATE_SUB(NOW(), INTERVAL 6 MONTH))
		";
		$group	= "DATE_FORMAT(interaksi_klinik.confirm_date,'%m-%y'),interaksi_klinik.klinik";
		$order	= "DATE_FORMAT(interaksi_klinik.confirm_date,'%m-%y') DESC";

		$result = $this->common_model->select_other($select, $where, $group, $order, $table);
		foreach ($result as $result_list) {
			$list[] = $result_list;
		}
		$data = array(
				  "data" => $list
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
    }
    //-- show list datatable Interaksi Klinik
	public function ajax_interaksi_maps_GET(){
		$select = 	"*";
		$table	= "city";
		$where	= "1=1";
		$group	= "";
		$order	= "";

		$result = $this->common_model->select_other($select, $where, $group, $order, $table);
		foreach ($result as $result_list) {
			$data_list[] = array(
				'name' => $result_list['name'],
				'lat'  => $result_list['lat'],
				'long' => $result_list['long'],
				'total' => $result_list['total']
			);
		}
		$data = array(
				  "data" => $data_list
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
	}
	
    //-- show list datatable Interaksi Klinik
	public function sales_GET(){
		$select = 	"COUNT(id) AS jml,
					(COUNT(id) * 25000) AS total,
					interaksi_klinik.klinik 
					";
		$table	= "interaksi_klinik";
		$where	= " DATE(created_at) = DATE(NOW())";
		$group	= "interaksi_klinik.klinik";
		$order	= "";

		$result_summary = $this->common_model->select_other($select, $where, $group, $order, $table);
		
		$select = 	"*";
		$table	= '	(SELECT 
						interaksi_klinik.created_by,
						interaksi_klinik.confirm_status,
						TIME_FORMAT(interaksi_klinik.created_at, "%H:%i") as time
					FROM
						interaksi_klinik 
					WHERE (interaksi_klinik.confirm_status != NULL OR interaksi_klinik.confirm_status != "") AND DATE(created_at) = DATE(NOW())
					LIMIT 100
					UNION ALL 
					SELECT 
						interaksi_produk.created_by,
						interaksi_produk.confirm_status,
						TIME_FORMAT(interaksi_produk.created_at, "%H:%i") as time
					FROM
					interaksi_produk 
					WHERE (interaksi_produk.confirm_status != NULL OR interaksi_produk.confirm_status != "") AND DATE(created_at) = DATE(NOW())
					LIMIT 100
					) AS summary
				';
		$where	= "1=1";
		$group	= "";
		$order	= "summary.time DESC";

		$result_log = $this->common_model->select_other($select, $where, $group, $order, $table);
		$data = array(
				  "data_summary" => $result_summary
				, "data_log" => $result_log
				, "message"	=> "ok"
				, "status" 	=> true
			);

		//FORMAT RETURN JSON
        $this->response($data, REST_Controller::HTTP_OK);
        return;
    }
    
}