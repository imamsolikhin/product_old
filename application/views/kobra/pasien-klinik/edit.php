

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add New <?php echo $page_title; ?></li>
            </ol>
        </div>
        
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Add New <?php echo $page_title; ?> <a href="<?php echo base_url($services) ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All <?php echo $page_title; ?> </a></h4>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url($services.'update/'.$data->id) ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>
                            <div class="row">
                                <div class="col-3">
                                    <div class="col-md-12">
                                        <img src="<?php echo base_url($data->img_mid); ?>" class="img-thumbnail" width="250" height="300" id="profile_show" />
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group mb-12">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="photo" id="profile_upload" onchange="readURL(this)">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-md-9">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3"></label>
                                            <div class="controls">
                                                <button type="submit" class="btn btn-success">Save <?php echo $page_title; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    $('#profile_show')
                                                        .attr('src', e.target.result)
                                                        .width(280)
                                                        .height(250);
                                                };
                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                                        $("#profile_show").click(function() {
                                            $("#profile_upload").click();
                                        });
                                    </script>
                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Klink || Iklan</label>
                                                <div class="col-md-5 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control custom-select" name="klinik" aria-invalid="false">
                                                            <option value="0">Select</option>
                                                            <?php foreach ($list_klinik as $klinik): ?>
                                                                <?php 
                                                                    if($klinik['name'] == $data->klinik){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $klinik['name']; ?>"><?php echo $klinik['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control custom-select" name="advertise" aria-invalid="false">
                                                            <option value="0">Select</option>
                                                            <?php foreach ($list_advertise as $advertise): ?>
                                                                <?php 
                                                                    if($advertise['name'] == $data->advertise){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $advertise['name']; ?>"><?php echo $advertise['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Nama Lengkap<span class="text-danger">*</span></label>
                                                <div class="col-md-9 controls">
                                                    <input type="text" name="name" class="form-control" required data-validation-required-message="Nama Lengkap is required" value="<?php echo $data->name; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Jenis Kelamin </label>
                                                <div class="col-md-4 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control custom-select" name="gender" aria-invalid="false">
                                                            <option value="0">Select</option>
                                                            <?php foreach ($list_gender as $gender): ?>
                                                                <?php 
                                                                    if($gender['name'] == $data->gender){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $gender['name']; ?>"><?php echo $gender['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <label class="control-label text-right col-md-1">Umur</label>
                                                <div class="col-md-4 controls">
                                                    <input type="text" name="age" class="form-control" value="<?php echo $data->age; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Tlp</label>
                                                <div class="col-md-4 controls">
                                                    <input type="text" name="phone" class="form-control" value="<?php echo $data->phone; ?>" >
                                                </div>
                                                <label class="control-label text-right col-md-1">Wa</label>
                                                <div class="col-md-4 controls">
                                                    <input type="text" name="phone_wa" class="form-control" value="<?php echo $data->phone_wa; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Kota</label>
                                                <div class="col-md-4 controls">
                                                    <input type="text" name="city" class="form-control" value="<?php echo $data->city; ?>" >
                                                </div>
                                                <label class="control-label text-right col-md-1">Email</label>
                                                <div class="col-md-4 controls">
                                                    <input type="text" name="email" class="form-control" value="<?php echo $data->email; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Alamat</label>
                                                <div class="col-md-9 controls">
                                                    <input type="text" name="address" class="form-control" value="<?php echo $data->address; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Keluhan</label>
                                                <div class="col-md-9 controls">
                                                    <input type="text" name="complain" class="form-control" value="<?php echo $data->complain; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Emosional Pasien </label>
                                                <div class="col-md-9 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control custom-select" name="emosional" aria-invalid="false">
                                                            <option value="0">Select</option>
                                                            <?php foreach ($list_emosional as $emosional): ?>
                                                                <?php 
                                                                    if($emosional['name'] == $data->emosional){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $emosional['name']; ?>"><?php echo $emosional['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Status Pasien </label>
                                                <div class="col-md-9 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control custom-select" name="pasien_status_klinik" aria-invalid="false">
                                                            <option value="0">Select</option>
                                                            <?php foreach ($list_pasien_status_klinik as $pasien_status_klinik): ?>
                                                                <?php 
                                                                    if($pasien_status_klinik['name'] == $data->pasien_status_klinik){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $pasien_status_klinik['name']; ?>"><?php echo $pasien_status_klinik['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Tanggal Kedatangan</label>
                                                <div class="col-md-9 controls">
                                                    <input type="date" name="confirm_date" class="form-control datepicker" value="<?php echo date("Y-m-d",strtotime($data->confirm_date));?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Content -->

</div>