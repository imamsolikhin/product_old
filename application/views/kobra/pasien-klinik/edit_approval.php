

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <?php $msg = $this->session->flashdata('msg'); ?>
    <?php if (isset($msg)): ?>
        <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php endif ?>

    <?php $error_msg = $this->session->flashdata('error_msg'); ?>
    <?php if (isset($error_msg)): ?>
        <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php endif ?>
    <form method="post" enctype="multipart/form-data" action="<?php echo base_url($services.'reqsapproval/'.$data->id) ?>" class="form-horizontal" novalidate>
        <div class="row">
        <!-- Column -->
            <div class="col-5">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30"> 
                            <div class="col-md-12">
                                <img style="cursor: pointer;" id="pasien_img" onclick="show_images('pasien_img')" alt="<?php echo  $data->gender." ".$data->name; ?>" src="<?php echo ($data->img_mid !="")? base_url($data->img_mid):base_url('assets/images/users/1.jpg'); ?>" class="img-thumbnail" width="280" height="250" id="profile_show" />
                            </div>
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="text"  class="custom-file-input" name="name" value="<?php echo $data->name; ?>" style="display:none;" >
                                    </div>
                                </div>
                            </div>
                            <h4 class="card-title m-t-10"><?php echo $data->gender.' '.$data->name.' ['.$data->age.' Th] '; ?></h4>
                            <div class="m-t-10">
                                <button type="submit" class="btn btn-success">Update <?php echo $page_title; ?></button>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab"><h2 style="font-weight:600;">Data Pasien</h2></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!--second tab-->
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $data->phone; ?></p>
                                        <br>
                                        <p class="text-muted"><?php echo $data->phone_wa; ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Description</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $data->emosional; ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $data->email; ?></p>
                                    </div>
                                </div>
                                <hr>
                                <p class="text-muted">Keluhan : <?php echo $data->complain; ?></p>
                                <p class="text-muted">Location : <?php echo $data->city.', '.$data->address; ?></p>
                                <hr>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Approval Status </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="approval_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_approval_status as $approval_status): ?>
                                                        <?php 
                                                            if($approval_status['name'] == $data->approval_status){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $approval_status['name']; ?>"><?php echo $approval_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">KODE</label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="approval_code" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="example-tools" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Pasien</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($list_pasien_closing as $pasien_closing): ?>
                                        <tr>
                                            <td>
                                            <img onclick="show_images('img_<?php echo $pasien_closing['id'];?>')" id="img_<?php echo $pasien_closing['id'];?>" style="cursor: pointer;" alt="<?php echo "Pasien ".$pasien_closing['created_by']."   :". $pasien_closing['gender'].' '.$pasien_closing['name']."<br>"; ?>" src="<?php echo base_url($pasien_closing['img_mid']) ?>" class="img-thumbnail" width="280" height="250" id="profile_show" /></td>
                                            <td>
                                                <?php echo "Nama    :". $pasien_closing['gender'].' '.$pasien_closing['name']."<br>"; ?>
                                                <?php echo "Keluhan :". $pasien_closing['complain']."<br>"; ?>
                                                <?php echo "Alamat  :". $pasien_closing['city'].' '.$pasien_closing['address']."<br>"; ?>
                                            </td>
                                            <td><?php echo "Pasien ".$pasien_closing['created_by']." <br> Konfirmasi :". $pasien_closing['confirm_status']?></td>
                                            <td><?php echo "KODE : ".md5($pasien_closing['id'])?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                function show_images(id_img){
                    var modal = document.getElementById("myModal");
                        modal.style.display = "block";
                    var img = document.getElementById(id_img);
                    var modalImg = document.getElementById("img-modal");
                        modalImg.src = img.src;
                    var captionText = document.getElementById("caption");
                        captionText.innerHTML = img.alt;
                    var span = document.getElementsByClassName("close")[0];
                        span.onclick = function() {
                            modal.style.display = "none";
                        }
                }
            </script>
            <style>

                /* The Modal (background) */
                .modal {
                    display: none; /* Hidden by default */
                    position: fixed; /* Stay in place */
                    z-index: 1; /* Sit on top */
                    padding-top: 100px; /* Location of the box */
                    left: 0;
                    top: 0;
                    width: 100%; /* Full width */
                    height: 100%; /* Full height */
                    overflow: auto; /* Enable scroll if needed */
                    background-color: rgb(0,0,0); /* Fallback color */
                    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                }

                /* Modal Content (Image) */
                .modal-content {
                    margin: auto;
                    display: block;
                    width: 80%;
                    max-width: 700px;
                }

                /* Caption of Modal Image (Image Text) - Same Width as the Image */
                #caption {
                    margin: auto;
                    display: block;
                    width: 80%;
                    max-width: 700px;
                    text-align: center;
                    color: #ccc;
                    padding: 10px 0;
                    height: 150px;
                }

                /* Add Animation - Zoom in the Modal */
                .modal-content, #caption {
                    animation-name: zoom;
                    animation-duration: 0.6s;
                }

                @keyframes zoom {
                    from {transform:scale(0)}
                    to {transform:scale(1)}
                }

                /* The Close Button */
                .close {
                    position: absolute;
                    top: 5rem;
                    right: 35px;
                    color: #fff;
                    font-size: 40px;
                    font-weight: bold;
                    transition: 0.3s;
                }

                .close:hover,.close:focus {
                    color: #bbb;
                    text-decoration: none;
                    cursor: pointer;
                }
            </style>
            <div id="myModal" class="modal">
                <img class="modal-content" id="img-modal">
                <span class="close">&times;</span>
                <div id="caption"></div>
            </div>
        </div>
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" >
    </form>
</div>  