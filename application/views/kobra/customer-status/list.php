

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <div class="card">

                <div class="card-body">
                    <a href="<?php echo base_url($services.'/add') ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New <?php echo $page_title; ?></a>
                

                    <div class="table-responsive m-t-40">
                        <table id="example-tools" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>

<script type="text/javascript">
    var code; 
    var table;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd+'-'+ mm +'-'+ yyyy;

    $(document).ready(function(){
        table = $('#example-tools').DataTable({
            "serverSide": true,  
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "dataType": 'json',
            "ajax": {
            "url": "<?php echo site_url('Api/ajax_list')?>",
            "data":{ 
                table: "<?php echo $table ?>",
                services: "<?php echo $services ?>",
                '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash();?>' 
            },
            "dataSrc": function ( json ) {
               if(json.csrf_token !== undefined) $('meta[name=csrf_token]').attr("content", json.csrf_token);
               return json.data;
            },
            "type": "POST",
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "initComplete": function(settings, json) {
                $('#example-tools_filter input').unbind();
                $('#example-tools_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search( this.value ).draw();
                    }
                }); 
            }
        });  
    });

    function delete_data(code,name){
        $(".modal-title").text(name);
        $("#myModal").modal();
        document.getElementById("delete_submit").href="<?php echo site_url($services.'delete/')?>"+code;
    }

</script>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">		
				<h4 class="modal-title">Are you sure?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<a type="button" class="btn btn-danger" href="#" id="delete_submit">Delete</a>
			</div>
		</div>
	</div>
</div>  