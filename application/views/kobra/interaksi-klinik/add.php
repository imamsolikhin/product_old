

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add New <?php echo $page_title; ?></li>
            </ol>
        </div>
        
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Add New <?php echo $page_title; ?> <a href="<?php echo base_url($services) ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All <?php echo $page_title; ?> </a></h4>

                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo base_url($services.'add') ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Klink || Ship Kerja</label>
                                        <div class="col-md-5 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="klinik" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_klinik as $klinik): ?>
                                                        <option value="<?php echo $klinik['name']; ?>"><?php echo $klinik['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="ship_kerja" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_ship_kerja as $ship_kerja): ?>
                                                        <option value="<?php echo $ship_kerja['name']; ?>"><?php echo $ship_kerja['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Iklan || Interaksi</label>
                                        <div class="col-md-5 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="advertise" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_advertise_klinik as $advertise_klinik): ?>
                                                        <option value="<?php echo $advertise_klinik['name']; ?>"><?php echo $advertise_klinik['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="interaksi_kategory" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_interaksi_kategory as $interaksi_kategory): ?>
                                                        <option value="<?php echo $interaksi_kategory['name']; ?>"><?php echo $interaksi_kategory['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                    <label class="control-label text-right col-md-3">URL || ID Iklan </label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="id_iklan" class="form-control" required data-validation-required-message="Last Name is required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Nama Lengkap<span class="text-danger">*</span></label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="name" class="form-control" required data-validation-required-message="Last Name is required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Jenis Kelamin </label>
                                        <div class="col-md-4 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="gender" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_gender as $gender): ?>
                                                        <option value="<?php echo $gender['name']; ?>"><?php echo $gender['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-1">Umur</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="age" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tlp</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="phone" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-1">Wa</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="phone_wa" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Kota</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="city" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-1">Email</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="email" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Alamat</label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="address" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Keluhan</label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="complain" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Emosional Pasien </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="emosional" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_emosional as $emosional): ?>
                                                        <option value="<?php echo $emosional['name']; ?>"><?php echo $emosional['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Status Pasien </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="pasien_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_pasien_status as $pasien_status): ?>
                                                        <option value="<?php echo $pasien_status['name']; ?>"><?php echo $pasien_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Konfirm Pasien </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="confirm_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_confirm_status as $confirm_status): ?>
                                                        <option value="<?php echo $confirm_status['name']; ?>"><?php echo $confirm_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tanggal Janji</label>
                                        <div class="col-md-9 controls">
                                            <input type="date" name="confirm_date" class="form-control datepicker">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="col-md-9 controls checkbox checkbox-primary">
                                            <input id="checkbox2" class="styled" type="checkbox" checked name="is_stay">
                                            <label for="checkbox2"><b style="font-weight:600;">stay on the page.</b></label>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                            
                            <hr>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-success">Save <?php echo $page_title; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Content -->

</div>