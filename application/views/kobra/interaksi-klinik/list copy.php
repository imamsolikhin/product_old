

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <div class="card">

                <div class="card-body">
                    <a href="<?php echo base_url($services.'/add') ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New <?php echo $page_title; ?></a>
                    <!-- <p id="date_filter">
                        <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
                        <span id="date-label-to" class="date-label">To:<input class="date_range_filter date" type="text" id="datepicker_to" />
                    </p> -->
                    <div class="table-responsive m-t-40">
                        <table id="example-tools" class="display nowrap table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="white-space: nowrap;"><b>#No</b></th>
                                    <th style="white-space: nowrap;">Name Pasien</th>
                                    <th style="white-space: nowrap;">Kontak Pasien</th>
                                    <th style="white-space: nowrap;">Data Pasien</th>
                                    <th style="white-space: nowrap;">Data Iklan</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <th>Name Pasien</th>
                                    <th>Data Pasien</th>
                                    <th>Data Kendala</th>
                                    <th>Data Iklan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot> -->
                            
                            <tbody>
                                
                            <?php $no = 0; 
                            foreach ($list_data as $data): 
                                $no = $no+1;
                            ?>
                                
                                <tr>
                                    <td><?php echo $no;?></td>
                                    <td style="white-space: nowrap;"><?php echo $data['gender']." ".$data['name']." <br> Karakter : ".$data['emosional']."<br> <b style='font-weight:900;'>ACTION : </b>"; ?>
                                    <?php if ($this->session->userdata('role') == 'admin'): ?>
                                        <a href="<?php echo base_url($services.'update/'.$data['id']) ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a>

                                        <a id="delete" data-toggle="modal" data-target="#confirm_delete_<?php echo $data['id'];?>" href="#"  data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>


                                    <?php else: ?>

                                        <!-- check logged user role permissions -->

                                        <?php if(check_power(2)):?>
                                            <a href="<?php echo base_url($services.'update/'.$data['id']) ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-success m-r-10"></i> </a>
                                        <?php endif; ?>
                                        <?php if(check_power(3)):?>
                                            <a href="<?php echo base_url($services.'delete/'.$data['id']) ?>" onClick="return doconfirm();" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-danger m-r-10"></i> </a>
                                        <?php endif; ?>

                                    <?php endif ?>
                                </td>
                                <td style="white-space: nowrap;"><?php echo "No Telp : ".$data['phone']." <br> No WA : ".$data['phone_wa']." <br> Kedatangan : ".date("d-m-Y", strtotime($data['confirm_date'])); ?></td>
                                <td style="white-space: nowrap;"><?php echo "Kota : ".$data['city']."<br> Alamat : ".$data['address']." <br> Keluhan : ".$data['complain']; ?></td>
                                <td style="white-space: nowrap;"><?php echo "Iklan : ".$data['advertise'].", ".$data['interaksi_kategory']." <br> ID Iklan : ".$data['id_iklan']." <br> Ship Kerja : ".$data['ship_kerja']; ?></td>

                            <?php endforeach ?>

                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>



<?php foreach ($list_data as $data): ?>
 
<div class="modal fade" id="confirm_delete_<?php echo $data['id'];?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">DATA: <?php echo $data['gender']." ".$data['name']; ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

            <div class="form-body">
                Are you sure want to delete? <br> <hr>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href="<?php echo base_url($services.'delete/'.$data['id']) ?>" class="btn btn-danger"> Delete</a>
                
            </div>

      </div>


    </div>
  </div>
</div>


<?php endforeach ?>