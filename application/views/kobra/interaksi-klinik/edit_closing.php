

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <?php $msg = $this->session->flashdata('msg'); ?>
    <?php if (isset($msg)): ?>
        <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php endif ?>

    <?php $error_msg = $this->session->flashdata('error_msg'); ?>
    <?php if (isset($error_msg)): ?>
        <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php endif ?>
    <form method="post" enctype="multipart/form-data" action="<?php echo base_url($services.'reqsclosing/'.$data->id) ?>" class="form-horizontal" novalidate>
        <div class="row">
        <!-- Column -->
            <div class="col-5">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30"> 
                            <div class="col-md-12">
                                <img src="<?php echo ($data->img_mid !="")? base_url($data->img_mid):base_url('assets/images/users/1.jpg'); ?>" class="img-thumbnail" width="280" height="250" id="profile_show" />
                            </div>
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="photo" id="profile_upload" onchange="readURL(this)">
                                        <input type="text" class="custom-file-input" name="name" value="<?php echo $data->name; ?>" style="display:none;" >
                                    </div>
                                </div>
                            </div>
                            <h4 class="card-title m-t-10"><?php echo $data->gender.' '.$data->name.' ['.$data->age.' Th] '; ?></h4>
                            <h6 class="card-subtitle"><?php echo $data->confirm_status; ?></h6>
                            <h6 class="card-subtitle"><?php echo date("d-m-Y",strtotime($data->confirm_date)); ?></h6>
                            <div class="m-t-10">
                                <button type="submit" class="btn btn-success">Update <?php echo $page_title; ?></button>
                            </div>
                        </center>
                            <script>
                                function readURL(input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();
                                        reader.onload = function (e) {
                                            $('#profile_show')
                                                .attr('src', e.target.result)
                                                .width(280)
                                                .height(250);
                                        };
                                        reader.readAsDataURL(input.files[0]);
                                    }
                                }
                                $("#profile_show").click(function() {
                                    $("#profile_upload").click();
                                });
                            </script>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab"><h2 style="font-weight:600;">Data Pasien</h2></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!--second tab-->
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $data->phone; ?></p>
                                        <br>
                                        <p class="text-muted"><?php echo $data->phone_wa; ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Description</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $data->emosional; ?></p>
                                        <br>
                                        <p class="text-muted"><?php echo $data->pasien_status; ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $data->email; ?></p>
                                    </div>
                                </div>
                                <hr>
                                <p class="text-muted">Keluhan : <?php echo $data->complain; ?></p>
                                <p class="text-muted">Location : <?php echo $data->city.', '.$data->address; ?></p>
                                <hr>
                                <h4 class="card-title m-t-10"><u>Keterangan Closing :</u></h4>
                                <textarea type="text" name="remark" class="form-control" placeholder="Please Insert Description closing." value="<?php echo $data->remark; ?>"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Column -->
        </div>
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" >
    </form>


    <!-- End Page Content -->

</div>