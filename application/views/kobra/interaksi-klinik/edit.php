

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">

                
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Edit <?php echo $page_title; ?> <a href="<?php echo base_url($services) ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All Iklans </a></h4>
                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo base_url($services.'update/'.$data->id) ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Klinik || Ship Kerja</label>
                                        <div class="col-md-5 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="klinik" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_klinik as $klinik): ?>
                                                        <?php 
                                                            if($klinik['name'] == $data->klinik){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $klinik['name']; ?>"><?php echo $klinik['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="ship_kerja" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_ship_kerja as $ship_kerja): ?>
                                                        <?php 
                                                            if($ship_kerja['name'] == $data->ship_kerja){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $ship_kerja['name']; ?>"><?php echo $ship_kerja['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Iklan || Interaksi</label>
                                        <div class="col-md-5 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="advertise" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_advertise_klinik as $advertise_klinik): ?>
                                                        <?php 
                                                            if($advertise_klinik['name'] == $data->advertise){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $advertise_klinik['name']; ?>"><?php echo $advertise_klinik['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="interaksi_kategory" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_interaksi_kategory as $interaksi_kategory): ?>
                                                        <?php 
                                                            if($interaksi_kategory['name'] == $data->interaksi_kategory){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $interaksi_kategory['name']; ?>"><?php echo $interaksi_kategory['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                    <label class="control-label text-right col-md-3">URL || ID Iklan </label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="id_advertise_klinik" class="form-control" required data-validation-required-message="ID IKlan is required" value="<?php echo $data->id_advertise_klinik; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Nama Lengkap<span class="text-danger">*</span></label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="name" class="form-control" required data-validation-required-message="Nama Lengkap is required" value="<?php echo $data->name; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Jenis Kelamin </label>
                                        <div class="col-md-4 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="gender" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_gender as $gender): ?>
                                                        <?php 
                                                            if($gender['name'] == $data->gender){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $gender['name']; ?>"><?php echo $gender['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-1">Umur</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="age" class="form-control" value="<?php echo $data->age; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tlp</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="phone" class="form-control" value="<?php echo $data->phone; ?>" >
                                        </div>
                                        <label class="control-label text-right col-md-1">Wa</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="phone_wa" class="form-control" value="<?php echo $data->phone_wa; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Kota</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="city" class="form-control" value="<?php echo $data->city; ?>" >
                                        </div>
                                        <label class="control-label text-right col-md-1">Email</label>
                                        <div class="col-md-4 controls">
                                            <input type="text" name="email" class="form-control" value="<?php echo $data->email; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Alamat</label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="address" class="form-control" value="<?php echo $data->address; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Keluhan</label>
                                        <div class="col-md-9 controls">
                                            <input type="text" name="complain" class="form-control" value="<?php echo $data->complain; ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Emosional Pasien </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="emosional" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_emosional as $emosional): ?>
                                                        <?php 
                                                            if($emosional['name'] == $data->emosional){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $emosional['name']; ?>"><?php echo $emosional['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Status Pasien </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="pasien_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_pasien_status as $pasien_status): ?>
                                                        <?php 
                                                            if($pasien_status['name'] == $data->pasien_status){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $pasien_status['name']; ?>"><?php echo $pasien_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Konfirm Pasien </label>
                                        <div class="col-md-9 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="confirm_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_confirm_status as $confirm_status): ?>
                                                        <?php 
                                                            if($confirm_status['name'] == $data->confirm_status){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $confirm_status['name']; ?>"><?php echo $confirm_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tanggal Janji</label>
                                        <div class="col-md-9 controls">
                                            <input type="date" name="confirm_date" class="form-control datepicker" value="<?php echo date("Y-m-d",strtotime($data->confirm_date)); ?>" >
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" >
                            <hr>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-success">Save <?php echo $page_title; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>