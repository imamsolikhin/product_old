

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">

                
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Edit <?php echo $page_title; ?> <a href="<?php echo base_url($services) ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All <?php echo $page_title; ?> </a></h4>

                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo base_url($services.'update/'.$data->id) ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Produk <span class="text-danger">*</span></label>
                                        <div class="col-md-9 controls">
                                            <select value="<?php echo $data->produk;?>" class="form-control custom-select" name="produk" aria-invalid="false">
                                                <option value="0">Produk</option>
                                                <?php foreach ($list_produk as $produk): ?>
                                                    <?php 
                                                        if($produk['name'] == $data->produk){
                                                            $selec = 'selected';
                                                        }else{
                                                            $selec = '';
                                                        }
                                                    ?>
                                                    <option <?php echo $selec; ?> value="<?php echo $produk['name']; ?>"><?php echo $produk['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tanggal <span class="text-danger">*</span></label>
                                        <div class="col-md-9 controls">
                                           <input value="<?php echo date("Y-m-d",strtotime($data->tanggal)); ?>" type="date" name="tanggal" class="form-control datepicker" required data-validation-required-message="Tanggal Barang Di isi">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Jumlah <span class="text-danger">*</span></label>
                                        <div class="col-md-9 controls">
                                            <input value="<?php echo $data->total; ?>" type="number" name="total" class="form-control" required data-validation-required-message="Jumlah Harap di isi">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Keterangan <span class="text-danger">*</span></label>
                                        <div class="col-md-9 controls">
                                            <textarea value="<?php echo $data->remark; ?>" type="text" name="remark" class="form-control" required data-validation-required-message="Keterangan harap di isi."> <?php echo $data->remark; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                            
                            <hr>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-success">Save <?php echo $page_title; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>