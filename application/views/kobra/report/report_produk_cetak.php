<?php
// require_once('tcpdf.php');
// Extend the TCPDF class to create custom Header and Footer

$logo = $data_company->LogoPath;
class MYPDF extends TCPDF {
	//Page header
	public function Header() {
		// Logo
		// $margin = $this->getMargin();
		// $this->SetMargins(20,20,20,true);
		// $image_file = K_PATH_IMAGES.PDF_HEADER_LOGO;
		$image_file = K_PATH_IMAGES.PDF_HEADER_LOGO;
		$this->Image($image_file, 10, 2 , 15, 15, PDF_HEADER_LOGO_EXT, '', 'T', false, 100, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 12);
		// Title
		// $this->Cell(0, 10, PDF_HEADER_TITLE, 0, true, 'C', 0, '', 0, false, 'N', 'M');
		$this->Cell(0, 0, '', 0, true, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 10, PDF_HEADER_DIR, 0, true, 'C', 0, '', 0, false, 'M', 'M');

		// detail
		$this->SetFont('helvetica', 'B', 8);
		$this->Cell(0, 10, PDF_HEADER_LOC, 0, true, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 10, PDF_HEADER_WEB, 0, true, 'C', 0, '', 0, false, 'M', 'M');

		$this->SetLineWidth(1);
		$this->Line(10,18,205,18);
		$this->SetLineWidth(0);
		$this->Line(10,19,205,19);
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT_A4, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('devjrcamp');
$pdf->SetTitle('LAPORAN-MARKETING-KLINIK.pdf');
$pdf->SetSubject('LAPORAN-MARKETING-KLINIK');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 11);

// add a page
$pdf->AddPage();


// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
// $val1 = ($result_cuti->ctJenis_cuti == 1) ? "V" : "" ;
// $val2 = ($result_cuti->ctJenis_cuti == 2) ? "V" : "" ;
// $val3 = ($result_cuti->ctJenis_cuti == 3) ? "V" : "" ;
// $val4 = ($result_cuti->ctJenis_cuti == 4) ? "V" : "" ;
// $val5 = ($result_cuti->ctJenis_cuti == 5) ? "V" : "" ;
// $val6 = ($result_cuti->ctJenis_cuti == 6) ? "V" : "" ;
// $ket_cnf1 = ($result_cuti->ctSts_approved_cnf == "PERUBAHAN") ? $result_cuti->ctKet_approved_cnf : "";
// $ket_cnf2 = ($result_cuti->ctSts_approved_cnf == "DITANGGUHKAN") ? $result_cuti->ctKet_approved_cnf : "";
// $ket_acc1 = ($result_cuti->ctSts_approved_acc == "PERUBAHAN") ? $result_cuti->ctKet_approved_acc : ""; 
// $ket_acc2 = ($result_cuti->ctSts_approved_acc == "DITANGGUHKAN") ? $result_cuti->ctKet_approved_acc : ""; 

// $sts_cnf1 = ($result_cuti->ctSts_approved_cnf == "DISETUJUI") ? "V" : "";
// $sts_cnf2 = ($result_cuti->ctSts_approved_cnf == "PERUBAHAN") ? "V" : "";
// $sts_cnf3 = ($result_cuti->ctSts_approved_cnf == "DITANGGUHKAN") ? "V" : "";
// $sts_cnf4 = ($result_cuti->ctSts_approved_cnf == "TIDAK_DISETUJUI") ? "V" : "";
// $sts_acc1 = ($result_cuti->ctSts_approved_acc == "DISETUJUI") ? "V" : "";
// $sts_acc2 = ($result_cuti->ctSts_approved_acc == "PERUBAHAN") ? "V" : "";
// $sts_acc3 = ($result_cuti->ctSts_approved_acc == "DITANGGUHKAN") ? "V" : "";
// $sts_acc4 = ($result_cuti->ctSts_approved_acc == "TIDAK_DISETUJUI") ? "V" : "";
// create some HTML content
$html = '
<style>
	.tab-title{padding-left: 200px}
	.tb-j{width:95%}
	.tb-i{width:85%}
	.tb-h{width:75%}
	.tb-g{width:65%}
	.tb-f{width:55%}
	.tb-e{width:45%}
	.tb-d{width:35%}
	.tb-c{width:25%}
	.tb-b{width:15%}
	.tb-a{width:5%}
	.tb-ii{width:90%}
	.tb-hh{width:80%}
	.tb-gg{width:70%}
	.tb-ff{width:60%}
	.tb-ee{width:50%}
	.tb-dd{width:40%}
	.tb-cc{width:30%}
	.tb-bb{width:20%}
	.tb-aa{width:10%}
	.tb-st{width:5px}
	.tb-ta-l{text-align:left}
	.tb-ta-c{text-align:center}
	.tb-ta-r{text-align:right}
</style>

<table style="width:100%" border="0">
	<tr>
		<td>
			<table style="width:100%">
				<tr>
					<td style="border: 1px solid black;" colspan="4">I.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA PEGAWAI</td>
				</tr>
			</table>
		</td>
	</tr>
	<br>
	<tr>
		<td>
			<table style="width:100%">
				<tr>
					<td class="tb-b tb-ta-l"><u>Catatan</u></td>
					<td class="tb-i tb-ta-l"></td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">*</td>
					<td class="tb-i tb-ta-l">: Coret yang tidak perlu</td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">**</td>
					<td class="tb-i tb-ta-l">: Pilih salasatu dengan memberi tanda centang (V)</td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">***</td>
					<td class="tb-i tb-ta-l">: Di isi oleh pejabat yang menangani bidang kepegawaian sebelum PNS mengajukan cuti</td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">****</td>
					<td class="tb-i tb-ta-l">: Diberi tanda centang dan alasannya</td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">N</td>
					<td class="tb-i tb-ta-l">: Cuti tahun berjalan</td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">N-1</td>
					<td class="tb-i tb-ta-l">: Sisa cuti 1 tahun sebelumnya</td>
				</tr>
				<tr>
					<td class="tb-b tb-ta-l">N-2</td>
					<td class="tb-i tb-ta-l">: Sisa cuti 2 tahun sebelumnya</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('LAPORAN-MARKETING-KLINIK.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
