

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Report Editor</h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <!-- <form method="post" action="<?php echo base_url($services.'report_klinik') ?>" class="form-horizontal" novalidate> -->
                                                <h4 class="card-title">FILTER REPORT</h4>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <h5 class="m-t-30 m-b-10">Start Date</h5>
                                                        <input type="date" id="start_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <h5 class="m-t-30 m-b-10">End Date</h5>
                                                        <input type="date" id="end_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5 class="m-t-30">KLINIK</h5>
                                                        <select class="form-control custom-select" id="klinik" aria-invalid="false">
                                                            <!-- <option value="0">Select</option> -->
                                                            <?php foreach ($list_klinik as $klinik): ?>
                                                                <option value="<?php echo $klinik['name']; ?>"><?php echo $klinik['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5 class="m-t-30">Ship Kerja Report</h5>
                                                        <select class="form-control custom-select" id="shipkerja" aria-invalid="false">
                                                            <!-- <option value="0">Select</option> -->
                                                            <?php foreach ($list_ship_kerja as $ship_kerja): ?>
                                                                <option value="<?php echo $ship_kerja['name']; ?>"><?php echo $ship_kerja['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- CSRF token -->
                                                <!-- <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" /> -->
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group row">
                                                            <label class="control-label text-right col-md-3"></label>
                                                            <div class="controls">
                                                                <button type=" button" id="generade" class="btn btn-success">GENERADE</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script>
                                                    $('#generade').click(function(){
                                                        var options = {
                                                            klinik:$('#klinik').val(),
                                                            shipkerja:$('#shipkerja').val(),
                                                            start_date: $('#start_date').val(),
                                                            end_date: $('#end_date').val()
                                                        };
                                                        var url = new URL('<?php echo base_url($services.'report_klinik') ?>');
                                                        url.searchParams.set('klinik',$('#klinik').val())
                                                        url.searchParams.set('shipkerja',$('#shipkerja').val())
                                                        url.searchParams.set('start_date', $('#start_date').val())
                                                        url.searchParams.set('end_date', $('#end_date').val())
                                                        // alert(url);return;
                                                        proses_print(url);
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>