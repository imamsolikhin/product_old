

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <?php $msg = $this->session->flashdata('msg'); ?>
    <?php if (isset($msg)): ?>
        <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php endif ?>

    <?php $error_msg = $this->session->flashdata('error_msg'); ?>
    <?php if (isset($error_msg)): ?>
        <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php endif ?>
    <form method="post" enctype="multipart/form-data" action="<?php echo base_url($services.'reqsapproval/'.$data->id) ?>" class="form-horizontal" novalidate>
        <div class="row">
        <!-- Column -->
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30"> 
                            <div class="col-md-12">
                                <img src="<?php echo ($data->delivery_img_mid !="")? base_url($data->delivery_img_mid):base_url('assets/images/users/1.jpg'); ?>" class="img-thumbnail" width="280" height="250" id="profile_show" />
                            </div>
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="delivery_photo" id="profile_upload" onchange="readURL(this)">
                                        <input type="text" class="custom-file-input" name="nama_lengkap" value="<?php echo $data->nama_lengkap; ?>" style="display:none;" >
                                    </div>
                                </div>
                            </div>
                            <h4 class="card-title m-t-10"><?php echo $data->created_by; ?></h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Approval Status</label>
                                        <div class="col-md-8 controls">
                                            <select class="form-control custom-select" name="closing_approval" aria-invalid="false" value="<?php echo $data->closing_approval; ?>">
                                                <?php if($data->closing_approval){ ?>
                                                    <option value="0">PENDING</option>
                                                    <option value="1" selected>APPROVED</option>
                                                <?php }else{ ?>
                                                    <option value="0" selected>PENDING</option>
                                                    <option value="1">APPROVED</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Delivery Status</label>
                                        <div class="col-md-8 controls">
                                            <div class="form-group has-success">
                                                <select value="<?php echo $data->delivery_status;?>" class="form-control custom-select" name="delivery_status" aria-invalid="false">
                                                    <option value="PENDING">Pilih</option>
                                                    <?php foreach ($list_delivery_status as $delivery_status): ?>
                                                        <?php 
                                                            if($delivery_status['name'] == $data->delivery_status){
                                                                $selec = 'selected';
                                                            }else{
                                                                $selec = '';
                                                            }
                                                        ?>
                                                        <option <?php echo $selec; ?> value="<?php echo $delivery_status['name']; ?>"><?php echo $delivery_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>
                        <script>
                            function readURL(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    reader.onload = function (e) {
                                        $('#profile_show')
                                            .attr('src', e.target.result)
                                            .width(280)
                                            .height(250);
                                    };
                                    reader.readAsDataURL(input.files[0]);
                                }
                            }
                            $("#profile_show").click(function() {
                                $("#profile_upload").click();
                            });
                        </script>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab"><h2 style="font-weight:600;">Data Pasien</h2></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!--second tab-->
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <hr>
                                        <p class="text-muted"><?php echo $data->tlp; ?></p>
                                        <p class="text-muted"><?php echo $data->wa; ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Transaksi</strong>
                                        <hr>
                                        <p class="text-muted"><?php echo (int)$data->jml_pesanan." ".$data->customer_status; ?></p>
                                        <p class="text-muted"><?php echo $data->total_transaksi_id; ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"> <strong>Description</strong>
                                        <hr>
                                        <p class="text-muted"><?php echo $data->type_transaction." ".$data->kurir; ?></p>
                                        <p class="text-muted"><?php echo $data->akun_market." ".$data->bank; ?></p>
                                    </div>
                                </div>
                                <hr>
                                <p class="text-muted">Nama Customer : <?php echo $data->jenis_kelamin.' '.$data->nama_lengkap.' ['.$data->umur.' Th] '; ?></p>
                                <p class="text-muted">Tanggal Order : <?php echo date("d-m-Y",strtotime($data->tanggal_order)); ?></p>
                                <p class="text-muted">Alamat Lengkap: <?php echo $data->alamat_lengkap; ?></p>
                                <hr>
                                <h4 class="card-title m-t-10"><u>Nomor Order :</u></h4>
                                <input type="text" name="delivery_order" class="form-control" value="<?php echo ($data->delivery_order=='')? 'AUTO':$data->delivery_order; ?>" style="text-align: center;" readonly />
                                <br/>
                                <br/>
                                <button type="submit" class="btn btn-success">Update <?php echo $page_title; ?></button>
                                <a href="<?php echo base_url($services.'approval') ?>" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Column -->
        </div>
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" >
    </form>


    <!-- End Page Content -->

</div>