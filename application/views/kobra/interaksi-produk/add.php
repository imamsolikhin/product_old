

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add New <?php echo $page_title; ?></li>
            </ol>
        </div>
        
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Add New <?php echo $page_title; ?> <a href="<?php echo base_url($services) ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All <?php echo $page_title; ?> </a></h4>
                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo base_url($services.'add') ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Produk || Ship Kerja</label>
                                        <div class="col-md-5 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="produk" aria-invalid="false">
                                                    <option value="0">Produk</option>
                                                    <?php foreach ($list_produk as $produk): ?>
                                                        <option value="<?php echo $produk['name']; ?>"><?php echo $produk['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="ship_kerja" aria-invalid="false">
                                                    <option value="0">Ship Kerja</option>
                                                    <?php foreach ($list_ship_kerja as $ship_kerja): ?>
                                                        <option value="<?php echo $ship_kerja['name']; ?>"><?php echo $ship_kerja['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Iklan || Interaksi</label>
                                        <div class="col-md-5 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="advertise" aria-invalid="false">
                                                    <option value="0">Iklan</option>
                                                    <?php foreach ($list_advertise_produk as $advertise_produk): ?>
                                                        <option value="<?php echo $advertise_produk['name']; ?>"><?php echo $advertise_produk['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="interaksi_kategory" aria-invalid="false">
                                                    <option value="0">Interaksi</option>
                                                    <?php foreach ($list_interaksi_kategory as $interaksi_kategory): ?>
                                                        <option value="<?php echo $interaksi_kategory['name']; ?>"><?php echo $interaksi_kategory['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                    <label class="control-label text-right col-md-3">HP CS</label>
                                        <div class="col-md-8 controls">
                                            <input type="text" name="id_iklan" class="form-control" required data-validation-required-message="Last Name is required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Nama Lengkap<span class="text-danger">*</span></label>
                                        <div class="col-md-8 controls">
                                            <input type="text" name="nama_lengkap" class="form-control" required data-validation-required-message="Last Name is required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Jenis Kelamin </label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="jenis_kelamin" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_gender as $gender): ?>
                                                        <option value="<?php echo $gender['name']; ?>"><?php echo $gender['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-2">Umur</label>
                                        <div class="col-md-3 controls">
                                            <input type="text" name="umur" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Konfirm Status </label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="confirm_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_confirm_status as $confirm_status): ?>
                                                        <option value="<?php echo $confirm_status['name']; ?>"><?php echo $confirm_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-2">Keluhan</label>
                                        <div class="col-md-3 controls">
                                            <input type="text" name="keluhan" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tlp</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="tlp" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-2">Wa</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="wa" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Alamat</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="alamat" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-2">No. Rumah</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="no_rumah" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Rt</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="rt" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-2">Rw</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="rw" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Desa</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="desa" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-2">Kelurahan</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="kelurahan" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Patokan</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="patokan" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-2">Kecamatan</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Kota</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="kota" class="form-control" >
                                        </div>
                                        <label class="control-label text-right col-md-2">Provinsi</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="provinsi" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11" hidden>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Alamat Lengkap<span class="text-danger">*</span></label>
                                        <div class="col-md-8 controls">
                                            <input type="text" name="alamat_lengkap" class="form-control" required data-validation-required-message="Last Name is required">
                                        </div>
                                        <script type="text/javascript">
                                            $( document ).ready(function() {
                                                // $("input name=''").keypress(function(){

                                                // });
                                            });

                                            function format_alamat(){
                                                var d1 = $("input[name='alamat']").val()=="" ? "": ""+$("input[name='alamat']").val();
                                                var d2 = $("input[name='no_rumah']").val()=="" ? "": " No. "+$("input[name='no_rumah']").val();
                                                var d3 = $("input[name='rt']").val()=="" ? "": " Rt "+$("input[name='rt']").val();
                                                var d4 = $("input[name='rw']").val()=="" ? "": " Rw "+$("input[name='rw']").val();
                                                var d5 = $("input[name='desa']").val()=="" ? "": ", Desa "+$("input[name='desa']").val();
                                                var d6 = $("input[name='kelurahan']").val()=="" ? "": " Kel. "+$("input[name='kelurahan']").val();
                                                var d7 = $("input[name='patokan']").val()=="" ? "": " "+$("input[name='patokan']").val();
                                                var d8 = $("input[name='kecamatan']").val()=="" ? "": ", kec. "+$("input[name='kecamatan']").val();
                                                var d9 = $("input[name='kota']").val()=="" ? "": " "+$("input[name='kota']").val();
                                                var d10 = $("input[name='provinsi']").val()=="" ? "": ", "+$("input[name='provinsi']").val();
                                                var d11 = $("input[name='kode_pos']").val()=="" ? "": " "+$("input[name='kode_pos']").val();

                                                $("input[name='alamat_lengkap']").val(d1+d2+d3+d4+d5+d6+d7+d8+d9+d10+d11);

                                            }
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Emosional Customer </label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="emosional" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_emosional as $emosional): ?>
                                                        <option value="<?php echo $emosional['name']; ?>"><?php echo $emosional['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-2">Kode Pos</label>
                                        <div class="col-md-3 controls">
                                            <input onchange="format_alamat()" type="text" name="kode_pos" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Jml Pesanan</label>
                                        <div class="col-md-3 controls">
                                            <input type="number" name="jml_pesanan" class="form-control" onchange="onchangeCalculation()" value="0">
                                        </div>
                                        <label class="control-label text-right col-md-2">Harga</label>
                                        <div class="col-md-3 controls">
                                            <input type="number" name="harga" class="form-control" onchange="onchangeCalculation()" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Biaya Pengiriman</label>
                                        <div class="col-md-3 controls">
                                            <input type="number" name="biaya_pengiriman" class="form-control" onchange="onchangeCalculation()" value="0">
                                        </div>
                                        <label class="control-label text-right col-md-2">Asuransi</label>
                                        <div class="col-md-3 controls">
                                            <input type="number" name="asuransi" class="form-control" onchange="onchangeCalculation()" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Total Transaksi</label>
                                        <div class="col-md-8 controls">
                                            <input readonly type="text" name="transaksi" class="form-control datepicker">
                                            <input hidden type="text" name="total_transaksi" class="form-control datepicker">
                                        </div>
                                        <script type="text/javascript">
                                            $( document ).ready(function() {
                                                // $("input name=''").keypress(function(){

                                                // });
                                            });

                                            function onchangeCalculation(){
                                                 var jml = parseFloat($("input[name='jml_pesanan']").val());
                                                 var harga = parseFloat($("input[name='harga']").val());
                                                 var biaya = parseFloat($("input[name='biaya_pengiriman']").val());
                                                 var asuransi = parseFloat($("input[name='asuransi']").val());

                                                 var total = ((jml*harga)+(biaya+asuransi));
                                                 $("input[name='transaksi']").val(convertToRupiah(total));
                                                 $("input[name='total_transaksi']").val(total);

                                            }
                                            function convertToRupiah(angka){
                                                var rupiah = '';        
                                                var angkarev = angka.toString().split('').reverse().join('');
                                                for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                                                return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
                                            }
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Kurir Pengiriman</label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="kurir" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_kurir as $kurir): ?>
                                                        <option value="<?php echo $kurir['name']; ?>"><?php echo $kurir['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-2">COD/TF</label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="type_transaction" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_type_transaction as $type_transaction): ?>
                                                        <option value="<?php echo $type_transaction['name']; ?>"><?php echo $type_transaction['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Akun Market</label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="akun_market" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_akun_market as $akun_market): ?>
                                                        <option value="<?php echo $akun_market['name']; ?>"><?php echo $akun_market['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-2">Bank</label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="bank" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_bank as $bank): ?>
                                                        <option value="<?php echo $bank['name']; ?>"><?php echo $bank['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Reqs. Status</label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="reqs_status" aria-invalid="false">
                                                    <option value="NO" selected>NO</option>
                                                    <option value="YES">YES</option>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="control-label text-right col-md-2">Order Customer</label>
                                        <div class="col-md-3 controls">
                                            <div class="form-group has-success">
                                                <select class="form-control custom-select" name="customer_status" aria-invalid="false">
                                                    <option value="0">Select</option>
                                                    <?php foreach ($list_customer_status as $customer_status): ?>
                                                        <option value="<?php echo $customer_status['name']; ?>"><?php echo $customer_status['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Tanggal Order</label>
                                        <div class="col-md-3 controls">
                                            <input type="date" name="tanggal_order" class="form-control datepicker">
                                        </div>
                                        <label class="control-label text-right col-md-2">Etimasi</label>
                                        <div class="col-md-3 controls">
                                            <input type="date" name="estimasi_sampai" class="form-control datepicker">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="col-md-8 controls checkbox checkbox-primary">
                                            <input id="checkbox2" class="styled" type="checkbox" checked name="is_stay">
                                            <label for="checkbox2"><b style="font-weight:600;">stay on the page.</b></label>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                            
                            <hr>
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-success">Save <?php echo $page_title; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Content -->

</div>