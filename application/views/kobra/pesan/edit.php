

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">

                
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    

    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Edit <?php echo $page_title; ?> <a href="<?php echo base_url($services) ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All Iklans </a></h4>

                </div>
                <div class="card-body">
                
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url($services.'update/'.$data->id); ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    <div class="col-md-9">
                                        <img src="<?php echo ($data->img_mid)? base_url($data->img_mid):base_url('assets/images/users/1.jpg'); ?>" class="img-thumbnail" width="200" height="200" id="profile_show" />
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="photo" id="profile_upload" onchange="readURL(this)">
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    $('#profile_show')
                                                        .attr('src', e.target.result)
                                                        .width(280)
                                                        .height(250);
                                                };
                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                                        $("#profile_show").click(function() {
                                            $("#profile_upload").click();
                                        });
                                    </script>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-left col-md-4"><h3>Kantor</h3> <span class="text-danger">*</span></label>
                                                <div class="col-md-8 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control form-control-line" name="branch">

                                                            <?php foreach ($list_branch as $branch): ?>
                                                                <?php 
                                                                    if($branch['name'] == $data->branch){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $branch['name']; ?>"><?php echo $branch['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-left col-md-4"><h3>Pesan Sistem</h3> <span class="text-danger">*</span></label>
                                                <div class="col-md-8 controls">
                                                    <textarea type="text" name="name" class="form-control" rows="10" cols="250" style="resize:both;" value="<?php echo $data->name; ?>"><?php echo $data->name; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                            
                            <hr>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3"></label>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-success">Save <?php echo $page_title; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>