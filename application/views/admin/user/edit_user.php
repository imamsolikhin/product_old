

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">User</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit User</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">

                
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white"> Edit User <a href="<?php echo base_url('admin/user/all_user_list') ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> All Users </a></h4>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/user/update/'.$user->id) ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-3">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <img src="<?php echo ($user->img_mid)? base_url($user->img_mid):base_url('assets/images/users/1.jpg'); ?>" class="img-thumbnail" width="280" height="250" id="profile_show" />
                                                </div>
                                                <div class="col-12">
                                                    <div class="input-group mb-3">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" name="photo" id="profile_upload" onchange="readURL(this)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <script>
                                                    function readURL(input) {
                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();
                                                            reader.onload = function (e) {
                                                                $('#profile_show')
                                                                    .attr('src', e.target.result)
                                                                    .width(280)
                                                                    .height(250);
                                                            };
                                                            reader.readAsDataURL(input.files[0]);
                                                        }
                                                    }
                                                    $("#profile_show").click(function() {
                                                        $("#profile_upload").click();
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-9">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Kantor</label>
                                                <div class="col-md-9 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control form-control-line" name="branch">

                                                            <?php foreach ($list_branch as $branch): ?>
                                                                <?php 
                                                                    if($branch['name'] == $user->branch){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $branch['name']; ?>"><?php echo $branch['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">First Name <span class="text-danger"></span></label>
                                                <div class="col-md-9 controls">
                                                    <input type="text" name="first_name" class="form-control" value="<?php echo $user->first_name; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Last Name <span class="text-danger"></span></label>
                                                <div class="col-md-9 controls">
                                                    <input type="text" name="last_name" class="form-control" value="<?php echo $user->last_name; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Email <span class="text-danger">*</span></label>
                                                <div class="col-md-9 controls">
                                                    <input type="email" name="email" class="form-control" required data-validation-required-message="Email is required" value="<?php echo $user->email; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Mobile </label>
                                                <div class="col-md-9 controls">
                                                    <input type="text" name="mobile" class="form-control" value="<?php echo $user->mobile; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Country</label>
                                                <div class="col-md-9 controls">
                                                    <div class="form-group has-success">
                                                        <select class="form-control form-control-line" name="country">

                                                            <?php foreach ($country as $cn): ?>
                                                                <?php 
                                                                    if($cn['id'] == $user->country){
                                                                        $selec = 'selected';
                                                                    }else{
                                                                        $selec = '';
                                                                    }
                                                                ?>
                                                                <option <?php echo $selec; ?> value="<?php echo $cn['id']; ?>"><?php echo $cn['name']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3"></label>
                                                <div class="controls">
                                                    <div class="form-check">
                                                        <label class="custom-control custom-radio">
                                                            <input <?php if($user->role == "admin"){echo "checked";}; ?> value="admin" name="role" type="radio" class="custom-control-input" required data-validation-required-message="You need to select user role type" aria-invalid="false">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">Admin</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input <?php if($user->role == "manajemen"){echo "checked";}; ?> value="manajemen" name="role" type="radio" class="custom-control-input" required data-validation-required-message="You need to select user role type" aria-invalid="false">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">Manajemen</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input <?php if($user->role == "cs-produk"){echo "checked";}; ?> value="cs-produk" name="role" type="radio" class="custom-control-input" required data-validation-required-message="You need to select user role type" aria-invalid="false">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">CS-Produk</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input <?php if($user->role == "admin-produk"){echo "checked";}; ?> value="admin-produk" name="role" type="radio" class="custom-control-input" required data-validation-required-message="You need to select user role type" aria-invalid="false">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">Admin-Produk</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input <?php if($user->role == "gudang-produk"){echo "checked";}; ?> value="gudang-produk" name="role" type="radio" class="custom-control-input" required data-validation-required-message="You need to select user role type" aria-invalid="false">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">Gudang-Produk</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- CSRF token -->
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3"></label>
                                                <div class="controls">
                                                    <button type="submit" class="btn btn-success">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>