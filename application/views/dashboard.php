

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    
<?php if ($this->session->userdata('role') == 'admin'){ ?>
    <a href="<?php echo base_url('admin/user/all_user_list') ?>">
    <div class="row">
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="d-flex flex-row">
                    <div class="p-10 bg-info">
                        <h3 class="text-white box m-b-0"><i class="fa fa-users fa-2x"></i></h3></div>
                    <div class="align-self-center m-l-20">
                        <h3 class="m-b-0 text-info"><?php echo $count->total; ?></h3>
                        <h5 class="text-muted m-b-0">Total User</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="d-flex flex-row">
                    <div class="p-10 bg-success">
                        <h3 class="text-white box m-b-0"><i class="fa fa-user fa-2x"></i></h3></div>
                    <div class="align-self-center m-l-20">
                        <h3 class="m-b-0 text-info"><?php echo $count->active_user; ?></h3>
                        <h5 class="text-muted m-b-0">Active User</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="d-flex flex-row">
                    <div class="p-10 bg-danger">
                        <h3 class="text-white box m-b-0"><i class="fa fa-user-times fa-2x"></i></h3></div>
                    <div class="align-self-center m-l-20">
                        <h3 class="m-b-0 text-info"><?php echo $count->inactive_user; ?></h3>
                        <h5 class="text-muted m-b-0">Inactive User</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="d-flex flex-row">
                    <div class="p-10 bg-primary">
                        <h3 class="text-white box m-b-0"><i class="fa fa-user-circle fa-2x"></i></h3></div>
                    <div class="align-self-center m-l-20">
                        <h3 class="m-b-0 text-info"><?php echo $count->admin; ?></h3>
                        <h5 class="text-muted m-b-0">Total Admin</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    </a>

<?php } ?>
<!--     <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Total Revenue</h4>
                    <div class="clear"></div>
                    <div class="grapic-sales" style="height: 42rem;"></div>
                    <script>
                        $(document).ready(function () {
                                        
                            $.ajax({
                                url: '<?php echo site_url('api/ajax_interaksi_chart')?>',
                                type: "GET",
                                dataType: "JSON",
                                // data : {'code':params},
                                success: function(response, status, xhr, $form) { 
                                    var name = new Array();
                                    var marks = new Array(); 
                                    var length = 0;          
                                    for (var i in response.data) {
                                        if(response.data[i].jml >= length){
                                            length = response.data[i].jml;
                                        }
                                        name.push(response.data[i].name);
                                        marks.push(response.data[i].jml);
                                    }
                                    var data = {
                                        labels: name,
                                        series: [marks]
                                    };
                                    var options = {
                                          seriesBarDistance: response.data.length
                                        , fullWidth: false
                                        , reverseData: false
                                        , horizontalBars: false
                                        , plugins: [Chartist.plugins.tooltip()]
                                        , stackBars: false
                                    };
                                    new Chartist.Bar('.grapic-sales', data, options)
                                    .on('draw', function (data) {
                                        if (data.type === 'bar') {
                                            data.element.attr({
                                                style: 'stroke-width: 2rem'
                                            });
                                        }
                                    });
                                },
                                error: function (jqXHR, textStatus, errorThrown){
                                    return;
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round align-self-center round-success"><i class="ti-wallet"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h3 class="m-b-0" id="klinik-1">0</h3>
                                    <h5 class="text-muted m-b-0" id="text-klinik-1">Prediction Income</h5></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-row">
                                <div class="round align-self-center round-success"><i class="ti-wallet"></i></div>
                                <div class="m-l-10 align-self-center">
                                    <h3 class="m-b-0" id="klinik-2">0</h3>
                                    <h5 class="text-muted m-b-0" id="text-klinik-2">Prediction Income</h5></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">LOG TRANSACTION 
                            <!-- <button id="startRealtime" class="btn btn-success">Start Realtime</button><button id="stopRealtime" class="btn btn-danger">Stop Realtime</button> -->
                            </h4>
                            
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table browser m-t-30 no-border scrolldown" id="log_table">
                                    <tbody style="overflow:auto;height:24rem;">
                                    </tbody>
                                </table>
                                <style> 
                                    table.scrolldown { 
                                        width: 100%; 
                                        border-spacing: 0; 
                                    } 
                                    
                                    table.scrolldown tbody, table.scrolldown thead { 
                                        display: block; 
                                    }  
                                    
                                    thead tr th { 
                                        height: 40px;  
                                        line-height: 40px; 
                                    } 
                                    
                                    table.scrolldown tbody { 
                                        height: 50px;  
                                        overflow-y: auto; 
                                        overflow-x: hidden;  
                                    } 
                                </style> 
                            </div>
                            <script type="text/javascript">
                                $( document ).ready(function() {
                                    var time = 0;
                                    var t = window.setInterval(function() {
                                        if(time==0) {
                                            $.ajax({
                                                url: '<?php echo site_url('api/sales')?>',
                                                type: "GET",
                                                dataType: "JSON",
                                                success: function(response, status, xhr, $form) { 
                                                    var klinik;
                                                    response.data_summary.reduce(function(res, value) {
                                                        if (klinik == res[value.klinik]) {
                                                            klinik = value.klinik;
                                                            $('#klinik-1').text("Mendapat : "+currency_idr(value.jml)+" Pasien");
                                                            $('#text-klinik-1').text("Prediction "+value.klinik);
                                                        }else{
                                                            $('#klinik-2').text("Mendapat : "+currency_idr(value.jml)+" Pasien");
                                                            $('#text-klinik-2').text("Prediction "+value.klinik);
                                                        }
                                                        return res;
                                                    }, {});
                                                    var Parent = document.getElementById("log_table");
                                                    while(Parent.hasChildNodes()){
                                                        Parent.removeChild(Parent.firstChild);
                                                    }
                                                    $('#log_table').append('<tbody style="overflow:auto;height:24rem;">');              
                                                        for (var i in response.data_log) {
                                                            // <td style="width:40px"><img src="<?php echo base_url() ?>assets/images/browser/chrome-logo.png" alt=logo /></td>
                                                            $('#log_table').append('<tr><td>'+response.data_log[i].created_by+'-'+response.data_log[i].time+'</td><td class="text-right"><span class="label label-light-info">'+response.data_log[i].confirm_status+'</span></td>');
                                                        }
                                                    $('#log_table').append('</tbody>');
                                                    time=5;
                                                },
                                                error: function (jqXHR, textStatus, errorThrown){
                                                    time = 5;
                                                    return;
                                                }
                                            });
                                        }time--
                                    }
                                    ,1000);
                                });

                                function currency_idr(val){
                                    var	number_string = val.toString(),
                                    sisa 	= number_string.length % 3,
                                        rupiah 	= number_string.substr(0, sisa),
                                        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                                            
                                    if (ribuan) {
                                        separator = sisa ? '.' : '';
                                        rupiah += separator + ribuan.join('.');
                                    }
                                    return rupiah;
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div id="calendar_marketing"></div>
                    <script>
                        $(document).ready(function() {
                            var calendar = $('#calendar_marketing').fullCalendar({
                                header:{
                                    left:'prev,next today',
                                    center:'title',
                                    right:'month,basicWeek,basicDay'
                                },
                                eventColor: 'green',
                                events: '<?php echo site_url('api/ajax_interaksi_calendar')?>',
                                displayEventTime: false,
                                eventRender: function (event, element, view) {
                                    if (event.allDay === 'true') {
                                        event.allDay = true;
                                    } else {
                                        event.allDay = false;
                                    }
                                },
                                selectable:true,
                                selectHelper:true

                            });
                        });
                    </script>
                </div>
            </div>
        </div>
        
    </div>
    <!-- Row --> -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
            