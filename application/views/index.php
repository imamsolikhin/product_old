<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/images/favicon.png">
    <title>KOBRA DIGITAL</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/css-chart/css-chart.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- toast CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Vector CSS -->
    <link href="<?php echo base_url() ?>/assets/plugins/vectormap/jquery-jvectormap-2.0.5.css" rel="stylesheet" />



    <!--Form css plugins -->
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <!-- <link href="<?php echo base_url() ?>assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo base_url() ?>assets/plugins/html5-editor/bootstrap-wysihtml5.css" rel="stylesheet" />
    <style>
        /* .col-md-9,.col-md-10{height:3rem;} */
        /* .dt-body-3-rem{width:6rem;} */
        .dt-body-nowrap {white-space: nowrap;}
        .form-group {margin-bottom: 5px;}
    </style>
    <!--Form css plugins end -->

    <!-- Calendar CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/calendar/dist/fullcalendar.css" rel="stylesheet" />
    <!-- summernotes CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />
    <!-- wysihtml5 CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/html5-editor/bootstrap-wysihtml5.css" />
    <!-- Dropzone css -->
    <link href="<?php echo base_url() ?>assets/plugins/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <!-- Cropper CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/cropper/cropper.min.css" rel="stylesheet">

    <!-- Footable CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />

    <!-- Date picker plugins css -->
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url() ?>assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <style>
    .topbar {
        background: #4fd879;
    }
    </style>
    
</head>

<body class="fix-header fix-sidebar card-no-border">
    
    <!-- Preloader - style you can find in spinners.css -->
    
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    
    <!-- Main wrapper - style you can find in pages.scss -->
    
    <div id="main-wrapper">
        
        <!-- Topbar header - style you can find in pages.scss -->
        
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                
                <!-- Logo -->
                
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url('dashboard') ?>">
                        <!-- Logo icon -->
                        <b>
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url() ?>assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url() ?>assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url() ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url() ?>assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                
                <!-- End Logo -->
                
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <marquee direction='left' scrollamount='8'><font color='white' size='8'><img src="<?php echo $this->session->userdata('psn_tmb') ?>" width="60" alt="homepage" class="dark-logo" /> <?php echo $this->session->userdata('psn'); ?></font></marquee>
                    
                    <!-- User profile and search -->
                    
                    <ul class="navbar-nav my-lg-0">
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo $this->session->userdata('tmb'); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo $this->session->userdata('mid'); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <hr>
                                                <h4><?php echo $this->session->userdata('name').' ['.$this->session->userdata('role').']'; ?></h4>
                                                <p class="text-muted"><?php echo $this->session->userdata('email'); ?></p>
                                                <!-- <a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div> -->
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo base_url('auth/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        
        <!-- End Topbar header -->
        









        
        
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        
        <aside class="left-sidebar">
            

            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap"></li>
                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url('dashboard') ?>" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
                        <?php if ($this->session->userdata('role') == 'admin'): ?>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/user/all_user_list') ?>" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">User</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('admin/User/all_user_list') ?>"><i class="fa fa-angle-right"></i> User Management</a></li>
                                </ul>
                            </li>
                            <li hidden>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">ANALISIS</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Analisis/cs_klinik') ?>"><i class="fa fa-angle-right"></i> MARKETING KLINIK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Analisis/calendar_klinik') ?>"><i class="fa fa-angle-right"></i> CALENDAR KLINIK </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">SETUP</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Branch') ?>"><i class="fa fa-angle-right"></i> KANTOR </a></li>
                                    <li><a href="<?php echo base_url('kobra/Klinik') ?>"><i class="fa fa-angle-right"></i> KLINK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Produk') ?>"><i class="fa fa-angle-right"></i> PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Iklan') ?>"><i class="fa fa-angle-right"></i> IKLAN </a></li>
                                    <li><a href="<?php echo base_url('kobra/ShipKerja') ?>"><i class="fa fa-angle-right"></i> SHIP KERJA </a></li>
                                    <li><a href="<?php echo base_url('kobra/Phone') ?>"><i class="fa fa-angle-right"></i> PHONE </a></li>
                                    <li><a href="<?php echo base_url('kobra/Pesan') ?>"><i class="fa fa-angle-right"></i> PESAN SISTEM </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MASTER</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/AdvertiseKlinik') ?>"><i class="fa fa-angle-right"></i> ADVERTISE KLINIK</a></li>
                                    <li><a href="<?php echo base_url('kobra/AdvertiseProduk') ?>"><i class="fa fa-angle-right"></i> ADVERTISE PRODUK</a></li>
                                    <li><a href="<?php echo base_url('kobra/Country') ?>"><i class="fa fa-angle-right"></i> COUNTRY </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiKategory') ?>"><i class="fa fa-angle-right"></i> INTERAKSI KATEGORY </a></li>
                                    <li><a href="<?php echo base_url('kobra/Gender') ?>"><i class="fa fa-angle-right"></i> GENDER </a></li>
                                    <li><a href="<?php echo base_url('kobra/Emosional') ?>"><i class="fa fa-angle-right"></i> EMOSIONAL </a></li>
                                    <li><a href="<?php echo base_url('kobra/PasienStatus') ?>"><i class="fa fa-angle-right"></i> PASIEN STATUS </a></li>
                                    <li><a href="<?php echo base_url('kobra/PasienStatusKlinik') ?>"><i class="fa fa-angle-right"></i> PASIEN STATUS KLINIK</a></li>
                                    <li><a href="<?php echo base_url('kobra/CustomerStatus') ?>"><i class="fa fa-angle-right"></i> CUSTOMER STATUS </a></li>
                                    <li><a href="<?php echo base_url('kobra/CustomerStatusProduk') ?>"><i class="fa fa-angle-right"></i> CUSTOMER STATUS PRODUK</a></li>
                                    <li><a href="<?php echo base_url('kobra/ConfirmStatus') ?>"><i class="fa fa-angle-right"></i> KONFIRM STATUS </a></li>
                                    <li><a href="<?php echo base_url('kobra/AkunMarket') ?>"><i class="fa fa-angle-right"></i> AKUN MARKET </a></li>
                                    <li><a href="<?php echo base_url('kobra/Bank') ?>"><i class="fa fa-angle-right"></i> BANK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Kurir') ?>"><i class="fa fa-angle-right"></i> KURIR PENGIRIMAN </a></li>
                                    <li><a href="<?php echo base_url('kobra/TypeTransaction') ?>"><i class="fa fa-angle-right"></i> TIPE TRANSAKSI </a></li>
                                    <li><a href="<?php echo base_url('kobra/ApprovalStatus') ?>"><i class="fa fa-angle-right"></i> APPROVAL STATUS </a></li>
                                    <li><a href="<?php echo base_url('kobra/DeliveryStatus') ?>"><i class="fa fa-angle-right"></i> DELIVERY STATUS </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MANAGEMENT DATA</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('management/Interaksi/interaksi_produk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                </ul>
                            </li>
                            <li hidden>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MARKETING KLINIK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiKlinik') ?>"><i class="fa fa-angle-right"></i> INTERAKSI </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiKlinik/closing') ?>"><i class="fa fa-angle-right"></i> CLOSING REQUEST </a></li>
                                    <li><a href="<?php echo base_url('kobra/PasienKlinik') ?>"><i class="fa fa-angle-right"></i> PASIEN KLINIK </a></li>
                                    <li><a href="<?php echo base_url('kobra/PasienKlinik/Approval') ?>"><i class="fa fa-angle-right"></i> APPROVAL PASIEN </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MARKETING PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/closing') ?>"><i class="fa fa-angle-right"></i> CLOSING REQUEST </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">ADMIN PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/approval') ?>"><i class="fa fa-angle-right"></i> REQS. APPROVAL </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/gudang') ?>"><i class="fa fa-angle-right"></i> GUDANG CONFIRMATION </a></li>
                                    <li><a href="<?php echo base_url('kobra/BarangMasuk') ?>"><i class="fa fa-angle-right"></i> BARANG MASUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/BarangKeluar') ?>"><i class="fa fa-angle-right"></i> BARANG KELUAR </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MANAGEMENT DATA</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('management/Interaksi/interaksi_produk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                    <li><a href="<?php echo base_url('management/Interaksi/marketing_data') ?>"><i class="fa fa-angle-right"></i> MARKETING DATA </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">REPORT</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Report/produk') ?>"><i class="fa fa-angle-right"></i> LAPORAN INTERAKSI </a></li>
                                    <li><a href="<?php echo base_url('kobra/Report/report_cs_produk') ?>"><i class="fa fa-angle-right"></i> LAPORAN CS PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Report/report_cs_customer') ?>"><i class="fa fa-angle-right"></i> LAPORAN CS CUSTOMER </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="waves-effect waves-dark" href="<?php echo base_url('Dashboard/backup') ?>" aria-expanded="false"><i class="fa fa-cloud-download"></i><span class="hide-menu">Backup Database</span></a>
                            </li>
                        <?php elseif ($this->session->userdata('role') == 'manajemen'): ?>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/User/all_user_list') ?>" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">User</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('admin/User/all_user_list') ?>"><i class="fa fa-angle-right"></i> User Management</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">ANALISIS</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Marketing') ?>"><i class="fa fa-angle-right"></i> MARKETING </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">SETUP</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Klinik') ?>"><i class="fa fa-angle-right"></i> KLINK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Produk') ?>"><i class="fa fa-angle-right"></i> PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Iklan') ?>"><i class="fa fa-angle-right"></i> IKLAN </a></li>
                                    <li><a href="<?php echo base_url('kobra/ShipKerja') ?>"><i class="fa fa-angle-right"></i> SHIP KERJA </a></li>
                                    <li><a href="<?php echo base_url('kobra/Phone') ?>"><i class="fa fa-angle-right"></i> PHONE </a></li>
                                    <li><a href="<?php echo base_url('kobra/Pesan') ?>"><i class="fa fa-angle-right"></i> PESAN SISTEM </a></li>
                                </ul>
                            </li>
                        <?php elseif ($this->session->userdata('role') == 'cs-klinik'): ?>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/InteraksiKlinik') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">INTERAKSI</span></a></li>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/InteraksiKlinik/closing') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">CLOSING REQUEST</span></a></li>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/InteraksiKlinik/report') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">LAPORAN INTERAKSI</span></a></li>

                        <?php elseif ($this->session->userdata('role') == 'cs-produk'): ?>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MARKETING PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/closing') ?>"><i class="fa fa-angle-right"></i> CLOSING REQUEST </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MANAGEMENT DATA</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('management/Interaksi/interaksi_produk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">REPORT</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Report/produk') ?>"><i class="fa fa-angle-right"></i> MARKETING PRODUK</a></li>
                                </ul>
                            </li>

                        <?php elseif ($this->session->userdata('role') == 'admin-produk'): ?>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MARKETING PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/closing') ?>"><i class="fa fa-angle-right"></i> CLOSING REQUEST </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">ADMIN PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/approval') ?>"><i class="fa fa-angle-right"></i> REQS. APPROVAL </a></li>
                                    <li><a href="<?php echo base_url('kobra/BarangMasuk') ?>"><i class="fa fa-angle-right"></i> BARANG MASUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/BarangKeluar') ?>"><i class="fa fa-angle-right"></i> BARANG KELUAR </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MANAGEMENT DATA</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('management/Interaksi/interaksi_produk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                    <li><a href="<?php echo base_url('management/Interaksi/marketing_data') ?>"><i class="fa fa-angle-right"></i> MARKETING DATA </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">REPORT</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Report/produk') ?>"><i class="fa fa-angle-right"></i> MARKETING PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Report/report_cs_produk') ?>"><i class="fa fa-angle-right"></i> LAPORAN CS PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/Report/report_cs_customer') ?>"><i class="fa fa-angle-right"></i> LAPORAN CS CUSTOMER </a></li>
                                </ul>
                            </li>

                        <?php elseif ($this->session->userdata('role') == 'gudang-produk'): ?>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MARKETING PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/closing') ?>"><i class="fa fa-angle-right"></i> CLOSING REQUEST </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">GUDANG PRODUK</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/InteraksiProduk/gudang') ?>"><i class="fa fa-angle-right"></i> GUDANG CONFIRMATION </a></li>
                                    <li><a href="<?php echo base_url('kobra/BarangMasuk') ?>"><i class="fa fa-angle-right"></i> BARANG MASUK </a></li>
                                    <li><a href="<?php echo base_url('kobra/BarangKeluar') ?>"><i class="fa fa-angle-right"></i> BARANG KELUAR </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">MANAGEMENT DATA</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('management/Interaksi/interaksi_produk') ?>"><i class="fa fa-angle-right"></i> INTERAKSI PRODUK </a></li>
                                    <li><a href="<?php echo base_url('management/Interaksi/marketing_data') ?>"><i class="fa fa-angle-right"></i> MARKETING DATA </a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">REPORT</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url('kobra/Report/produk') ?>"><i class="fa fa-angle-right"></i> MARKETING PRODUK </a></li>
                                </ul>
                            </li>

                        <?php elseif ($this->session->userdata('role') == 'klinik'): ?>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/InteraksiKlinik') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">INTERAKSI</span></a></li>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/InteraksiKlinik/closing') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">CLOSING REQUEST</span></a></li>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/InteraksiKlinik/report') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">LAPORAN INTERAKSI</span></a></li>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/PasienKlinik') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">PASIEN KLINIK</span></a></li>
                            <li><a class="waves-effect waves-dark" href="<?php echo base_url('kobra/PasienKlinik/Approval') ?>" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">APPROVAL PASIEN</span></a></li>
                        <?php endif ?>
                        <br>
                        <hr>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item-->
                <a href="#" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                <!-- item-->
                <a href="#" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <!-- item-->
                <a href="<?php echo base_url('auth/logout') ?>" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
            </div>
            <!-- End Bottom points-->



        </aside>
        
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        


    
    <!-- End Wrapper -->
    



    
    <!-- All Jquery -->
    
    <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url() ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url() ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/custom.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
    
    <!-- This page plugins -->
    
    <!-- chartist chart -->
    <script src="<?php echo base_url() ?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/echarts/echarts-all.js"></script>
    
    <!-- Calendar JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/moment/moment.js"></script>
    <script src='<?php echo base_url() ?>assets/plugins/calendar/dist/fullcalendar.min.js'></script>
    <script src="<?php echo base_url() ?>assets/plugins/calendar/dist/jquery.fullcalendar.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/calendar/dist/cal-init.js"></script>
    
    <!-- sparkline chart -->
    <script src="<?php echo base_url() ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- <script src="<?php echo base_url() ?>assets/js/dashboard4.js"></script> -->

    <!-- Sweet-Alert  -->
    <script src="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
    
    <!-- toast notification CSS -->
    <script src="<?php echo base_url() ?>assets/plugins/toast-master/js/jquery.toast.js"></script>
    <script src="<?php echo base_url() ?>assets/js/toastr.js"></script>

    <!-- google maps api -->
    <!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyCUBL-6KdclGJ2a_UpmB2LXvq7VOcPT7K4&amp;sensor=true"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/gmaps/gmaps.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/gmaps/jquery.gmaps.js"></script> -->

    <!-- Vector map JavaScript -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-2.0.5.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap.id.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-2.0.5.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-in-mill.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-uk-mill-en.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jquery-jvectormap-au-mill.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/vectormap/jvectormap.custom.js"></script> -->

    <!--Morris JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/raphael/raphael-min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/morrisjs/morris.js"></script>
    <script src="<?php echo base_url() ?>assets/js/morris-data.js"></script>
    <!-- Chart JS -->
    
    <script src="<?php echo base_url() ?>assets/plugins/Chart.js/chartjs.init.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/Chart.js/Chart.min.js"></script>

    <!-- Invoice print JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    
    <script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="http://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="http://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="http://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="http://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="http://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="http://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/switchery/dist/switchery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <!-- <script type="text/javascript" src="../assets/plugins/multiselect/js/jquery.multi-select.js"></script> -->
    <script src="<?php echo base_url() ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

    <script>
        $(document).ready(function() {
            $("#print").click(function() {
                var options = {
                    mode: 'iframe',
                    popClose: 'popup'
                };
                $("div.printableArea").printArea(options);
            });
            
        });
        $.extend( true, $.fn.dataTable.defaults, {
            "select": true,
            "searching": true,
            "processing": true, //Feature control the processing indicator.
            "responsive": true, 
            "autoWidth": true,
            "scrollX": true,
            "scrollY": "600px",
            "scrollCollapse": true,
            "order": [],
            "ordering": false,
            "order": [[ 0, "desc" ]],
            "lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]],
            "bPaginate": true,
            "lengthChange": true,
            "iDisplayLength": 100,
            "language": {
                "decimal":        "",
                "emptyTable":     "Data Tidak Ditemukan",
                "info":           "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 ke 0 dari 0 data",
                "infoFiltered":   "(pencarian dari _MAX_ jumlah data)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Menampilkan _MENU_",
                "loadingRecords": "Menunggu...",
                "processing":     "Memproses data...",
                "search":         "Cari:",
                "zeroRecords":    "Tidak ada data yang sesuai",
                "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "Selanjutnya",
                "previous":   "Sebelumnya"
                },
                "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
                }
            }

        });

        $( document ).ready(function(){
           $('.delete_msg').delay(3000).slideUp();
        });
        

        function show_data_product(data_id, table_id, services_id){
           // ajax adding data to database
            $.ajax({
              url : "<?php echo base_url(); ?>Api/ajax_data",
              type: "GET",
              data: {id:data_id,table:table_id,services:services_id},
              dataType: "JSON",
              success: function(datas){
                $('#modal_show_data').modal("show");
                $('.summernote').summernote('code', datas.data_show);
              },
              error: function (jqXHR, textStatus, errorThrown){
                  alert('Gagal Cetak Data');
                  return;
              }
            });
        }

        function proses_print(urls){
            window.open(urls,"A4", "resizable,scrollbars,status");
           // ajax adding data to database
            // $.ajax({
            //   url : "<?php echo base_url(); ?>src/cetak/print",
            //   type: "POST",
            //   headers: {
            //     "Content-Type": "application/x-www-form-urlencoded", 
            //     "Authorization": $('#token').val(), 
            //   },
            //   data: $('#form-kop').serialize(),
            //   dataType: "JSON",
            //   success: function(datas){
            //         window.open("<?php echo base_url(); ?>src/Pdfs/cetak/"+datas,"Legal", "resizable,scrollbars,status");
            //   },
            //   error: function (jqXHR, textStatus, errorThrown){
            //       alert('Gagal Cetak Data');
            //       return;
            //   }
            // });
        }

        function print_pdf(urls,data){
            proses_print(urls,data);
            //    // ajax adding data to database
            //     $.ajax({
            //       url : urls,
            //       type: "Get",
            //       headers: {
            //         "Content-Type": "application/x-www-form-urlencoded", 
            //       },
            //       dataType: "JSON",
            //       success: function(data){
            //             $('#token').val(data.token);
            //             $('#nomor').val(data.result.ctNo_sk);
            //             $('#tanggal').val(data.result.ctTgl_sk);
            //             $('#tanggalPengajuan').val(data.result.ctTgl_pengajuan);
            //             $('#kepada').val(data.result.ctKeterangan);
            //             $('#form-kop-head').modal('show');
            //       },
            //       error: function (jqXHR, textStatus, errorThrown){
            //           alert('Gagal Print Data');
            //           return;
            //       }
            //     });
        }
    </script>

    <div class="page-wrapper">
        <?php echo $main_content; ?>
        <footer class="footer">
            powered by devjrcamp © 2020 KOBRA DIGITAL
        </footer>
    </div>
    </div>

    <!-- Modal -->
    <div id="modal_show_data" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">FORMAT LAPORAN</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class="summernote">
                </div>
                <script>
                    $('.summernote').summernote({
                        height: 350, // set editor height
                        minHeight: null, // set minimum height of editor
                        maxHeight: null, // set maximum height of editor
                        focus: false // set focus to editable area after initializing summernote
                    });
                
                    function copy_report(pram) {
                        var from = document.getElementById(pram);
                        var range = document.createRange();
                        window.getSelection().removeAllRanges();
                        range.selectNode(from);
                        window.getSelection().addRange(range);
                        document.execCommand('copy');
                        window.getSelection().removeAllRanges();
                        
                        $.toast({
                            heading: 'Salin Laporan',
                            text: 'Laporan Berhasil di Salin.',
                            icon: 'warning',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#9EC600'  // To change the background
                        });
                    }
                </script>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Copy Format</button> -->
          </div>
        </div>
      </div>
    </div>
</body>

</html>
