    

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <div class="card">

                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-12 controls pull-left">
                                <div class=" form-groupcol-md-1 pull-left">
                                    Periode
                                </div>
                                <div class="form-group col-md-3 pull-left">
                                    <input type="date" id="start_date" name="start_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                                </div>
                                <div class="form-group col-md-3 pull-left">
                                    <input type="date" id="end_date" name="end_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                                </div>
                                <div class="form-group col-md-3 pull-left">
                                    <select value="0" class="form-control custom-select" name="create_or_order_status" aria-invalid="false">
                                        <option value="0" selected>TGL DIBUAT</option>
                                        <option value="1">TGL TRANSAKSI</option>
                                    </select>
                                </div>
                            </div> 
                        </div> 
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-12 controls pull-left">
                                <div class=" form-groupcol-md-1 pull-left">
                                    FILTER
                                </div>
                                <div class="form-group col-md-3 pull-left">
                                    <select value="" class="form-control custom-select" name="admin_status" aria-invalid="false">
                                        <option value="" selected>Konfirmasi Admin</option>
                                        <option value="0" >PENDING</option>
                                        <option value="1">APPROVED</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 pull-left">
                                    <select value="" class="form-control custom-select" name="gudang_status" aria-invalid="false">
                                        <option value="">Konfirmasi Gudang</option>
                                        <?php foreach ($list_gudang_status as $gudang_status): ?>
                                            <option value="<?php echo $gudang_status['name']; ?>"><?php echo $gudang_status['name']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 pull-left">
                                    <select value="" class="form-control custom-select" name="interaksi_status" aria-invalid="false">
                                        <option value="" selected>All</option>
                                        <option value="NO">INTERAKSI</option>
                                        <option value="YES">TRANSAKSI</option>
                                    </select>
                                </div>
                                <button onclick="reload_table()" class="btn btn-info pull-left"><i class="fa fa-search-plus"></i> Search</button>  
                            </div> 
                        </div> 
                    </div> 
                

                    <div class="table-responsive m-t-10">
                        <table id="example-tools" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>no</th>
                                    <th>No Ref</th>
                                    <th>No Resi</th>
                                    <th>Status Pengiriman</th>
                                    <th>Kantor</th>
                                    <th>Produk</th>
                                    <th>Ship Kerja</th>
                                    <th>Iklan</th>
                                    <th>Interaksi</th>
                                    <th>No CS</th>
                                    <th>Nama CS</th>
                                    <th>Tanggal Input</th>
                                    <th>Nama Customer</th>
                                    <th>Konfirmasi Status</th>
                                    <th>Keluhan</th>
                                    <th>Umur</th>
                                    <th>Emosional</th>
                                    <th>Tlp</th>
                                    <th>Wa</th>
                                    <th>Alamat</th>
                                    <th>No Rumah</th>
                                    <th>Rt</th>
                                    <th>Rw</th>
                                    <th>Desa</th>
                                    <th>Kelurahan</th>
                                    <th>Patokan</th>
                                    <th>Kecamatan</th>
                                    <th>Kota</th>
                                    <th>Provinsi</th>
                                    <th>Kode Pos</th>
                                    <th>Alamat Lengkap</th>
                                    <th>Jml Pesanan</th>
                                    <th>Harga Produk</th>
                                    <th>Biaya Pengiriman</th>
                                    <th>Asuransi</th>
                                    <th>Total Transaksi</th>
                                    <th>Total Transaksi_id</th>
                                    <th>Kurir</th>
                                    <th>Type Transaction</th>
                                    <th>Akun Market</th>
                                    <th>Bank</th>
                                    <th>Transaksi Status</th>
                                    <th>Jenis Pembelian</th>
                                    <th>Tanggal Pembelian</th>
                                    <th>Estimasi Sampai</th>
                                    <th>Closing Status</th>
                                    <th>Closing Approval</th>
                                    <th>Closing Remark</th>
                                    <th>Packing By</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>

<script type="text/javascript">
    var code; 
    var table;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd+'-'+ mm +'-'+ yyyy;
    $(document).ready(function(){
        table = $('#example-tools').DataTable({
            "processing":true,
            "serverSide": true,  
            "paginate": true,
            "lengthChange": true,
            "bFilter": true,
            "dataType": 'json',
            "ajax": {
            "url": "<?php echo site_url('Api/ajax_list_management_marketing_produk')?>",
            "data":{ 
                table: "<?php echo $table ?>",
                services: "<?php echo $services ?>",
                '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $token;?>',
                start_date: $("#start_date").val(),
                end_date: $("#end_date").val(),
                interaksi_status: $("select[name='interaksi_status'] option:selected").val(),
                create_or_order_status: $("select[name='create_or_order_status'] option:selected").val(),
                admin_status: $("select[name='admin_status'] option:selected").val(),
                gudang_status: $("select[name='gudang_status'] option:selected").val()
            },
            "dataSrc": function ( json ) {
               if(json.csrf_token !== undefined) $('meta[name=csrf_token]').attr("content", json.csrf_token);
               return json.data;
            },
            "type": "POST",
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "initComplete": function(settings, json) {
                $('#example-tools_filter input').unbind();
                $('#example-tools_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search( this.value ).draw();
                    }
                }); 
            },
            "columnDefs": [
                { className: "dt-body-nowrap", "targets":'_all' },
            ]
        });  
    });
    function reload_table(){
        $('#example-tools').DataTable().destroy();
        table = $('#example-tools').DataTable({
            "processing":true,
            "serverSide": true,  
            "paginate": true,
            "lengthChange": true,
            "bFilter": true,
            "dataType": 'json',
            "ajax": {
            "url": "<?php echo site_url('Api/ajax_list_management_marketing_produk')?>",
            "data":{ 
                table: "<?php echo $table ?>",
                services: "<?php echo $services ?>",
                '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $token;?>',
                start_date: $("#start_date").val(),
                end_date: $("#end_date").val(),
                interaksi_status: $("select[name='interaksi_status'] option:selected").val(),
                create_or_order_status: $("select[name='create_or_order_status'] option:selected").val(),
                admin_status: $("select[name='admin_status'] option:selected").val(),
                gudang_status: $("select[name='gudang_status'] option:selected").val()
            },
            "dataSrc": function ( json ) {
               if(json.csrf_token !== undefined) $('meta[name=csrf_token]').attr("content", json.csrf_token);
               return json.data;
            },
            "type": "POST",
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "initComplete": function(settings, json) {
                $('#example-tools_filter input').unbind();
                $('#example-tools_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search( this.value ).draw();
                    }
                }); 
            },
            "columnDefs": [
                { className: "dt-body-nowrap", "targets":'_all' },
            ]
        });  
        // table.reload();
    }

    function delete_data(code,name){
        $(".modal-title").text(name);
        $("#myModal").modal();
        document.getElementById("delete_submit").href="<?php echo site_url($services.'delete/')?>"+code;
    }

</script>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">		
				<h4 class="modal-title">Are you sure?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<a type="button" class="btn btn-danger" href="#" id="delete_submit">Delete</a>
			</div>
		</div>
	</div>
</div>  