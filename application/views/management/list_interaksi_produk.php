

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <div class="card">

                <div class="card-body">
                    <div class="form-group col-md-12">
                        <div class="form-group col-md-12 controls pull-left">
                            <div class=" form-groupcol-md-1 pull-left">
                                Periode
                            </div>
                            <div class="form-group col-md-3 pull-left">
                                <input type="date" id="start_date" name="start_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                            </div>
                            <div class="form-group col-md-3 pull-left">
                                <input type="date" id="end_date" name="end_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                            </div>
                            <div class="form-group col-md-3 pull-left">
                                <select value="NO" class="form-control custom-select" name="reqs_status" aria-invalid="false">
                                    <option value="NO" selected>INTERAKSI</option>
                                    <option value="YES">TRANSAKSI</option>
                                </select>
                            </div>
                            <button onclick="reload_table()" class="btn btn-info pull-left"><i class="fa fa-search-plus"></i> Search</button>  
                        </div> 
                    </div> 
                

                    <div class="table-responsive m-t-40">
                        <table id="example-tools" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>no</th>
                                    <th>branch</th>
                                    <th>produk</th>
                                    <th>ship_kerja</th>
                                    <th>advertise</th>
                                    <th>interaksi_kategory</th>
                                    <th>id_iklan</th>
                                    <th>nama_lengkap</th>
                                    <th>confirm_status</th>
                                    <th>keluhan</th>
                                    <th>jenis_kelamin</th>
                                    <th>umur</th>
                                    <th>tlp</th>
                                    <th>wa</th>
                                    <th>alamat</th>
                                    <th>alamat_lengkap</th>
                                    <th>no_rumah</th>
                                    <th>rt</th>
                                    <th>rw</th>
                                    <th>desa</th>
                                    <th>kelurahan</th>
                                    <th>patokan</th>
                                    <th>kecamatan</th>
                                    <th>kota</th>
                                    <th>provinsi</th>
                                    <th>kode_pos</th>
                                    <th>emosional</th>
                                    <th>jml_pesanan</th>
                                    <th>harga</th>
                                    <th>biaya_pengiriman</th>
                                    <th>asuransi</th>
                                    <th>total_transaksi</th>
                                    <th>total_transaksi_id</th>
                                    <th>kurir</th>
                                    <th>type_transaction</th>
                                    <th>akun_market</th>
                                    <th>bank</th>
                                    <th>reqs_status</th>
                                    <th>customer_status</th>
                                    <th>tanggal_order</th>
                                    <th>estimasi_sampai</th>
                                    <th>status</th>
                                    <th>closing_status</th>
                                    <th>closing_approval</th>
                                    <th>closing_remark</th>
                                    <th>created_by</th>
                                    <th>created_at</th>
                                    <th>delivery_img_tmb</th>
                                    <th>delivery_img_mid</th>
                                    <th>delivery_status</th>
                                    <th>delivery_resi</th>
                                    <th>delivery_ref</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>

<script type="text/javascript">
    var code; 
    var table;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd+'-'+ mm +'-'+ yyyy;
    $(document).ready(function(){
        table = $('#example-tools').DataTable({
            "processing":true,
            "serverSide": true,  
            "paginate": true,
            "lengthChange": true,
            "bFilter": true,
            "dataType": 'json',
            "ajax": {
            "url": "<?php echo site_url('Api/ajax_list_management_interaksi_produk')?>",
            "data":{ 
                table: "<?php echo $table ?>",
                services: "<?php echo $services ?>",
                '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $token;?>',
                start_date: $("#start_date").val(),
                end_date: $("#end_date").val(),
                reqs_status: $("select[name='reqs_status'] option:selected").val()
            },
            "dataSrc": function ( json ) {
               if(json.csrf_token !== undefined) $('meta[name=csrf_token]').attr("content", json.csrf_token);
               return json.data;
            },
            "type": "POST",
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "initComplete": function(settings, json) {
                $('#example-tools_filter input').unbind();
                $('#example-tools_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search( this.value ).draw();
                    }
                }); 
            },
            "columnDefs": [
                { className: "dt-body-nowrap", "targets":'_all' },
            ]
        });  
    });
    function reload_table(){
        $('#example-tools').DataTable().destroy();
        table = $('#example-tools').DataTable({
            "processing":true,
            "serverSide": true,  
            "paginate": true,
            "lengthChange": true,
            "bFilter": true,
            "dataType": 'json',
            "ajax": {
            "url": "<?php echo site_url('Api/ajax_list_management_interaksi_produk')?>",
            "data":{ 
                table: "<?php echo $table ?>",
                services: "<?php echo $services ?>",
                '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $token;?>',
                start_date: $("#start_date").val(),
                end_date: $("#end_date").val(),
                reqs_status: $("select[name='reqs_status'] option:selected").val()
            },
            "dataSrc": function ( json ) {
               if(json.csrf_token !== undefined) $('meta[name=csrf_token]').attr("content", json.csrf_token);
               return json.data;
            },
            "type": "POST",
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "initComplete": function(settings, json) {
                $('#example-tools_filter input').unbind();
                $('#example-tools_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search( this.value ).draw();
                    }
                }); 
            },
            "columnDefs": [
                { className: "dt-body-nowrap", "targets":'_all' },
            ]
        });  
        // table.reload();
    }

    function delete_data(code,name){
        $(".modal-title").text(name);
        $("#myModal").modal();
        document.getElementById("delete_submit").href="<?php echo site_url($services.'delete/')?>"+code;
    }

</script>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">		
				<h4 class="modal-title">Are you sure?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<a type="button" class="btn btn-danger" href="#" id="delete_submit">Delete</a>
			</div>
		</div>
	</div>
</div>  