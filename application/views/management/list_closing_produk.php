

<!-- Container fluid  -->

<div class="container-fluid">
    
    <!-- Bread crumb and right sidebar toggle -->
    
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">All <?php echo $page_title; ?></li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Start Page Content -->

    <div class="row">
        <div class="col-12">

            <?php $msg = $this->session->flashdata('msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>

            <div class="card">

                <div class="card-body">
                    <div class="col-md-12 controls pull-left">
                        <div class="col-md-1 pull-left">
                            Periode
                        </div>
                        <div class="col-md-3 pull-left">
                            <input type="date" id="start_date" name="start_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                        </div>
                        <div class="col-md-3 pull-left">
                            <input type="date" id="end_date" name="end_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>">
                        </div>
                        <div class="col-md-3 pull-left">
                            <select value="" class="form-control custom-select" name="produk" aria-invalid="false">
                                <option value="0" selected>Produk</option>
                                <?php foreach ($list_produk as $produk): ?>
                                    <?php 
                                        if($produk['name'] == $data->produk){
                                            $selec = 'selected';
                                        }else{
                                            $selec = '';
                                        }
                                    ?>
                                    <option <?php echo $selec; ?> value="<?php echo $produk['name']; ?>"><?php echo $produk['name']; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <button onclick="reload_table()" class="btn btn-info pull-left"><i class="fa fa-search-plus"></i> Search</button>  
                    </div> 
                

                    <div class="table-responsive m-t-40">
                        <table id="example-tools" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>no</th>
                                    <th>CS Produk</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Customer</th>
                                    <th>Jml Pesanan</th>
                                    <th>Total Transaksi</th>
                                    <th>No Resi</th>
                                    <th>No Pesanan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Page Content -->

</div>

<script type="text/javascript">
    var code; 
    var table;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd+'-'+ mm +'-'+ yyyy;
    $(document).ready(function(){
        table = $('#example-tools').DataTable({
            "processing":true,
            "serverSide": true,  
            "paginate": true,
            "lengthChange": true,
            "bFilter": true,
            "dataType": 'json',
            "dataSrc": "Data",
            "ajax": {
            "url": "<?php echo site_url('Api/ajax_list_management_closing_produk')?>",
            "data":{ 

                table: "<?php echo $table ?>",
                services: "<?php echo $services ?>",
                '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $token;?>',
                start_date: $("#start_date").val(),
                end_date: $("#end_date").val(),
                produk: $("select[name='produk'] option:selected").val()
            },
            "dataSrc": function ( json ) {
               if(json.csrf_token !== undefined) $('meta[name=csrf_token]').attr("content", json.csrf_token);
               return json.data;
            },
            "type": "POST",
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "initComplete": function(settings, json) {
                $('#example-tools_filter input').unbind();
                $('#example-tools_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search( this.value ).draw();
                    }
                }); 
            },
            "columnDefs": [
                { className: "dt-body-nowrap", "targets":'_all' },
            ]
        });  
    });
    function reload_table(){
        table.ajax.reload();
    }

    function send_closing(){
        $("#myModal").modal("hide");
        $.ajax({
          url:"<?php echo base_url(); ?>Api/approval_closing",
          dataType: 'json',
          method:"GET",
          data:{id:$("#id").val(id)},
          success:function(data){
            reload_table();
          }
        });
    };

    function aproval_data(id,created_by,customer,transaksi,pesanan,status){
        $(".modal-title").text("KONFIRMASI CLOSING");
        $("#id").val(id);
        $("#status").val(status);
        $("#created_by").val(created_by);
        $("#nama_lengkap").val(customer);
        $("#total_transaction").val(transaksi);
        $("#no_pesanan").val(pesanan);
        $("#myModal").modal("show");
    }

</script>


<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">      
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_approval">
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Nama CS Produk</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" id="created_by">
                      <input hidden type="text" readonly class="form-control-plaintext" id="id">
                      <input hidden type="text" readonly class="form-control-plaintext" id="status">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Nama Customer</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" id="nama_lengkap">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Jumlah Pesanan</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" id="total_transaction">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">No Pesanan</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" id="no_pesanan">
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" onclick="send_closing()" id="approved_submit">Approved</button>
            </div>
        </div>
    </div>
</div>  