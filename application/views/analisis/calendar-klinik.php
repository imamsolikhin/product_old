

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title; ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
            </ol>
        </div>
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">FILTER CALENDAR</h4>
                    <div class="col-md-10">
                        <div class="form-group row">
                            <div class="col-md-12 controls">
                                <div class="form-group has-success">
                                    <select class="form-control custom-select" id="confirm_status" value="Berkunjung" aria-invalid="false">
                                        <?php foreach ($list_confirm_status as $confirm_status): ?>
                                            <?php 
                                                if($confirm_status['name'] == 'Berkunjung'){
                                                    $selec = 'selected';
                                                }else{
                                                    $selec = '';
                                                }
                                            ?>
                                            <option <?php echo $selec; ?> value="<?php echo $confirm_status['name']; ?>"><?php echo $confirm_status['name']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">SHOW CALENDAR</h4>
                    <div class="col-md-10">
                        <div class="form-group row">
                            <div class="col-md-12 controls">
                                <div class="form-group has-success">
                                    <select class="form-control custom-select" id="show_status" value="created_at" aria-invalid="false">
                                        <option selected value="created_at">Interaksi Masuk</option>
                                        <option value="confirm_date">Kedatangan Pasien</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4 class="card-title">DATA CALENDAR</h4>
                    <div class="col-md-10">
                        <div class="form-group row">
                            <div class="col-md-12 controls">
                                <div class="form-group has-success">
                                    <select class="form-control custom-select" id="data_status" value="created_at" aria-invalid="false">
                                        <option selected value="created_at">Interaksi Masuk</option>
                                        <option value="confirm_date">Kedatangan Pasien</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">SUMMARY KARYAWAN</h4>
                    <table id="summary_table" class="table browser m-t-30 no-border scrolldown">
                    </table>
                    <style> 
                        table.scrolldown { 
                            width: 100%; 
                            border-spacing: 0; 
                        } 
                        
                        table.scrolldown tbody, table.scrolldown thead { 
                            display: block; 
                        }  
                        
                        thead tr th { 
                            height: 40px;  
                            line-height: 40px; 
                        } 
                        
                        table.scrolldown tbody { 
                            height: 50px;  
                            overflow-y: auto; 
                            overflow-x: hidden;  
                        } 
                    </style> 
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <br>
                    <h2 style="text-align:center;" class="display-5" >GLOBAL KALENDER KLINIK</h2>
                    <h2 id="head_title" style="text-align:center; color:blue;">KEDATANGAN PASIEN</h2>
                </div>
                <div class="card-body">
                    <div id="calendar_marketing"></div>
                    <script>
                        $(document).ready(function() {
                            document.getElementById("head_title").innerHTML = "PASIEN "+$("select#confirm_status").children("option:selected").val().toUpperCase();
                            $("#confirm_status,#show_status,#data_status").change(function(){
                                $('#calendar_marketing').fullCalendar('refetchEvents');
                                document.getElementById("head_title").innerHTML = "PASIEN "+$("select#confirm_status").children("option:selected").val().toUpperCase();
                            });

                            var calendar = $('#calendar_marketing').fullCalendar({
                                header:{
                                    left:'prev,next today',
                                    center:'title',
                                    right:'month,basicWeek,basicDay'
                                },
                                eventColor: 'green',
                                events: function(start, end, timezone, callback) {
                                    // alert(end);
                                    $.ajax({
                                        url: '<?php echo site_url('api/ajax_analisis_calendar')?>',
                                        dataType: 'Json',
                                        data: {
                                            start: start.unix(),
                                            end: end.unix(),
                                            status: $("select#confirm_status").children("option:selected").val(),
                                            show: $("select#show_status").children("option:selected").val(),
                                            data: $("select#data_status").children("option:selected").val()
                                        },
                                        success: function(response) {      
                                            var summary = [];
                                            var events = [];
                                            for (var i in response.data) {
                                                events.push({
                                                    title: response.data[i].title,
                                                    start: response.data[i].start,
                                                    end: response.data[i].end,
                                                    url: response.data[i].url
                                                });
                                            }
                                            callback(events);

                                            var result = [];
                                            response.data.reduce(function(res, value) {
                                            if (!res[value.by]) {
                                                res[value.by] = { name: value.by,mid:value.mid,tmb:value.tmb, total: 0 };
                                                result.push(res[value.by])
                                            }
                                            res[value.by].total = parseFloat(res[value.by].total)+parseFloat(value.id);
                                            return res;
                                            }, {});
                                            var Parent = document.getElementById("summary_table");
                                            while(Parent.hasChildNodes()){
                                                Parent.removeChild(Parent.firstChild);
                                            }
                                            $('#summary_table').append('<tbody style="overflow:auto;height:30rem;">');              
                                            // $('#summary_table').append('<tbody>');              
                                            for (var i in result) {
                                                $('#summary_table').append('<tr><td><img width="50" src="<?php echo base_url() ?>'+result[i].tmb+'" alt=logo /></td><td>'+result[i].name+'</td><td class="text-right"><span class="label label-light-primary">'+result[i].total+'</span></td></tr>');
                                                // $('#log_table').append('<tr><td>'+response.data_log[i].created_by+'-'+response.data_log[i].time+'</td><td class="text-right"><span class="label label-light-info">'+response.data_log[i].confirm_status+'</span></td>');
                                            }
                                            $('#summary_table').append('</tbody>');
                                        }
                                    });
                                },
                                eventRender: function (event, element, view) {
                                    if (event.allDay === 'true') {
                                        event.allDay = true;
                                    } else {
                                        event.allDay = false;
                                    }
                                },
                                displayEventTime: true,
                                selectable:true,
                                selectHelper:true

                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
            