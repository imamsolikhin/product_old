<?php
class Common_model extends CI_Model {

    
    public function __construct(){
        parent::__construct();        
        $this->db->from('pesan');
        $this->db->where('status', true);
        if($this->session->userdata('branch') !=""){
            $this->db->where('branch', $this->session->userdata('branch'));
        }
        $this->db->order_by('id DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $query = $query->row(); 
        // echo var_dump($query);die;
        $this->session->set_flashdata('psn', $query->name);
        $this->session->set_flashdata('psn_tmb', base_url($query->img_tmb));
        $this->session->set_flashdata('psn_mid', base_url($query->img_mid));
        
    }
    
	var $column_search = array(
          'id','name'
    );
    
	// FUNGSI PENGAMBILAN DATA UNTUK JQUERY DATA TABLE
	private function _get_datatables_query($where,$table,$order){
		$this->db->from($table);
		$i = 0;
		foreach ($this->column_search as $data) {
			if($_POST['search']['value']){
				if($i===0) {
					$this->db->group_start();
					$this->db->like($data, $_POST['search']['value']);
				}else{
					if($_POST['search']['value'] == 'Done'){
						$this->db->or_like($data, 1);
					}else{
						$this->db->or_like($data, $_POST['search']['value']);
					}
				}
				if(count($this->column_search)-1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}
		$this->db->where($where);
        $this->db->order_by($order);
        $this->db->order_by("created_at DESC");
		if(isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
        $this->db->group_by("id");
	}

	// FUNGSI PENGAMBILAN DATA UNTUK JQUERY DATA TABLE
	public function get_datatables($where,$table,$order){
		$this->_get_datatables_query($where,$table,$order);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		
		$query = $this->db->get();
		return $query->result();
	}

	// FUNGSI PENGAMBILAN DATA UNTUK JQUERY DATA TABLE
	public function count_filtered($where,$table){
		$this->_get_datatables_query($where,$table,"");
		$query = $this->db->get();
		return $query->num_rows();
	}

	// FUNGSI PENGAMBILAN JUMLAH DATA BERDASARKAN PARAMETER
	public function count_all($where,$table){
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
    }

    //-- select other function
    function select_other($select, $where, $group, $order, $table){
        $this->db->select($select);
		$this->db->from($table);
        $this->db->group_by($group);
        $this->db->order_by($order);
        $this->db->where($where);
        // $this->db->limit(5);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 
    
    
    //-- insert function
	public function insert($data,$table){
        $this->db->insert($table,$data);        
        return $this->db->insert_id();
    }
    //-- edit function
    function edit_option($action, $id, $table){
        $this->db->where('id',$id);
        $this->db->update($table,$action);
        return;
    } 

    //-- update function
    function update($action, $id, $table){
        $this->db->where('id',$id);
        $this->db->update($table,$action);
        return;
    } 

    //-- delete function
    function delete($id,$table){
        $this->db->delete($table, array('id' => $id));
        return;
    }

    //-- user role delete function
    function delete_user_role($id,$table){
        $this->db->delete($table, array('user_id' => $id));
        return;
    }

    //-- count function
    function count($table){
        $this->db->select();
        $this->db->from($table);
        $this->db->order_by('id','ASC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    //-- select function
    function select($table){
        $this->db->select();
        $this->db->from($table);
        $this->db->order_by('id','ASC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }
    
    //-- select function
    function get_id($id,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }

    //-- select function
    function get_name($name,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where('name', $name);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }
    
    //-- select function
    function get_generade_code($where,$table){
        $this->db->select("COUNT(id) as jml");
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }
    
    //-- select function
    function get_opt($opt,$val,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where($opt, $val);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }

    //-- select by id
    function select_option($id,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 

    //-- select by id
    function select_active($table){
        $this->db->select();
        $this->db->from($table);
        // $this->db->where('id', $id);
        $this->db->where('status', true);
        $this->db->order_by('id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 

    //-- select by id
    function select_active_by_branch($table){
        $this->db->select();
        $this->db->from($table);
        // $this->db->where('id', $id);
        $this->db->where('status', true);
        $this->db->where('branch', $this->session->userdata('branch'));
        $this->db->order_by('id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 


    //-- select by id
    function select_active_by_branch_other($table,$where){
        $this->db->select();
        $this->db->from($table);
        // $this->db->where('id', $id);
        $this->db->where('status', true);
        $this->db->where('branch', $this->session->userdata('branch'));
        $this->db->order_by('id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 

    //-- select by mod
    function select_query($where,$order,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($order);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 

    //-- check user role power
    function check_power($type){
        $this->db->select('ur.*');
        $this->db->from('user_role ur');
        $this->db->where('ur.user_id', $this->session->userdata('id'));
        $this->db->where('ur.action', $type);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    public function check_email($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {                 
            return $query->result();
        }else{
            return false;
        }
    }

    public function check_exist_power($id){
        $this->db->select('*');
        $this->db->from('user_power');
        $this->db->where('power_id', $id); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {                 
            return $query->result();
        }else{
            return false;
        }
    }


    //-- get all power
    function get_all_power($table){
        $this->db->select();
        $this->db->from($table);
        $this->db->order_by('power_id','ASC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    //-- get logged user info
    function get_user_info(){
        $this->db->select('u.*');
        $this->db->select('c.name as country_name');
        $this->db->from('user u');
        $this->db->join('country c','c.id = u.country','LEFT');
        $this->db->where('u.id',$this->session->userdata('id'));
        $this->db->order_by('u.id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    //-- get single user info
    function get_single_user_info($id){
        $this->db->select('u.*');
        $this->db->select('c.name as country_name');
        $this->db->from('user u');
        $this->db->join('country c','c.id = u.country','LEFT');
        $this->db->where('u.id',$id);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }

    //-- get single user info
    function get_user_role($id){
        $this->db->select('ur.*');
        $this->db->from('user_role ur');
        $this->db->where('ur.user_id',$id);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }


    //-- get all users with type 2
    function get_all_user(){
        $this->db->select('u.*');
        $this->db->select('c.name as country, c.id as country_id');
        $this->db->from('user u');
        $this->db->join('country c','c.id = u.country','LEFT');
        $this->db->order_by('u.id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }


    //-- count active, inactive and total user
    function get_user_total(){
        $this->db->select('*');
        $this->db->select('count(*) as total');
        $this->db->select('(SELECT count(user.id)
                            FROM user 
                            WHERE (user.status = 1)
                            )
                            AS active_user',TRUE);

        $this->db->select('(SELECT count(user.id)
                            FROM user 
                            WHERE (user.status = 0)
                            )
                            AS inactive_user',TRUE);

        $this->db->select('(SELECT count(user.id)
                            FROM user 
                            WHERE (user.role = "admin")
                            )
                            AS admin',TRUE);

        $this->db->from('user');
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }

    //-- image upload function with resize option
    function upload_image($max_size,$name_file,$id_field){
            
            //-- set upload path
            $config['upload_path']  = "./assets/images/";
            $config['allowed_types']= 'gif|jpg|png|jpeg';
            $config['max_size']     = '92000';
            $config['overwrite']    = TRUE;
            $config['max_width']    = '92000';
            $config['max_height']   = '92000';
            $config['file_name']   = $name_file;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload($id_field)) {

                
                $data = $this->upload->data();

                //-- set upload path
                $source             = "./assets/images/".$data['file_name'] ;
                $destination_thumb  = "./assets/images/thumbnail/" ;
                $destination_medium = "./assets/images/medium/" ;
                $main_img = $data['file_name'];
                // Permission Configuration
                chmod($source, 0777) ;

                /* Resizing Processing */
                // Configuration Of Image Manipulation :: Static
                $this->load->library('image_lib') ;
                $img['image_library'] = 'GD2';
                $img['create_thumb']  = TRUE;
                $img['maintain_ratio']= TRUE;

                /// Limit Width Resize
                $limit_medium   = $max_size ;
                $limit_thumb    = 200 ;

                // Size Image Limit was using (LIMIT TOP)
                $limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;

                // Percentase Resize
                if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
                    $percent_medium = $limit_medium/$limit_use ;
                    $percent_thumb  = $limit_thumb/$limit_use ;
                }

                //// Making THUMBNAIL ///////
                $img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
                $img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
                $img['thumb_marker'] = '_thumb-'.floor($img['width']).'x'.floor($img['height']) ;
                $img['quality']      = ' 100%' ;
                $img['source_image'] = $source ;
                $img['new_image']    = $destination_thumb ;

                $thumb_nail = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
                $this->image_lib->initialize($img);
                $this->image_lib->resize();
                $this->image_lib->clear() ;

                ////// Making MEDIUM /////////////
                $img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
                $img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
                $img['thumb_marker'] = '_medium-'.floor($img['width']).'x'.floor($img['height']) ;
                $img['quality']      = '100%' ;
                $img['source_image'] = $source ;
                $img['new_image']    = $destination_medium ;

                $mid = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
                $this->image_lib->initialize($img);
                $this->image_lib->resize();
                $this->image_lib->clear() ;

                //-- set upload path
                $images = 'assets/images/medium/'.$mid;
                $thumb  = 'assets/images/thumbnail/'.$thumb_nail;
                unlink($source) ;
                $data = array(
                    'images' => $images,
                    'thumb' => $thumb
                );
                return $data;
            }
            else {
                return false;
            }
            
    }
    
	function sendEmail($subject="INFO CUTI/001/KP.05.03/X/2018",$email="de4imamsolikhin@gmail.com",$pesan="STATUS KONFIRMASI OLEH NIP/NAMA TELAH DI SETUJUI"){
		$config['protocol']    = 'smtp';
	    $config['smtp_host']    = 'mx1.hostinger.co.id';
	    $config['smtp_port']    = '587';
	    $config['smtp_timeout'] = '5';
	    $config['smtp_user']    = 'cuti-kemendesa@cdf-pe.com';
	    $config['smtp_pass']    = 'de4ragil';
	    $config['charset']    = 'utf-8';
	    $config['newline']    = "\r\n";
	    $config['mailtype'] = 'html'; // or html
	    // $config['validation'] = TRUE; // bool whether to validate email or not     
        
        $this->email->initialize($config);
        // $this->email->set_mailtype("html");
        // $this->email->set_newline("\r\n");

	    $this->email->from('cuti-kemendesa@cdf-pe.com', 'CUTI ONLINE KEMENDESA');
	    $this->email->to($email); 

	    $this->email->subject($subject);
	    $this->email->message("<h1>INFO PENGAJUAN CUTI KEMENDESA</h1>".$pesan); 

        //Send email
	    $this->email->send();
	}
}