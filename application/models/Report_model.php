<?php
class Report_model extends CI_Model {

    //-- select 
    function select($select){
        $this->db->select($select);
        $query = $this->db->get();
        $query = $query->result();  
        return $query;
    } 

    //-- select function
    function get_select($query){
        $this->db->select($query);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }

    //-- count by mod
    function count_query($where,$group,$order,$table){
        $this->db->count();
        $this->db->from($table);
        $this->db->where($where);
        $this->db->group_by($group);
        $this->db->order_by($order);
        // $this->db->limit(100);
        return $this->db->count_all_results();
        return $query;
    } 
    //-- select by mod
    function select_query($select,$where,$group,$order,$table){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->group_by($group);
        $this->db->order_by($order);
        // $this->db->limit(100);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 
}